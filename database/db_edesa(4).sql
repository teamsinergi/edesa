-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Okt 2019 pada 10.00
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_edesa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_admin_rt`
--

CREATE TABLE `tbl_admin_rt` (
  `id` int(11) NOT NULL,
  `nama_rt` varchar(150) NOT NULL,
  `no_rt` int(11) NOT NULL,
  `no_rw` int(11) NOT NULL,
  `no_hp` char(20) NOT NULL,
  `email_rt` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `kode_rt` char(20) NOT NULL,
  `status` int(11) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_admin_rt`
--

INSERT INTO `tbl_admin_rt` (`id`, `nama_rt`, `no_rt`, `no_rw`, `no_hp`, `email_rt`, `username`, `password`, `kode_rt`, `status`, `create_at`) VALUES
(14, 'gema antika hariadi', 43, 9, '081230123091283', 'adminrt@gmail.com', '', 'adminrt@gmail.com', '943', 2, '2019-08-27 00:00:00'),
(15, 'luqman hanafi', 2, 1, '', 'adminrt2@gmail.com', '', 'adminrt2@gmail.com', '12', 2, '0000-00-00 00:00:00'),
(16, 'bambang', 45, 9, '', 'bamba@gmail.com', '', 'hgggjh', '945', 2, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_admin_rw`
--

CREATE TABLE `tbl_admin_rw` (
  `id` int(11) NOT NULL,
  `nama_rw` varchar(150) NOT NULL,
  `no_rw` int(11) NOT NULL,
  `no_hp` char(20) NOT NULL,
  `email_rw` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `kode_rw` char(20) NOT NULL,
  `status` int(11) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_admin_rw`
--

INSERT INTO `tbl_admin_rw` (`id`, `nama_rw`, `no_rw`, `no_hp`, `email_rw`, `username`, `password`, `kode_rw`, `status`, `create_at`) VALUES
(2, 'gema antika hariadi RW', 9, '081230123091283', 'adminrw@gmail.com', 'adminrw@gmail.com', 'adminrw@gmail.com', '9', 1, '0000-00-00 00:00:00'),
(3, 'Bambang sugiono', 7, '0982324', 'bambang@gmail.com', 'bambang@gmail.com', 'bambang@gmail.com', '7', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_agama`
--

CREATE TABLE `tbl_agama` (
  `id_agama` int(11) NOT NULL,
  `nama_agama` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_agama`
--

INSERT INTO `tbl_agama` (`id_agama`, `nama_agama`) VALUES
(1, 'Islam'),
(2, 'Kristen Protestan'),
(3, 'Katolik'),
(4, 'Hindu'),
(5, 'Buddha'),
(6, 'Kong Hu Cu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_agenda`
--

CREATE TABLE `tbl_agenda` (
  `id` int(11) NOT NULL,
  `nama_agenda` varchar(150) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal_agenda` date NOT NULL,
  `tempat` text NOT NULL,
  `jam_agenda` time NOT NULL,
  `gambar_agenda` varchar(150) NOT NULL,
  `kode_rt` char(20) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_agenda`
--

INSERT INTO `tbl_agenda` (`id`, `nama_agenda`, `keterangan`, `tanggal_agenda`, `tempat`, `jam_agenda`, `gambar_agenda`, `kode_rt`, `create_at`) VALUES
(1, 'Launching edesa', 'buat launcing e desa', '2016-06-28', 'dekat', '14:12:00', '', '12', '2019-08-28'),
(3, 'Bazar Hiburan Sumpah Pemuda', 'Sumpah pemuda', '2019-09-26', 'desaku', '11:11:00', '5ec5330becf416b9ee49a79f654f166e.jpg', '943', '2019-09-19'),
(4, 'agenda 1', 'Kerja bakti', '2019-09-02', 'lubuk hatiku paling dalam', '21:21:00', '8c66fe1d9ec7b8bf48e1e972c7ff64a7.jpg', '943', '2019-09-19'),
(5, 'Pemilu', 'pemilifan presiden', '2019-09-11', 'rt 63', '11:11:00', 'efd9e54d5df37441d8521e83f3cdeb47.jpg', '943', '2019-09-27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_agenda_rw`
--

CREATE TABLE `tbl_agenda_rw` (
  `id` int(11) NOT NULL,
  `nama_agenda` varchar(150) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal_agenda` date NOT NULL,
  `tempat` text NOT NULL,
  `jam_agenda` time NOT NULL,
  `kode_rw` char(20) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_agenda_rw`
--

INSERT INTO `tbl_agenda_rw` (`id`, `nama_agenda`, `keterangan`, `tanggal_agenda`, `tempat`, `jam_agenda`, `kode_rw`, `create_at`) VALUES
(1, 'Rapat harian RW', 'Rapat harian RW', '2019-09-28', 'rumah pak rw', '11:11:00', '9', '2019-09-27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_akun_warga`
--

CREATE TABLE `tbl_akun_warga` (
  `id` int(11) NOT NULL,
  `no_kk` char(50) NOT NULL,
  `nik` char(50) NOT NULL,
  `norw` char(10) NOT NULL,
  `nort` char(10) NOT NULL,
  `password` varchar(150) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_akun_warga`
--

INSERT INTO `tbl_akun_warga` (`id`, `no_kk`, `nik`, `norw`, `nort`, `password`, `create_at`) VALUES
(5, '3471022403030073', '3471021904750001', '9', '43', 'rahasia', '2019-09-20 00:00:00'),
(6, '3471020801020415', '3471020708740000', '9', '43', 'rahasia', '2019-09-26 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_bulan`
--

CREATE TABLE `tbl_bulan` (
  `id_bulan` int(11) NOT NULL,
  `kode_bulan` int(11) NOT NULL,
  `nama_bulan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_bulan`
--

INSERT INTO `tbl_bulan` (`id_bulan`, `kode_bulan`, `nama_bulan`) VALUES
(1, 1, 'Januari'),
(2, 2, 'Februari'),
(3, 3, 'Maret'),
(4, 4, 'April'),
(5, 5, 'Mei'),
(6, 6, 'Juni'),
(7, 7, 'Juli'),
(8, 8, 'Agustus'),
(9, 9, 'September'),
(10, 10, 'Oktober'),
(11, 11, 'November'),
(12, 12, 'Desember');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_galeri`
--

CREATE TABLE `tbl_galeri` (
  `id` int(11) NOT NULL,
  `nama_galeri` varchar(150) NOT NULL,
  `keterangan` text NOT NULL,
  `tempat` text NOT NULL,
  `tanggal_galeri` date NOT NULL,
  `foto_galeri` varchar(150) NOT NULL,
  `create_at` date NOT NULL,
  `kode_rt` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_galeri`
--

INSERT INTO `tbl_galeri` (`id`, `nama_galeri`, `keterangan`, `tempat`, `tanggal_galeri`, `foto_galeri`, `create_at`, `kode_rt`) VALUES
(4, 'Kerja Bakti', 'Kerja bakti bersama remaja', 'jogjaku', '2019-09-28', '8120d5c8f9efea95fc3646e2a2df6e79.jpg', '2019-09-27', '943'),
(5, 'Karnaval Busana', 'Karnaval busana jogja', 'jogja', '2019-09-23', '985e001084c259f96b2e775521007299.jpeg', '2019-09-27', '943'),
(6, 'Voli', 'Voli', 'Lapangan', '2019-09-18', '02fc0edb4174b301c289a02cd9cf21ed.jpg', '2019-09-27', '943'),
(8, 'Karnaval Busana', 'Memeriahkan', 'tugu', '2019-09-19', '190f7052b90738713096f9bad877285d.jpeg', '2019-09-27', '943');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_galeri_rw`
--

CREATE TABLE `tbl_galeri_rw` (
  `id` int(11) NOT NULL,
  `nama_galeri` varchar(150) NOT NULL,
  `keterangan` text NOT NULL,
  `tempat` text NOT NULL,
  `tanggal_galeri` date NOT NULL,
  `foto_galeri` varchar(150) NOT NULL,
  `create_at` date NOT NULL,
  `kode_rw` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_galeri_rw`
--

INSERT INTO `tbl_galeri_rw` (`id`, `nama_galeri`, `keterangan`, `tempat`, `tanggal_galeri`, `foto_galeri`, `create_at`, `kode_rw`) VALUES
(2, 'asdfasdf', 'buat main aja', 'mall malioboro', '2018-11-11', '9355d3a06eb091853db86757d6c38e28.png', '2019-08-28', '1'),
(3, 'q', 'q', 'q', '2019-11-01', '601a0bcd2d8534e5ba8203fa810e03e6.png', '2019-08-28', '1'),
(4, 'Lomba nari', 'Memeriahkan ', 'gedung wanita', '2019-09-28', 'eb8505697403cfb611fc078c342822a1.jpg', '2019-09-27', '9');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_inventori_barang`
--

CREATE TABLE `tbl_inventori_barang` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `spesifikasi` varchar(150) NOT NULL,
  `banyak_barang` int(11) NOT NULL,
  `kondisi_barang` varchar(150) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `create_at` date NOT NULL,
  `foto_barang` varchar(150) NOT NULL,
  `kode_rt` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_inventori_barang`
--

INSERT INTO `tbl_inventori_barang` (`id`, `nama_barang`, `spesifikasi`, `banyak_barang`, `kondisi_barang`, `keterangan`, `create_at`, `foto_barang`, `kode_rt`) VALUES
(1, 'Kursi', 'warna biru', 150, 'Rusak', 'baru beli', '2019-09-19', '', '943');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_inventori_barang_rw`
--

CREATE TABLE `tbl_inventori_barang_rw` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `spesifikasi` varchar(150) NOT NULL,
  `banyak_barang` int(11) NOT NULL,
  `kondisi_barang` varchar(150) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `create_at` date NOT NULL,
  `foto_barang` varchar(150) NOT NULL,
  `kode_rw` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_inventori_barang_rw`
--

INSERT INTO `tbl_inventori_barang_rw` (`id`, `nama_barang`, `spesifikasi`, `banyak_barang`, `kondisi_barang`, `keterangan`, `create_at`, `foto_barang`, `kode_rw`) VALUES
(3, 'cintaku adalah cintamu ku', 'Ram 4 gb ku', 34, 'Baik', 'mahal coy ku', '2019-08-27', '285c549f360dc26a36b058b0fffaa4be.png', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kas`
--

CREATE TABLE `tbl_kas` (
  `id` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `tanggal_kas` date NOT NULL,
  `create_at` datetime NOT NULL,
  `kode_rt` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kas_keluar`
--

CREATE TABLE `tbl_kas_keluar` (
  `id` int(11) NOT NULL,
  `jenis_pengeluaran` varchar(150) NOT NULL,
  `nominal` int(11) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `tanggal_pengeluaran` date NOT NULL,
  `create_at` datetime NOT NULL,
  `kode_rt` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_kas_keluar`
--

INSERT INTO `tbl_kas_keluar` (`id`, `jenis_pengeluaran`, `nominal`, `keterangan`, `tanggal_pengeluaran`, `create_at`, `kode_rt`) VALUES
(6, 'Sodakoh', 2500000, 'sodakoh', '2019-08-27', '2019-08-27 00:00:00', '15'),
(7, 'Penyewaan Barang', 250000, 'sewa monitor', '2019-09-26', '2019-09-27 00:00:00', '943'),
(8, 'Penyewaan Barang', 40000, 'sewa mobil', '2019-01-08', '2019-10-28 00:00:00', '943'),
(9, 'Pembelian Barang', 35000, 'beli lampu', '2019-10-08', '2019-10-28 00:00:00', '943'),
(10, 'Sodakoh', 800000, 'anak yatim', '2019-10-15', '2019-10-28 00:00:00', '943'),
(11, 'Pembiayaan Acara', 1000000, 'acara 17an', '2019-08-15', '2019-10-28 00:00:00', '943'),
(12, 'Pembiayaan Acara', 200000, 'beli sapu', '2019-07-17', '2019-10-28 00:00:00', '943'),
(13, 'Pembiayaan Acara', 1000000, 'buat acara', '2019-10-14', '2019-10-28 00:00:00', '943'),
(14, 'Pembiayaan Acara', 1000000, 'acara sinergicreative', '2019-05-09', '2019-10-28 00:00:00', '943'),
(15, 'Sodakoh', 100000, 'sodakoh', '2019-07-09', '2019-10-28 00:00:00', '943'),
(16, 'Sodakoh', 100000, 'sodakoh', '2019-01-07', '2019-10-28 00:00:00', '943'),
(17, 'Pembelian Barang', 100000, 'kertas hvs', '2019-03-13', '2019-10-28 00:00:00', '943'),
(18, 'Penyewaan Barang', 30000, 'sewa mobil', '2019-02-06', '2019-10-28 00:00:00', '943');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kas_masuk`
--

CREATE TABLE `tbl_kas_masuk` (
  `id` int(11) NOT NULL,
  `jenis_pemasukan` char(50) NOT NULL,
  `nominal` int(11) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `tanggal_pemasukan` date NOT NULL,
  `create_at` datetime NOT NULL,
  `kode_rt` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_kas_masuk`
--

INSERT INTO `tbl_kas_masuk` (`id`, `jenis_pemasukan`, `nominal`, `keterangan`, `tanggal_pemasukan`, `create_at`, `kode_rt`) VALUES
(1, 'Usaha', 250000, 'penjualan barang', '2019-09-28', '2019-09-27 00:00:00', '943'),
(2, 'APBD Desa', 15000000, 'Anggaran Desa', '2019-09-25', '2019-09-27 00:00:00', '943'),
(3, 'Kas Bulanang', 1000000, 'kas bulanan', '2019-01-16', '2019-10-28 00:00:00', '943'),
(4, 'Sumbangan', 2000000, 'sumbangan', '2019-02-13', '2019-10-28 00:00:00', '943'),
(5, 'Usaha', 3000000, 'usaha ', '2019-03-13', '2019-10-28 00:00:00', '943'),
(6, 'APBD Desa', 5000000, 'apbd', '2019-04-12', '2019-10-28 00:00:00', '943'),
(7, 'Kas Bulanang', 1000000, 'bulanan', '2019-10-16', '2019-10-28 00:00:00', '943'),
(8, 'Sumbangan', 6000000, 'semoga barokah', '2019-05-03', '2019-10-28 00:00:00', '943'),
(9, 'Usaha', 4000000, 'bulanan', '2019-06-13', '2019-10-28 00:00:00', '943'),
(10, 'Sumbangan', 1000000, 'barokah', '2019-07-12', '2019-10-28 00:00:00', '943'),
(11, 'Sumbangan', 4000000, 'barokah', '2019-08-17', '2019-10-28 00:00:00', '943'),
(12, 'Sumbangan', 8000000, 'barokah', '2019-11-21', '2019-10-28 00:00:00', '943'),
(13, 'Sumbangan', 3000000, 'bulanan', '2019-12-12', '2019-10-28 00:00:00', '943');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kematian`
--

CREATE TABLE `tbl_kematian` (
  `id` int(11) NOT NULL,
  `nik` char(50) NOT NULL,
  `nama_warga` varchar(150) NOT NULL,
  `tanggal_meninggal` date NOT NULL,
  `tempat_pemakaman` text NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_kematian`
--

INSERT INTO `tbl_kematian` (`id`, `nik`, `nama_warga`, `tanggal_meninggal`, `tempat_pemakaman`, `create_at`) VALUES
(5, '3471024503550002', 'RR A DWI SRI RAHAYU', '2019-09-27', 'TPU Jogja', '2019-09-27'),
(6, '3471021306750001', 'LANJAR KUSSUHASTO', '2019-09-20', 'TPU Jogja', '2019-09-27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_lapor_kejadian`
--

CREATE TABLE `tbl_lapor_kejadian` (
  `id` int(11) NOT NULL,
  `nama_kejadian` varchar(150) NOT NULL,
  `tempat_kejadian` varchar(150) NOT NULL,
  `foto_kejadian` varchar(150) NOT NULL,
  `jam` time NOT NULL,
  `norw` int(11) NOT NULL,
  `nort` int(11) NOT NULL,
  `nik` varchar(150) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_lapor_kejadian`
--

INSERT INTO `tbl_lapor_kejadian` (`id`, `nama_kejadian`, `tempat_kejadian`, `foto_kejadian`, `jam`, `norw`, `nort`, `nik`, `create_at`) VALUES
(1, 'asdasd', 'asdasd', '', '00:00:00', 0, 0, '0', '2019-09-26'),
(2, 'nakal', 'kosan pak mul', '0f7f9a88adcef8408025e71ff4cf3c75.png', '00:00:00', 9, 43, '0', '2019-10-15'),
(3, 'kos nakal', 'kosan pak mul', '66e9b77f009633dcb3f77e73a3849137.png', '11:11:00', 9, 43, '0', '2019-10-15'),
(4, 'asdasd', 'asdasdasdasdas', '31043a268438d0f3729cca04661a107a.png', '15:33:00', 9, 43, '43', '2019-10-15'),
(5, 'nakal banget', 'asdasdas', '2c94b2dcae25281dc23d7280a03260e0.png', '11:11:00', 9, 43, '43', '2019-10-15'),
(6, 'asd', 'a', '9e0e0d1f96b9ef3be6984f4f25a8c968.png', '11:11:00', 9, 43, '0', '2019-10-15'),
(7, 'asd', 's', '103fd4d42b2763b2989739efaadee182.png', '11:11:00', 9, 43, '3471021904750001', '2019-10-15'),
(8, 'penjahat', 'kos kulon', '97a51ba60caabe5c1f409b510cfdc594.jpg', '02:01:00', 9, 43, '3471021904750001', '2019-10-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pekerjaan`
--

CREATE TABLE `tbl_pekerjaan` (
  `id_pekerjaan` int(11) NOT NULL,
  `nama_pekerjaan` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pekerjaan`
--

INSERT INTO `tbl_pekerjaan` (`id_pekerjaan`, `nama_pekerjaan`) VALUES
(8, 'Ibu Rumah Tangga'),
(1, 'Mahasiswa / Pelajar'),
(4, 'Nelayan'),
(5, 'Pedagang'),
(2, 'Pegawai Negri Sipil'),
(3, 'Perawat'),
(7, 'POLISI'),
(10, 'Tidak Bekerja'),
(6, 'TNI'),
(9, 'Wiraswasta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_superuser`
--

CREATE TABLE `tbl_superuser` (
  `id` int(11) NOT NULL,
  `email_superuser` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_superuser`
--

INSERT INTO `tbl_superuser` (`id`, `email_superuser`, `password`) VALUES
(1, 'superuser@gmail.com', 'superuser@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_warga`
--

CREATE TABLE `tbl_warga` (
  `id` int(11) NOT NULL,
  `no_kk` char(25) NOT NULL,
  `nik` char(25) NOT NULL,
  `nama_warga` varchar(150) NOT NULL,
  `no_rw` int(11) NOT NULL,
  `no_rt` int(11) NOT NULL,
  `pekerjaan` varchar(150) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `status_keluarga` int(11) NOT NULL,
  `jenis_kelamin` enum('pria','wanita') NOT NULL,
  `id_agama` int(11) NOT NULL,
  `kode_rt` char(20) NOT NULL,
  `status` char(20) NOT NULL,
  `alamat` text NOT NULL,
  `domisili` text NOT NULL,
  `status_hidup` enum('meninggal','hidup') NOT NULL DEFAULT 'hidup',
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_warga`
--

INSERT INTO `tbl_warga` (`id`, `no_kk`, `nik`, `nama_warga`, `no_rw`, `no_rt`, `pekerjaan`, `tanggal_lahir`, `status_keluarga`, `jenis_kelamin`, `id_agama`, `kode_rt`, `status`, `alamat`, `domisili`, `status_hidup`, `create_at`) VALUES
(1, '3471020801020415', '3471020708740000', 'BONDAN WIRAWAN', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'bapak', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(2, '3471020801020415', '3471026901780001', 'RAFIKA NILASARI', 9, 43, 'IBU RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(3, '3471020801020415', '3471023110010001', 'NAUFAL ARIEQ WIRA PRADANA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(4, '3471022403030073', '3471021904750001', 'SUPRIYANA', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'bapak', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(5, '3471022403030073', '3471025603780001', 'MARSETYANI HADIYANTI', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(6, '3471022403030073', '3471024908010001', 'SILVIA AGATHA PUTRIYANA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(7, '3471022403030073', '2471024506070001', 'MUTIARAHMADEWI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(8, '3471021301150003', '3302260710820002', 'HENDARSYAH EKO SAPUTRO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(9, '3471021301150003', '3403105209850001', 'FARIDA NURIYATUN', 9, 43, 'GURU', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(10, '3471022712970814', '3471021405270001', 'PONO BA', 9, 43, 'PENSIUNAN', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(11, '3471022712970814', '3471020512900001', 'AJI INDRA KUMARA', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(12, '3471022208961494', '3471020911630002', 'BAMBANG PRACAYA', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(13, '3471022208961494', '3471027007620001', 'SRI SISWANTI', 9, 43, 'IBU RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(14, '3471022208961494', '3471020504910001', 'DESIKO REZA PUTRA', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(15, '3471022208961494', '3471025512960001', 'DEVI FRASISCA', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(16, '3471022208961494', '3471022702000001', 'AULIA ANGKI TRI SUNU', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(17, '3471020908120001', '3471024809620001', 'PARJIYAH', 9, 43, 'IBU RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(18, '3471020908120001', '3471025706910001', 'FITRI CHUMAIROH', 9, 43, 'GURU', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(19, '3471020908120001', '3471021612950001', 'MUHAMMAD SYA''BANI', 9, 43, 'GURU', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(20, '3471020908120001', '3471024705530001', 'SUTINAH', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(21, '3471023003040045', '3471025811680001', 'SRI ISWAHYUNI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(22, '3471023003040045', '3471020406900001', 'RAISE BARA ISWANTO', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(23, '3471023003040045', '3471026909920001', 'LIA ANGGRAENY PURWANDARI', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(24, '3471023003040045', '3471024211940001', 'RESTY ADELIA NOVITASARI', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(25, '3471023003040045', '3471025912030001', 'NATHALIA TRISNAWATI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(26, '3471021105070160', '3471021208820001', 'MOH ZAINI', 9, 43, 'PEDAGANG', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(27, '3471021105070160', '3471025203860002', 'HERMIN MULYANTI', 9, 43, 'PEDAGANG', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(28, '3471021105070160', '3471020210060001', 'ACHMAD ILHAMSYAH', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(29, '3471021105070160', '3471021707090002', 'MUHAMMAD RAFFLI', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(30, '3471021105070160', '3471026708140001', 'CINTA AINUN NUR HASANAH', 9, 43, 'BELUM', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(31, '3471021501972575', '3471022501600002', 'HARYONO', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(32, '3471021501972575', '3471025908630002', 'SULASTRI', 9, 43, 'IBU RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(33, '3471021501972575', '3471022305800001', 'EKO SULAKSONO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(34, '3471021501972575', '3471022502960001', 'PETRUS SULAKSONO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(35, '3471021501972575', '3471020204920002', 'AYUB SULAKSONO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(36, '3471022010050313', '3471020709730000', 'TRI RUWARNO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(37, '3471022010050313', '3471025008800001', 'RETNO FITRIANI', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(38, '3471022010050313', '3471020401030000', 'FIRMAN ENGGAR YANUARDI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(39, '3471022010050313', '3471024510050001', 'TIARA RAMADHANI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(40, '3471020209080377', '3471022505800002', 'JOKO SURYANTO, S.Pd.M.HUM', 9, 43, 'PNS', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(41, '3471020209080377', '3404106502870003', 'FLORENTINAFEBRIWIYANTIKA', 9, 43, '', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(42, '3471020209080377', '3471024706090001', 'SABRINA IRMA SURYAPUTRI', 9, 43, 'BELUM', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(43, '3471022602050056', '3471026309690002', 'RR SEPTI SRI REJEKI', 9, 43, 'PNS', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(44, '3471022602050056', '3471024501000001', 'RAMADHANIA VINCA ROSIANTY', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(45, '3471022602050056', '3471025507060001', 'ALZAINA SATIVA MAHIDARA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(46, '3471022602050056', '3471025010080003', 'INTIHA MIRABILIA SAQIFA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(47, '3471021102140001', '3471021501730002', 'SETYO KARYAWAN', 9, 43, 'TIDAK BEKERJA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(48, '3471021102140001', '3471021111740001', 'SETYO PRANYOTO', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(49, '3471021102140001', '3471020700760004', 'SETYO WIKANTONO', 9, 43, 'TIDAK BEKERJA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(50, '3471020409981197', '3471020301700001', 'BOWO NURCAHYO HADI ', 9, 43, 'PNS', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(51, '3471020409981197', '3471024903780003', 'LENI SETYOWATI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(52, '3471020409981197', '3471026807980001', 'RIBKA LISTYANI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(53, '3471020409981197', '3471022803000002', 'MIKA PRASETYA AJI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(54, '3471020409981197', '3471021608050001', 'VIEKO YUSTIAWAN', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(55, '3471020409981197', '3471025608050001', 'VIEKE YUSTIAWATI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(56, '3471020409981197', '3471024904070001', 'ESTER CINDY PRATIWI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(57, '3471020409980043', '3471027112460016', 'SUMILAH', 9, 43, 'PEDAGANG', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(58, '3471020409980043', '3471021111630001', 'ISMU JOKO SUROYO', 9, 43, 'TIDAK BEKERJA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(59, '3471020409980043', '3471026803720001', 'ENDANG SULISTIYAWATI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(60, '3471020812110002', '3471022307760001', 'TRIAWAN YULIARDI', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(61, '3471020812110002', '3475045409840015', 'EPI SUSANTI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(62, '3471020812110002', '3471020808120002', 'SATRIA BINTANG LELAKIQUE', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(63, '3471020409990439', '3471020704560003', 'ACHMAD  ', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(64, '3471020409990439', '3471026112600002', 'MARYAM', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(65, '3471020409990439', '3471020503840001', 'MOH BESIRI', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(66, '3471020409990439', '3527074503860003', 'MUTMAINAH', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(67, '3471020409990439', '3471021003070002', 'MOHAMMAD FARIDI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(68, '3471020409990439', '3471026908110001', 'FITRIA RAMADHANI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(69, '3471020409990548', '3471020810770002', 'DWI WINARTO', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(70, '3471020409990548', '3471025404820002', 'RIPAH', 9, 43, 'BURUH', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(71, '3471020409990548', '3471025910990001', 'DIFA INAWATI ALMARATUS SHOLIHAH', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(72, '3471021501972075', '3471027112470018', 'NGADIYEM', 9, 43, 'BURUH', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(73, '3471020409990079', '3471022309730001', 'SUMARTONO', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(74, '3471020409990079', '3471024702800004', 'ERNAWATI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(75, '3471020409990079', '3471024805000001', 'MELANIA ADHINATA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(76, '3471020409990079', '3471021009060001', 'TITES ESTATAMA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(77, '3471020409990079', '3471024406130001', 'ALIFA ZAHRA ADHINATA', 9, 43, 'BELUM', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(78, '3471022001060029', '3471020502750001', 'DIDIK SETYAWAN', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(79, '3471022001060029', '3471026001800001', 'YAYUK NUR WIDAYATI', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(80, '3471022001060029', '3471024912050001', 'IVANIA ATHA AZZAHRA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(81, '3471022001060029', '3471021509150001', 'ALDI AZHAR ALFARIZQI', 9, 43, 'BELUM SEKOLAH', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(82, '3471021501970328', '3471021506430001', 'SLAMET', 9, 43, 'PENSIUNAN', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(83, '3471021501970328', '3471024608450001', 'MUJIATI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(84, '3471021501970328', '3471023004680001', 'ARIJADI WINARNO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(85, '3471022606070212', '3471022509710002', 'HARYANTO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(86, '3471022606070212', '3471024801680002', 'ASRI WAHYUNIASIH', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(87, '3471022606070212', '3471025805040001', 'MAYRISKA ELIANA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(88, '3471022606070212', '3471025908070002', 'ELISHA NOVARETTA DEWI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(89, '3471021809140004', '3471023003880002', 'ARYO AJI BASKORO', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(90, '3471021809140004', '3274026708950004', 'KIKI ANGGRAENI', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(91, '3471021501972875', '3471020101610001', 'MINARTO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(92, '3471021501972875', '3471024612630001', 'SRI PRAPTI LESTARI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(93, '3471021501972875', '3471020206810001', 'YUNI HARDI KRISTIANTO', 9, 43, 'HONORER', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(94, '3471020409010278', '3471021712650002', 'R SLAMET MULYO NUGROHO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(95, '3471020409010278', '3471025007780002', 'SENIATI', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(96, '3471020409010278', '3471026203000001', 'MARGARETHA VINTYAS KINANTI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(97, '3471022207090246', '3515182901810001', 'MUHAMMAD SHOFIAN', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(98, '3471022207090246', '3471024305820001', 'ARICHA YULIANTINA, S.Pd', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(99, '3471022207090246', '3471023011090001', 'ALVINO GERALD YUDHATAMA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(100, '3471022207090246', '3471024707140001', 'ARCILIA ZAFIRA RENATA ABIGAIL', 9, 43, '', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(101, '3471021501970893', '3471025011490001', 'SAMINEM', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(102, '3471021501970893', '3471026406740001', 'UMI KUSRINIATUN', 9, 43, 'PEKERJAAN LAINNYA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(103, '3471022204140004', '3403010605920002', 'FENDI ADIANA CANDRA SAPUTRA', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(104, '3471022204140004', '3471026010900001', 'RINA KUSHANDAYANI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(105, '3471022204140004', '3471024206140001', 'CAROLIN FENDRINA YOANNA', 9, 43, 'BELUM', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(106, '3471022204140004', '3471022203160001', 'ANDREW JONATHAN FENDRINO', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(107, '3471022506120007', '3471021905700001', 'BERNADUS RADJGUKGUK', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 3, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(108, '3471022506120007', '3471026906720001', 'LINI HERMINI ASRI', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(109, '3471020610110001', '3402161705950002', 'HERI KISWANTO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(110, '3471020610110001', '3471025910830004', 'SEKAR CHANDRA SARI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(111, '3471020610110001', '3471020811110001', 'RADITYA PUTRA PRABASWORO', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(112, '3471020409990394', '3471022812600002', 'MUJI HARYADI', 9, 43, 'PNS', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(113, '3471020409990394', '3471026503630002', 'SARSIYANI', 9, 43, 'PEDAGANG', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(114, '3471020409990394', '3471025102850001', 'ARUM WIDYANINGGAR', 9, 43, 'GURU', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(115, '3471020409990394', '3471025710900002', 'AYU OKTAVIANI', 9, 43, 'GURU', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(116, '3471020409990394', '3471026806970001', 'PUTRI PURBOWATI', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(117, '3471021501970561', '3471027112260007', 'SUMIDJEM', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(118, '3471021207050195', '3471022205750002', 'ALFANI MURIZA', 9, 43, 'PNS', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(119, '3471021207050195', '3471025212800001', 'DESY TRI WAHYUNI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(120, '3471021207050195', '3471024405050001', 'ARUNDAYA PUTRI ALDY', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(121, '3471021207050195', '3471024801100003', 'KIRANA MAHARANI PUTRI ALDY', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(122, '3471021405120001', '3471012710980001', 'SUKIYAT', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(123, '3471021405120001', '3324155502870001', 'LILIS MUKAROMAH', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(124, '3471021405120001', '3471023004720001', 'FATIH ABDURROHMAN', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(125, '3471021405120001', '3471020508150001', 'FAJAR AL AYYUBI', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(126, '3471020905080190', '3471022007800003', 'TRI WINARNO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(127, '3471020905080190', '3471026908800001', 'EVA RIYANTI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(128, '3471020905080190', '3471020301070002', 'DZAKY AHMAD KHOIRUDDIN IYAS', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(129, '3471020905080190', '3471022409080001', 'AZFAR AHMAD RAMADHAN', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(130, '3471020905080190', '3471024704160001', 'NAFIZA AZMYA FARZANA', 9, 43, 'BELUM', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(131, '3471021501971915', '3471021104690001', 'KRISTANTO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(132, '3471021501971915', '3471024912760002', 'SRI WAHYU LESTARI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(133, '3471021501971915', '3471024301970001', 'CHRISTINA EKA LESTARI', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(134, '3471021501971915', '3471025212970003', 'MAGDALENA DESTA LESTARI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(135, '3471021501971915', '3471024601130001', 'KHEYLA AMOORA', 9, 43, 'BELUM', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(136, '3471022208960927', '3471024612590001', 'SUPRI HARTATI', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(137, '3471022208960927', '3471024502850001', 'NIA FEBRI ASTA NURANI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(138, '3471022208960927', '3471026012910001', 'DETA DWI ARYANI', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(139, '3471020409980165', '3471025010570001', 'MINARNI', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(140, '3471020409980165', '3471027103850001', 'SITI CHORIAH', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(141, '3471020409980870', '3471025010550003', 'YUPI AISYAH', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(142, '3471020409980870', '3471021909680002', 'SUPRIHATNO', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(143, '3471020110990746', '3471022310600002', 'WIDODO', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(144, '3471020110990746', '3471026810890001', 'ANIS RESTU SARTIKA', 9, 43, 'BURUH', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(145, '3471022901050027', '3471021808760002', 'SUHANA', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(146, '3471022901050027', '''3471025209820002', 'SRI WATINI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(147, '3471022901050027', '3471025402060001', 'NUR RAHMA IRSANAH', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(148, '3471022901050027', '3471024406090001', 'REGINA DWI CAHYA IRSANAH', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(149, '3471022208961206', '3471020205560001', 'AGUS SUROSO', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(150, '3471022208961206', '3471024405590001', 'SUTINI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(151, '3471022208961206', '3471020606880001', 'SONY ARIF ANGGORO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(152, '3471022712020408', '3471022603720001', 'SAPARDI ATMOKO', 9, 43, 'KARYAWAN BUMN', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(153, '3471022712020408', '3471025101730002', 'SURATINAH', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(154, '3471022712020408', '3471021011960001', 'LUQMAN HANAFI', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(155, '3471022712020408', '3471025412020001', 'TAZKIYATUNNAFSIYATI ZUHROWIYAH', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(156, '3471021511060366', '3471021505630002', 'SUTIKNO', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(157, '3471021511060366', '3471025508680001', 'WARIYEM', 9, 43, 'BURUH', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(158, '3471021511060366', '3471022706920002', 'WAHYU CANDRA IRAWAN', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(159, '3471021511060366', '3471024407000001', 'YULIANA PRATIWI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(160, '3471020409010357', '3471021107680001', 'BAMBANG YULIS PRIYAMBODO', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 3, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(161, '3471020409010357', '3471025103660001', 'RINTIH KUSSULAMTI CONSTANTINA', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 3, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(162, '3471020409010357', '3471022803910001', 'BONAVENTURA RANDY INKA PERMANA', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 3, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(163, '3471020409010357', '3471022211010001', 'BONAFICIUS EDO WISNU WASKITA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 3, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(164, '3471012301970890', '3471014702560001', 'SULARMI', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(165, '3471012301970890', '3471011909850001', 'DAMAR PRIAMBODO', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(166, '3471022208960361', '3471027112360007', 'SRI SUPRAPTI GREGORIA', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 3, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(167, '3471022208960361', '3471024103700001', 'TITIK KUSSUMARTI YOSEPHIN', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(168, '3471022208960361', '3471026603720002', 'INDAH KUSSUSAPTI', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'wanita', 3, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(169, '3471022208960361', '3471021306750001', 'LANJAR KUSSUHASTO', 9, 43, 'KARY SWASTA', '0000-00-00', 1, 'pria', 3, '943', 'ibu', 'jogja', 'jogja', 'meninggal', '0000-00-00 00:00:00'),
(170, '3471022208960361', '3471025112980001', 'GRACE SHEILA KUSS TANIA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(171, '3471022208960361', '3471025103010001', 'DOMINIC NAOMI JOVANKA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(172, '3471020302960782', '3471022204450001', 'MARSUDI NOTOWIYONO', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(173, '3471020302960782', '3471027112630002', 'WINARTI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(174, '3471022708150001', '3471025912550002', 'SUGIARSIH', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(175, '3471022708150001', '3471021405810004', 'ABADIE TRI PRASETYO', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(176, '3471022708150001', '3471020711850001', 'YUDHA ARDIAN PRATISTA', 9, 43, 'PELAUT', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(177, '3471022208960717', '3471026410670001', 'SRI LESTARI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(178, '3471022208960717', '3471020410000001', 'EFWANINDYA RAUSYANFIKRI', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(179, '3471021005070158', '3471022507700002', 'JEFTA SINANU, SE', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(180, '3471021005070158', '3471024805750001', 'SRI UTAMI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(181, '3471021005070158', '3471022801970002', 'JANUAR ADI BUDI PRAKASA', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(182, '3471021005070158', '3471022411030001', 'ELDO DWI NASTATA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(183, '3471021005070158', '3471026411030002', 'ELENA DWI NASTITI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(184, '3471021005070158', '3471020405050001', 'MOSSES TRI NOOR SASONGKO', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(185, '3471020409980768', '3471020306620001', 'NUR ALI BARI', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(186, '3471020409980768', '3471020811920003', 'ANGGAVISCA NUR ALI', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(187, '3471020409980768', '3471022005980002', 'SATRIO MULYO NUR REFORMASI', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(188, '3471022808080374', '3471026712660001', 'ROSIAH', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(189, '3471022808080374', '3471024811890001', 'YULI ELIA ROSDIANA', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(190, '3471022808080374', '3471026702950001', 'YOSI ROSITA DEWI', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(191, '3471022808080374', '3471020310030003', 'MOHAMMAD YOGA ROSADI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(192, '3471022403140002', '9171032210710002', 'DRS JAINGOT ANGGIAT PARHUSIP', 9, 43, 'PNS', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(193, '3471022403140002', '9171034106770004', 'ENDANG SIMAREMARE', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(194, '3471022403140002', '9171031912020005', 'MICHAEL LAZWARD PARHUSIP', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(195, '3471022403140002', '9171031401050003', 'MARVEL LUCENT PARHUSIP', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(196, '3471022403140002', '9171034403030001', 'MERCY LUCIANA PARHUSIP', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(197, '3471022403140002', '9171035203100002', 'GYTHA SHIRA ELISIA PARHUSIP', 9, 43, 'BELUM', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(198, '3471021501971188', '3471025512540001', 'RR E SRI KADARISMIYATI', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(199, '3471021501971188', '3471024503550002', 'RR A DWI SRI RAHAYU', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'meninggal', '0000-00-00 00:00:00'),
(200, '3471021501971188', '3471020604620001', 'RM SLAMET SUDARSONO', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(201, '3471020409990409', '3471021004680002', 'OTO HARJANTO', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(202, '3471020409990409', '3471025304710003', 'WELAS SUGIYANTI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(203, '3471020409990409', '3471021110950003', 'JODI DARMAWAN', 9, 43, 'KARYAWAN', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(204, '3471020409990409', '1105011606880005', 'MUHAMMAD IRFAN FAJAR', 9, 43, '', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(205, '3471020409980655', '3471022808500001', 'PONIDJO', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(206, '3471020409980655', '3471024109520001', 'SUMANDARYATI', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(207, '3471020409980655', '3471025211850001', 'YANTI LESTARI', 9, 43, 'TIDAK BEKERJA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(208, '3471020409980655', '3471020704990001', 'FEBRIANDIKA ANJASMARA A', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(209, '3471022208960631', '3471021201620001', 'MUJI HARI PRASETYA', 9, 43, 'PNS', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(210, '3471022208960631', '3471026702640001', 'SUPRIYATI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(211, '3471022208960631', '3471020807890001', 'PANDU GIRI PANGESTU', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(212, '3471022208960631', '3471020909960002', 'DIMAS RACHMAT PAMULAT', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(213, '3471022208960631', '3471024707990001', 'NOVIA RESTU PRAMESTI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(214, '3471022208960631', '3471025304020001', 'DIAH AYU PRAMUDYANINGTYAS', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(215, '3471022712970242', '3471027112460006', 'SUTILAH', 9, 43, 'BURUH', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(216, '3471022801110006', '3471020411840001', 'NOVIANTO HARI WIBOWO', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(217, '3471022801110006', '3471115812860001', 'TENY HAPSARI', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(218, '3471022801110006', '3471022603130001', 'ARKANA NANDANA WIBOWO', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(219, '3471022208961745', '3471021306640002', 'SUNARYO, DRS', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(220, '3471022208961745', '3471024604750001', 'SITI MUTMAINAH', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(221, '3471022208961745', '3471027011960001', 'VEBI AYU ARTHA ARTIKA', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(222, '3471022208961745', '3471024207030002', 'NIKI NIKITA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(223, '3471022208961745', '3471020907080003', 'BAGUS LINDU AJI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(224, '3471022208961745', '3471021201100001', 'RICHI RIDHO ILLAHI', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(225, '3471020409980848', '3471022708650003', 'MOHAMMAD AGUS SANTOSO', 9, 43, 'TIDAK BEKERJA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(226, '3471020409980848', '3471024412740001', 'HENI PURWANI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(227, '3471020409980848', '3471025610960002', 'CHIKA AWANI', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(228, '3471020409980848', '3471020701040001', 'HELGA KENT PRINCEKU', 9, 43, 'PELAJAR', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(229, '3471020409980848', '3471020701040001', 'HELEN KENT RATULAGI', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(230, '3471020409980848', '3471026910050001', 'JOANE KENT OCKTAVIONA PASSA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(231, '3471021907130003', '3471021810820001', 'WAHYU SAPUTRA', 9, 43, 'KARYAWAN SWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(232, '3471021907130003', '3372016309830002', 'ROOSATI', 9, 43, 'PNS', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(233, '3471021907130003', '3471021606130001', 'ALI FAJAR SADAM SAPUTRA', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(234, '3471021907130003', '3471020806150001', 'ZUBAIR AZZAM SAPUTRA', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(235, '3471020502070053', '3471021701760002', 'HARIYANTO SE', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(236, '3471020502070053', '3471026303830001', 'NOK AMELIA', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(237, '3471020502070053', '3471020810050001', 'PASHA OCTA PERDANA', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(238, '3471020502070053', '3471020603080001', 'SYAHADANIAL HAQH', 9, 43, 'BELUM', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(239, '3471020502070053', '3471022502900001', 'AKHMAD ROZAK', 9, 43, '', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(240, '1', '3471017112760001', 'PAINEM', 9, 43, '', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(241, '3471022411140002', '3314122110650004', 'SUPARDI', 9, 43, '', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(242, '3471022411140002', '3314125108070004', 'GALUOH DWI PRASASTI', 9, 43, '', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(243, '3471022411140002', '3314127008090005', 'GITA RAMADHANI', 9, 43, '', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(244, '3471022401110003', '3471026304730001', 'DIANA ASPRIANTI', 9, 43, 'BURUH', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(245, '3471022401110003', '3471026812010001', 'DELVINA ADELIA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(246, '8101141608110007', '8101140610750005', 'OLDWEIN E PUPELLA', 9, 43, 'PENDETA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(247, '8101141608110007', '8101146210790002', 'VANNY SUITELA', 9, 43, 'DOSEN', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(248, '8101141608110007', '8101146403010002', 'ABDIELA E T A PUPULLA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(249, '8101141608110007', '8101146506040004', 'PRICESO .. R S PUPELLA', 9, 43, 'PELAJAR', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(250, '8101141608110007', '8101143011450002', 'JACOB SUITELA', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(251, '3471012301970154', '3471011702620001', 'MARNOSUWITO', 9, 43, 'PEDAGANG', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(252, '3471012301970154', '3471017004930002', 'INDAH NUGRAHANTI', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(253, '3471012301970154', '3471016712980001', 'ANISA SETYANINGRUM', 9, 43, 'MAHASISWA', '0000-00-00', 1, 'wanita', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(254, '3471020409981542', '3471023112550013', 'SUTRISNO', 9, 43, 'BURUH', '0000-00-00', 1, 'pria', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(255, '3471020409981542', '3471026406570001', 'SUYATI', 9, 43, 'MENGURUS RUMAH TANGGA', '0000-00-00', 1, 'wanita', 2, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(256, '3471021701170002', '3527071508940004', 'UNTUNG', 9, 43, 'WIRASWASTA', '0000-00-00', 1, 'pria', 1, '943', 'ibu', 'jogja', 'jogja', 'hidup', '0000-00-00 00:00:00'),
(257, '3471020801020415', '347102080102041512', 'Bapak baru banget', 9, 43, 'POLISI', '1980-09-24', 0, 'pria', 1, '', 'bapak', 'otomatis nanti', 'mataram cinta', 'hidup', '2019-09-09 11:19:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_warga_pendatang`
--

CREATE TABLE `tbl_warga_pendatang` (
  `id` int(11) NOT NULL,
  `no_kk` char(25) NOT NULL,
  `nik` char(25) NOT NULL,
  `nama_warga` varchar(150) NOT NULL,
  `no_rw` int(11) NOT NULL,
  `no_rt` int(11) NOT NULL,
  `pekerjaan` varchar(150) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `status_keluarga` int(11) NOT NULL,
  `jenis_kelamin` enum('pria','wanita') NOT NULL,
  `id_agama` int(11) NOT NULL,
  `kode_rt` char(20) NOT NULL,
  `status` char(20) NOT NULL,
  `alamat` text NOT NULL,
  `domisili` text NOT NULL,
  `status_hidup` enum('meninggal','hidup') NOT NULL DEFAULT 'hidup'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_warga_pendatang`
--

INSERT INTO `tbl_warga_pendatang` (`id`, `no_kk`, `nik`, `nama_warga`, `no_rw`, `no_rt`, `pekerjaan`, `tanggal_lahir`, `status_keluarga`, `jenis_kelamin`, `id_agama`, `kode_rt`, `status`, `alamat`, `domisili`, `status_hidup`) VALUES
(1, '123123123123123', '123123123123', 'burhan', 9, 43, 'Wiraswasta', '2019-09-28', 0, 'pria', 1, '', 'bapak', 'otomatis nanti', 'jogja matarman', 'hidup');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_wisata`
--

CREATE TABLE `tbl_wisata` (
  `id` int(11) NOT NULL,
  `nama_wisata` varchar(150) NOT NULL,
  `keterangan` text NOT NULL,
  `jenis_wisata` varchar(150) NOT NULL,
  `foto_wisata` varchar(150) NOT NULL,
  `tempat` text NOT NULL,
  `kode_rt` char(20) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_wisata`
--

INSERT INTO `tbl_wisata` (`id`, `nama_wisata`, `keterangan`, `jenis_wisata`, `foto_wisata`, `tempat`, `kode_rt`, `create_at`) VALUES
(1, 'malioboro', 'sepanjang jalan', 'budaya', '98e52a6d3e8e24414259c00f3e27e9db.png', 'mall malioboro', '', '2019-08-27'),
(2, 'tugu', 'wisata masyarak', 'budaya', 'ae79062f3e3a1b6f09983b93a003d271.jpg', 'tugu', '943', '2019-09-27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_wisata_rw`
--

CREATE TABLE `tbl_wisata_rw` (
  `id` int(11) NOT NULL,
  `nama_wisata` varchar(150) NOT NULL,
  `keterangan` text NOT NULL,
  `jenis_wisata` varchar(150) NOT NULL,
  `foto_wisata` varchar(150) NOT NULL,
  `tempat` text NOT NULL,
  `kode_rw` char(20) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_wisata_rw`
--

INSERT INTO `tbl_wisata_rw` (`id`, `nama_wisata`, `keterangan`, `jenis_wisata`, `foto_wisata`, `tempat`, `kode_rw`, `create_at`) VALUES
(2, 'cintaku adalah cintamu ku', 'ku', 'pendidikan', 'beb6d71136494ddaac6928f1d9e6d0cd.png', 'ku', '1', '2019-08-27 00:00:00'),
(3, 'Kali code', 'Kali code', 'air', 'b81f981402c30424d0a0a6f1e063a70a.jpg', 'Kali code', '9', '2019-09-27 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin_rt`
--
ALTER TABLE `tbl_admin_rt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admin_rw`
--
ALTER TABLE `tbl_admin_rw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_agama`
--
ALTER TABLE `tbl_agama`
  ADD PRIMARY KEY (`id_agama`);

--
-- Indexes for table `tbl_agenda`
--
ALTER TABLE `tbl_agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_agenda_rw`
--
ALTER TABLE `tbl_agenda_rw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_akun_warga`
--
ALTER TABLE `tbl_akun_warga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_bulan`
--
ALTER TABLE `tbl_bulan`
  ADD PRIMARY KEY (`id_bulan`);

--
-- Indexes for table `tbl_galeri`
--
ALTER TABLE `tbl_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_galeri_rw`
--
ALTER TABLE `tbl_galeri_rw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_inventori_barang`
--
ALTER TABLE `tbl_inventori_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_inventori_barang_rw`
--
ALTER TABLE `tbl_inventori_barang_rw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kas`
--
ALTER TABLE `tbl_kas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kas_keluar`
--
ALTER TABLE `tbl_kas_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kas_masuk`
--
ALTER TABLE `tbl_kas_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kematian`
--
ALTER TABLE `tbl_kematian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_lapor_kejadian`
--
ALTER TABLE `tbl_lapor_kejadian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pekerjaan`
--
ALTER TABLE `tbl_pekerjaan`
  ADD PRIMARY KEY (`id_pekerjaan`),
  ADD KEY `nama_pekerjaan` (`nama_pekerjaan`);

--
-- Indexes for table `tbl_superuser`
--
ALTER TABLE `tbl_superuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_warga`
--
ALTER TABLE `tbl_warga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agama` (`id_agama`),
  ADD KEY `pekerjaan` (`pekerjaan`);

--
-- Indexes for table `tbl_warga_pendatang`
--
ALTER TABLE `tbl_warga_pendatang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pekerjaan_3` (`pekerjaan`),
  ADD KEY `pekerjaan_4` (`pekerjaan`),
  ADD KEY `pekerjaan_5` (`pekerjaan`),
  ADD KEY `pekerjaan` (`pekerjaan`) USING BTREE,
  ADD KEY `id_agama` (`id_agama`) USING BTREE,
  ADD KEY `pekerjaan_2` (`pekerjaan`) USING BTREE;

--
-- Indexes for table `tbl_wisata`
--
ALTER TABLE `tbl_wisata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_wisata_rw`
--
ALTER TABLE `tbl_wisata_rw`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin_rt`
--
ALTER TABLE `tbl_admin_rt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_admin_rw`
--
ALTER TABLE `tbl_admin_rw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_agama`
--
ALTER TABLE `tbl_agama`
  MODIFY `id_agama` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_agenda`
--
ALTER TABLE `tbl_agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_agenda_rw`
--
ALTER TABLE `tbl_agenda_rw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_akun_warga`
--
ALTER TABLE `tbl_akun_warga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_bulan`
--
ALTER TABLE `tbl_bulan`
  MODIFY `id_bulan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_galeri`
--
ALTER TABLE `tbl_galeri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_galeri_rw`
--
ALTER TABLE `tbl_galeri_rw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_inventori_barang`
--
ALTER TABLE `tbl_inventori_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_inventori_barang_rw`
--
ALTER TABLE `tbl_inventori_barang_rw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_kas`
--
ALTER TABLE `tbl_kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_kas_keluar`
--
ALTER TABLE `tbl_kas_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_kas_masuk`
--
ALTER TABLE `tbl_kas_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_kematian`
--
ALTER TABLE `tbl_kematian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_lapor_kejadian`
--
ALTER TABLE `tbl_lapor_kejadian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_pekerjaan`
--
ALTER TABLE `tbl_pekerjaan`
  MODIFY `id_pekerjaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_superuser`
--
ALTER TABLE `tbl_superuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_warga`
--
ALTER TABLE `tbl_warga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `tbl_warga_pendatang`
--
ALTER TABLE `tbl_warga_pendatang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_wisata`
--
ALTER TABLE `tbl_wisata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_wisata_rw`
--
ALTER TABLE `tbl_wisata_rw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
