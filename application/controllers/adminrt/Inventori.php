<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventori extends CI_Controller {

	public function __construct(){
            parent::__construct();
                $this->load->model('m_inventori');
                $this->load->library('session');
                $this->load->library('upload');
    }
    public function index()
    {
       if ($this->session->userdata('email_rt')==null){
              $this->load->view("adminrt/login/login");
          }else{
                 
                $kode_rt = $this->session->userdata('kode_rt');
                $data['inventori_barang'] = $this->m_inventori->data_barang($kode_rt);
                $this->template->load('static', 'adminrt/inventori/v_inventori_barang', $data);
       }
    }
    public function simpan_inventori_barang(){
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $spek = $this->input->post("xspek");
                $banyak = $this->input->post("xbanyak");
                $kondisi = $this->input->post("xkondisi");
                $keterangan = $this->input->post("xketerangan");
                $kode_rt = $this->session->userdata('kode_rt');
                $query = $this->m_inventori->simpan_inventori_barang($nama, $spek, $banyak, $kondisi, $keterangan,$photo, $kode_rt);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di simpan');
                }
                redirect('adminrt/inventori/');
            }else{
                redirect('adminrt/inventori/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $spek = $this->input->post("xspek");
            $banyak = $this->input->post("xbanyak");
            $kondisi = $this->input->post("xkondisi");
            $keterangan = $this->input->post("xketerangan");
            $kode_rt = $this->session->userdata('kode_rt');
            $query = $this->m_inventori->simpan_inventori_barang_tanpa_foto($nama, $spek, $banyak, $kondisi, $keterangan, $kode_rt);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di simpan');
            }
            redirect('adminrt/inventori/');
        }

    }
    public function hapus_inventori_barang($id){
        $this->m_inventori->hapus_inventori_barang($id);
        redirect('adminrt/inventori');
    }
    public function update_inventori_barang(){
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $spek = $this->input->post("xspek");
                $banyak = $this->input->post("xbanyak");
                $kondisi = $this->input->post("xkondisi");
                $keterangan = $this->input->post("xketerangan");
                $idbarang = $this->input->post('xidbarang'); 
                $kode_rt = $this->session->userdata('kode_rt');
                $query = $this->m_inventori->update_inventori_barang($nama, $spek, $banyak, $kondisi, $keterangan,$photo, $idbarang);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di update');
                }
                redirect('adminrt/inventori/');
            }else{
                redirect('adminrt/inventori/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $spek = $this->input->post("xspek");
            $banyak = $this->input->post("xbanyak");
            $kondisi = $this->input->post("xkondisi");
            $keterangan = $this->input->post("xketerangan");
            $idbarang = $this->input->post('xidbarang'); 
            $kode_rt = $this->session->userdata('kode_rt');
            $query = $this->m_inventori->update_inventori_barang_tanpa_foto($nama, $spek, $banyak, $kondisi, $keterangan, $idbarang);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di edit');
            }
            redirect('adminrt/inventori/');
        }

    }

}