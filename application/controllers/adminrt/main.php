<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
  public function __construct(){
    parent::__construct();
        $this->load->model('m_keuangan');
        $this->load->model('m_warga');
        $this->load->library('session');
}
  public function index()
  {
     if ($this->session->userdata('email_rt')==null){
            $this->load->view("adminrt/login/login");
        }else{
          $kode_rt = $this->session->userdata('kode_rt');
          $no_rt = $this->session->userdata('no_rt');
          $no_rw = $this->session->userdata('no_rw');
          $data['kasmasuk'] = $this->m_keuangan->total_kas_masuk($kode_rt);
          $data['kaskeluar'] = $this->m_keuangan->total_kas_keluar($kode_rt);
          $data['jumlahwarga'] = $this->m_warga->jumlah_warga($no_rt, $no_rw);
          $this->template->load('static', 'adminrt/main', $data);
     
     }
  }

}
