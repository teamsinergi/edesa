<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CI_Controller {

	public function __construct(){
            parent::__construct();
                $this->load->model('m_galeri');
                $this->load->library('session');
                $this->load->library('upload');
    }
    public function index()
    {
       if ($this->session->userdata('email_rt')==null){
              $this->load->view("adminrt/login/login");
          }else{ 
                $kode_rt = $this->session->userdata('kode_rt');
                $data['galeri'] = $this->m_galeri->data_galeri($kode_rt);
                $this->template->load('static', 'adminrt/galeri/v_galeri', $data);
       }
    }
    public function simpan_galeri(){
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $judul = $this->input->post("xnama");
                $tanggal = $this->input->post("xtanggal");
                $tempat = $this->input->post("xtempat");
                $keterangan = $this->input->post("xketerangan");
                $kode_rt = $this->session->userdata('kode_rt');
                $query = $this->m_galeri->simpan_galeri($judul, $tanggal, $tempat, $keterangan,$photo, $kode_rt);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di simpan');
                }
                redirect('adminrt/galeri/');
            }else{
                redirect('adminrt/galeri/');
            }

        }else{
            $judul = $this->input->post("xnama");
            $tanggal = $this->input->post("xtanggal");
            $tempat = $this->input->post("xtempat");
            $keterangan = $this->input->post("xketerangan");
            $kode_rt = $this->session->userdata('kode_rt');
            $query = $this->m_galeri->simpan_galeri_tanpa_foto($judul, $tanggal, $tempat, $keterangan, $kode_rt);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di simpan');
            }
            redirect('adminrt/galeri/');
        }

    }
    public function hapus_galeri($id){
        $this->m_galeri->hapus_galeri($id);
        redirect('adminrt/galeri');
    }
    public function update_galeri(){
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $judul = $this->input->post("xnama");
                $tanggal = $this->input->post("xtanggal");
                $tempat = $this->input->post("xtempat");
                $keterangan = $this->input->post("xketerangan");
                $kode_rt = $this->session->userdata('kode_rt');
                $id = $this->input->post("xid");
                $query = $this->m_galeri->update_galeri($judul, $tanggal, $tempat, $keterangan,$photo, $id);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di update');
                }
                redirect('adminrt/galeri/');
            }else{
                redirect('adminrt/galeri/');
            }

        }else{
            $judul = $this->input->post("xnama");
            $tanggal = $this->input->post("xtanggal");
            $tempat = $this->input->post("xtempat");
            $keterangan = $this->input->post("xketerangan");
            $kode_rt = $this->session->userdata('kode_rt');
            $id = $this->input->post("xid");
            $query = $this->m_galeri->update_galeri_tanpa_foto($judul, $tanggal, $tempat, $keterangan, $id);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di edit');
            }
            redirect('adminrt/galeri/');
        }

    }

}