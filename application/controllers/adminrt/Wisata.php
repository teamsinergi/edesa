<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wisata extends CI_Controller {

	public function __construct(){
            parent::__construct();
                $this->load->model('m_galeri');
                $this->load->model('m_wisata');
                $this->load->library('session');
                $this->load->library('upload');
    }
    public function index()
    {
       if ($this->session->userdata('email_rt')==null){
              $this->load->view("adminrt/login/login");
          }else{ 
                $kode_rt = $this->session->userdata('kode_rt');
                $data['wisata'] = $this->m_wisata->data_wisata($kode_rt);
                $this->template->load('static', 'adminrt/wisata/v_wisata', $data);
       }
    }
    public function simpan_wisata(){
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $jenis = $this->input->post("xjenis");
                $tempat = $this->input->post("xtempat");
                $keterangan = $this->input->post("xketerangan");
                $kode_rt = $this->session->userdata('kode_rt');
                $query = $this->m_wisata->simpan_wisata($nama, $jenis, $tempat, $keterangan,$photo, $kode_rt);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di simpan');
                }
                redirect('adminrt/wisata/');
            }else{
                redirect('adminrt/wisata/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $jenis = $this->input->post("xjenis");
            $tempat = $this->input->post("xtempat");
            $keterangan = $this->input->post("xketerangan");
            $kode_rt = $this->session->userdata('kode_rt');
            $query = $this->m_wisata->simpan_wisata_tanpa_foto($nama, $jenis, $tempat, $keterangan, $kode_rt);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di simpan');
            }
            redirect('adminrt/wisata/');
        }

    }
    public function hapus_wisata($id){
        $this->m_wisata->hapus_wisata($id);
        redirect('adminrt/wisata');
    }
    public function update_wisata(){
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $jenis = $this->input->post("xjenis");
                $tempat = $this->input->post("xtempat");
                $keterangan = $this->input->post("xketerangan");
                $id = $this->input->post("xid");
                $query = $this->m_wisata->update_wisata($nama, $jenis, $tempat, $keterangan,$photo, $id);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di update');
                }
                redirect('adminrt/wisata/');
            }else{
                redirect('adminrt/wisata/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $jenis = $this->input->post("xjenis");
            $tempat = $this->input->post("xtempat");
            $keterangan = $this->input->post("xketerangan");
            $id = $this->input->post("xid");
            $query = $this->m_wisata->update_wisata_tanpa_foto($nama, $jenis, $tempat, $keterangan, $id);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di edit');
            }
            redirect('adminrt/wisata/');
        }

    }

}