<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller{
    public function __construct(){
        parent::__construct();
            $this->load->helper('url');
            $this->load->library('Session');
            $this->load->model('m_warga');
            $this->load->model('m_agenda');
            $this->load->library('upload');
    }
    public function index()
    {
        $kode_rt = $this->session->userdata('kode_rt');
        echo $kode_rt;
        $data['agenda'] = $this->m_agenda->agenda($kode_rt);
        $this->template->load('static', 'adminrt/agenda/v_agenda', $data);
    }    
    public function update_agenda(){
        // $nama = $this->input->post("xnama");
        // $keterangan = $this->input->post("xketerangan");
        // $tanggal = $this->input->post("xtanggal");
        // $tempat = $this->input->post("xtempat");
        // $jam = $this->input->post("xjam");
        $id = $this->input->post("xid");
        
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $keterangan = $this->input->post("xketerangan");
                $tanggal = $this->input->post("xtanggal");
                $tempat = $this->input->post("xtempat");
                $jam = $this->input->post("xjam");
                $kode_rt = $this->session->userdata('kode_rt');
                $query = $this->m_agenda->update_agenda($nama, $keterangan, $tanggal, $tempat, $jam, $id, $photo);
                if($query){
                    $this->session->set_flashdata('pesan','Agenda berhasil di simpan');
                }
                redirect('adminrt/agenda/');
            }else{
                redirect('adminrt/agenda/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $keterangan = $this->input->post("xketerangan");
            $tanggal = $this->input->post("xtanggal");
            $tempat = $this->input->post("xtempat");
            $jam = $this->input->post("xjam");
            $kode_rt = $this->session->userdata('kode_rt');
            $query = $this->m_agenda->update_agenda_tanpa_foto($nama, $keterangan, $tanggal, $tempat, $jam, $id);
            if($query){
                $this->session->set_flashdata('pesan','Agenda berhasil di simpan');
            }
            redirect('adminrt/agenda/');
        }
        
        redirect('adminrt/agenda');
    }
    public function hapus_agenda($id){
        $this->m_agenda->hapus_agenda($id);
        redirect("adminrt/agenda");
    }

    public function simpan_agenda(){
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $keterangan = $this->input->post("xketerangan");
                $tanggal = $this->input->post("xtanggal");
                $tempat = $this->input->post("xtempat");
                $jam = $this->input->post("xjam");
                $kode_rt = $this->session->userdata('kode_rt');
                $query = $this->m_agenda->simpan_agenda($nama, $keterangan, $tanggal, $tempat, $jam, $kode_rt,$photo);
                if($query){
                    $this->session->set_flashdata('pesan','Agenda berhasil di simpan');
                }
                redirect('adminrt/agenda/');
            }else{
                redirect('adminrt/agenda/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $keterangan = $this->input->post("xketerangan");
            $tanggal = $this->input->post("xtanggal");
            $tempat = $this->input->post("xtempat");
            $jam = $this->input->post("xjam");
            $kode_rt = $this->session->userdata('kode_rt');
            $query = $this->m_agenda->simpan_agenda_tanpa_foto($nama, $keterangan, $tanggal, $tempat, $jam, $kode_rt);
            if($query){
                $this->session->set_flashdata('pesan','Agenda berhasil di simpan');
            }
            redirect('adminrt/agenda/');
        }
    }


}

?>