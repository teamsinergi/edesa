<?php
class Testing extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_warga');
		error_reporting(0);
	}

	function index(){
		$this->load->view('blog_view');
	}

	function get_autocomplete(){
		if (isset($_GET['term'])) {
		  	$result = $this->m_warga->cari_warga($_GET['term']);
		   	if (count($result) > 0) {
		    foreach ($result as $row)
		     	$arr_result[] = array(
					'label'			=> $row->nik,
					'description'	=> $row->nama_warga,
				);
		     	echo json_encode($arr_result);
		   	}
		}
	}

}