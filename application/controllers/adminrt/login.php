<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	 public function __construct(){

            parent::__construct();
                $this->load->model('M_login');
                $this->load->library('session');
    }

    public function index(){

  	 if ($this->session->userdata('email_rt')==null){  
            $this->load->view("adminrt/login/login"); 
        }else{
            redirect('adminrt/main');
     }

    //$this->load->view('code/login');
  }

     function login_admin(){

        $user_login=array(
        'email_rt'=>$this->input->post('email_rt'),
        'password'=>$this->input->post('password'));

            $data=$this->M_login->login_admin($user_login['email_rt'],$user_login['password']);

            if($data)
            {
                $this->session->set_userdata('id',$data['id']);
                $this->session->set_userdata('email_rt',$data['email_rt']);
                $this->session->set_userdata('password',$data['password']);
                $this->session->set_userdata('kode_rt',$data['kode_rt']);
                $this->session->set_userdata('no_rt',$data['no_rt']);
                $this->session->set_userdata('no_rw',$data['no_rw']);
                	//<?php echo $this->session->userdata('email_rt');
                
                redirect('adminrt/main');
            }
            else{

                $this->session->set_flashdata('error_msg', 'Gagal.! Email Atau Password Salah');
                $this->load->view("adminrt/login/login");

            }

       }

      public function user_logout(){
          $this->session->sess_destroy();
          redirect('adminrt/login', 'refresh');
      }
      
      public function add(){
        $this->load->view("adminrw/loginrw/v_kelolah_admin");
    }

      public function simpan_admin(){

        $nama_rt = $this->input->post("xnama_rt");
        $email_rt = $this->input->post("xemail_rt");
        $password = $this->input->post("xpass");
        $this->M_login->simpan_admin($nama_rt, $email_rt, $password);
        $this->session->set_flashdata('success', 'Berhasil disimpan');

        redirect('adminrt/login/');
    }

    public function admin()
    {
        $id = $this->session->userdata('id');
        $data['admin'] = $this->M_login->admin($id);
        $this->template->load('static', 'adminrw/loginrw/v_kelolah_admin', $data);
    }

}