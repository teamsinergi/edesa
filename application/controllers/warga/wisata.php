<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wisata extends CI_Controller {

  public function __construct(){
            parent::__construct();
                $this->load->model('m_galeri');
                $this->load->model('m_wisata');
                $this->load->library('session');
                $this->load->library('upload');
    }
    public function index()
    {
                $kode_rt = $this->session->userdata('kode_rt');
                $data['wisata'] = $this->m_wisata->data_wisata($kode_rt);
                $this->load->view('warga/wisata/v_wisata', $data);
    }
   
    

}