<?php 

class Pengaduan extends CI_Controller{
 
    function __construct()
	{
		 parent::__construct();
                $this->load->model('M_login_warga');
                $this->load->model('m_pengaduan');
                $this->load->library('session');
                $this->load->library('upload');
    }

    public function simpan_pengaduan(){
        $norw = $this->session->userdata('norw');
        $nort = $this->session->userdata('nort');
        $nik = $this->session->userdata('nik');

        if($nik = null || $nort == null || $norw == null){
            redirect('warga/login_warga');
        }else{
            $config['upload_path'] = './assets/images/'; //path folder
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
            $this->upload->initialize($config);
            if(!empty($_FILES['filefoto']['name']))
            {
                if ($this->upload->do_upload('filefoto'))
                {
                    $gbr = $this->upload->data();
    
                    $config['image_library']='gd2';
                    $config['source_image']='./assets/images/'.$gbr['file_name'];
                    $config['create_thumb']= FALSE;
                    $config['maintain_ratio']= FALSE;
                    $config['quality']= '50%';
                    // $config['width']= 300;
                    // $config['height']= 300;
                    $config['new_image']= './assets/images/'.$gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    
                    $photo=$gbr['file_name'];
                    $keterangan = $this->input->post("xketerangan");
                    $tempat = $this->input->post("xtempat");
                    $tanggal = $this->input->post("xtanggal");
                    $jam = $this->input->post("xjam");
                    $create_at = date('Y-m-d');
                    $nik = $this->session->userdata('nik');
                    $data = array(
                        'nama_kejadian' =>$keterangan,
                        'tempat_kejadian' => $tempat,
                        'foto_kejadian' => $photo,
                        'jam' => $jam,
                        'norw' => $norw,
                        'nort' => $nort,
                        'nik' =>  $nik,
                        'create_at' => $create_at
                    );
                    $query = $this->m_pengaduan->simpan_pengaduan($data);
                    if($query){
                        $this->session->set_flashdata('pesan','data berhasil di simpan');
                    }
                    redirect('warga/main/');
                }else{
                    redirect('warga/main/');
                }
    
            }else{
                $judul = $this->input->post("xnama");
                $tanggal = $this->input->post("xtanggal");
                $tempat = $this->input->post("xtempat");
                $keterangan = $this->input->post("xketerangan");
                $kode_rt = $this->session->userdata('kode_rt');
                $query = $this->m_galeri->simpan_galeri_tanpa_foto($judul, $tanggal, $tempat, $keterangan, $kode_rt);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di simpan');
                }
                redirect('adminrt/galeri/');
            }
        }
    }
    
}

?>