<?php
class Login_warga extends CI_Controller
{
	
	function __construct()
	{
		 parent::__construct();
                $this->load->model('M_login_warga');
                $this->load->library('session');
	}
	public function index(){  	
        if ($this->session->userdata('no_kk')==null){
                $this->load->view("warga/login/v_login_warga");
            }else{
                redirect('warga/main');
        }
        //$this->load->view('code/login');
    }

    function login_warga(){

        $user_login=array(
        'nokk'=>$this->input->post('xnokk'),
        'password'=>$this->input->post('xpassword'));
        
            $data=$this->M_login_warga->login_warga($user_login['nokk'],$user_login['password']);
            if($data)
            {
                $this->session->set_userdata('id',$data['id']);
                $this->session->set_userdata('no_kk',$data['no_kk']);
                $this->session->set_userdata('norw',$data['norw']);
                $this->session->set_userdata('nort',$data['nort']);
                $this->session->set_userdata('nik',$data['nik']);
                	//<?php echo $this->session->userdata('email_rt');
                // redirect('adminrt/main');
                redirect('warga/main');
            }
            else{
                redirect('warga/login_warga');
            }

    }

    public function warga_keluar(){
        $this->session->sess_destroy();
        redirect('warga/main');
    }


}
?>