<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller {
       public function __construct(){
              parent::__construct();
                  $this->load->model('m_keuangan');
                  $this->load->library('session');
      }
  public function index()
  {
     if ($this->session->userdata('no_kk')==null){
      $this->load->view("warga/login/v_login_warga");
        }else{
               
              $kode_rt = $this->session->userdata('kode_rt');
              $bulan = date('m');
              $data['bulan'] = $this->m_keuangan->nama_bulan($bulan);
              $data['kasmasuk'] = $this->m_keuangan->total_kas_masuk($kode_rt);
              $data['kaskeluar'] = $this->m_keuangan->total_kas_keluar($kode_rt);
              $this->load->view('warga/keuangan/v_lapor_keuangan', $data);
     }
  }

  
  
  public function filter_keuangan(){
       $bulan = $this->input->post('xbulan');
       $kode_rt = $this->session->userdata('kode_rt');
       $data['bulan'] = $this->m_keuangan->nama_bulan($bulan);
       $data['kasmasuk'] = $this->m_keuangan->total_kas_masuk_filter($kode_rt, $bulan);
       $data['kaskeluar'] = $this->m_keuangan->total_kas_keluar_filter($kode_rt,$bulan);
       $this->load->view('warga/keuangan/v_lapor_keuangan', $data);
  }

 
  

}