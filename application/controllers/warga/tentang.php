<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tentang extends CI_Controller {
  
     public function __construct(){
          parent::__construct();
              $this->load->model('m_galeri');
              $this->load->model('m_agenda');
              $this->load->model('m_kelolaadmin');
              $this->load->library('session');
              $this->load->library('upload');
  }
  public function index(){
     $this->load->view('warga/tentang');
  }
  

}
