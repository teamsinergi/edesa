<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
  
     public function __construct(){
          parent::__construct();
              $this->load->model('m_warga');
              $this->load->model('m_galeri');
              $this->load->model('m_agenda');
              $this->load->model('m_kelolaadmin');
              $this->load->library('session');
              $this->load->library('upload');
  }
  public function index(){
     $data['galeri'] = $this->m_galeri->list_galeri();
     $data['agenda'] = $this->m_agenda->list_agenda();
     $data['banyakrw'] = $this->m_kelolaadmin->banyakrw();
     $data['banyakrt'] = $this->m_kelolaadmin->banyakrt();
     $data['kepalakeluarga'] = $this->m_kelolaadmin->kepalakeluarga();
     $data['banyakwarga'] = $this->m_kelolaadmin->banyakwarga();
     $this->load->view('warga/v_main', $data);
  }
  public function lapor(){
     $this->load->view('warga/lapor.php');
  }
//   public function simpan_pengaduan() {

//         $config['upload_path'] = './assets/images/'; //path folder
//         $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
//         $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
//         $this->upload->initialize($config);
//         if(!empty($_FILES['filefoto']['name']))
//         {
//             if ($this->upload->do_upload('filefoto'))
//             {
//                 $gbr = $this->upload->data();

//                 $config['image_library']='gd2';
//                 $config['source_image']='./assets/images/'.$gbr['file_name'];
//                 $config['create_thumb']= FALSE;
//                 $config['maintain_ratio']= FALSE;
//                 $config['quality']= '50%';
//                 // $config['width']= 300;
//                 // $config['height']= 300;
//                 $config['new_image']= './assets/images/'.$gbr['file_name'];
//                 $this->load->library('image_lib', $config);
//                 $this->image_lib->resize();
                
//                 $photo=$gbr['file_name'];

//                 $nama_kejadian = $this->input->post("xnama");
//                 $tempat_kejadian = $this->input->post("xtempat");
//                 $tanggal = $this->input->post("xtanggal");
                
                
//                 $query = $this->m_warga->simpan_laporan($nama_kejadian, $tempat_kejadian, $tanggal);

//                 if($query){

//                     $this->session->set_flashdata('pesan','Barang berhasil di simpan');
//                 }
//                     $this->load->view('warga/lapor.php');
//                 }else{
//                 redirect('warga/main');
//                 }

//             }else {
//                 $nama_kejadian = $this->input->post("xnama");
//                 $tempat_kejadian = $this->input->post("xtempat");
//                 $tanggal = $this->input->post("xtanggal");

//             $query = $this->m_warga->simpan_laporan_tanpa_foto($nama_kejadian, $tempat_kejadian, $tanggal);
//             if($query){
//                 $this->session->set_flashdata('pesan','Barang berhasil di simpan');
//             }
//             redirect('warga/main');
//         }

//     }

}
  
  


