<?php

/**
 * 
 */
class Agenda extends CI_Controller
{
	function __construct()
	{
		 parent::__construct();
                $this->load->model('m_warga');
                $this->load->library('session');
	}
	public function index()
	{
		if ($this->session->userdata('no_kk')==null){
			$this->load->view("warga/login/v_login_warga");
		}else{
			$norw = $this->session->userdata('norw');
			$nort = $this->session->userdata('nort');
			$data['agenda_rt'] = $this->m_warga->agenda_rt($norw, $nort);
			$data['agenda_rt1'] = $this->m_warga->agenda_rt1($norw, $nort);
			$this->load->view('warga/agenda/v_agenda_rt2.php', $data);
		}
	}
}


?>