<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventori extends CI_Controller {

  public function __construct(){
            parent::__construct();
                $this->load->model('m_inventori');
                $this->load->library('session');
                $this->load->library('upload');
    }
    public function index()
    {
       if ($this->session->userdata('no_kk')==null){
              $this->load->view("warga/login/login");
          }else{
                $nort = $this->session->userdata('nort');
                $norw = $this->session->userdata('norw');
                $kode_rt = $norw.$nort;
                $data['inventori_barang'] = $this->m_inventori->data_barang($kode_rt);
                $this->load->view('warga/inventori/v_lapor_inventori', $data);
       }
    }
   
    

}