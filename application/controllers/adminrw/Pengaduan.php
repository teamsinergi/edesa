<?php 

class Pengaduan extends CI_Controller{
    public function __construct(){
        parent::__construct();
            $this->load->helper('url');
            $this->load->library('Session');
            $this->load->model('m_pengaduan');
            $this->load->model('m_agenda_rw');
    }
    public function index()
    {
       if ($this->session->userdata('email_rw')==null){
              redirect('adminrw');
          }else{ 
                $kode_rw = $this->session->userdata('no_rw');
                $data['galeri'] = $this->m_pengaduan->data_pengaduan($kode_rw);
                $this->template->load('adminrw/static', 'adminrw/pengaduan/v_pengaduan', $data);
       }
    }

}

?>