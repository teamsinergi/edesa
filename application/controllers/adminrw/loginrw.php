<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginrw extends CI_Controller {
	public function __construct(){
            parent::__construct();
                $this->load->model('M_login');
                $this->load->library('session');
    }

  public function index()
  {
  	 if ($this->session->userdata('email_rw')==null){
            $this->load->view("adminrw/loginrw/loginrw");
        }else{
            redirect('adminrw/mainrw');
     }

    //$this->load->view('code/login');
  }

     function login_admin_rw(){

        $user_login=array(
        'email_rw'=>$this->input->post('email_rw'),
        'password'=>$this->input->post('password')
            );

            $data=$this->M_login->login_admin_rw($user_login['email_rw'],$user_login['password']);

            if($data)
            {

                $this->session->set_userdata('id',$data['id']);
                $this->session->set_userdata('email_rw',$data['email_rw']);
                $this->session->set_userdata('no_rw',$data['no_rw']);
                $this->session->set_userdata('password',$data['password']);
                	//<?php echo $this->session->userdata('email_rt');
                redirect('adminrw/mainrw');
            }
            else{
                $this->session->set_flashdata('error_msg', 'Gagal.! Email Atau Password Salah');
                $this->load->view("adminrw/loginrw/loginrw");
            }


 }

    public function user_logout(){
          $this->session->sess_destroy();
          redirect('adminrw/loginrw', 'refresh');
      }


}

?>