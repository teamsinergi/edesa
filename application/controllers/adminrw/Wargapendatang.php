<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Wargapendatang extends CI_Controller{
    public function __construct(){
        parent::__construct();
            $this->load->helper('url');
            // $this->load->model('m_komentar');
            $this->load->library('Session');
            $this->load->model('m_warga_pendatang');
            error_reporting(0);
    }
    public function index()
    {
        $kode_rw = $this->session->userdata('no_rw');
        $data['pekerjaan'] = $this->m_warga_pendatang->pekerjaan();
        $data['agama'] = $this->m_warga_pendatang->agama();
        $data['warga'] = $this->m_warga_pendatang->warga_rw($kode_rw);
        $this->template->load('adminrw/static', 'adminrw/warga/v_warga_pendatang', $data);
    }

    function tambah()
    {
        $data['agama'] = $this->m_warga_pendatang->agama();
        $data['pekerjaan'] = $this->m_warga_pendatang->pekerjaan();
        $this->template->load('static', 'adminrt/warga/v_tambah_warga_pendatang', $data);
    }


    function simpan_warga(){
        $nama = $this->input->post('xnama');
        $nokk = $this->input->post('xnokk');
        $nik = $this->input->post('xnik');
        $rt = $this->input->post('xrt');
        $rw = $this->input->post('xrw');
        $status = $this->input->post('xstatus');
        $agama = $this->input->post('xagama');
        $kelamin = $this->input->post('xjk');
        $pekerjaan = $this->input->post('xpekerjaan');
        $pekerjaanlain = $this->input->post('xpekerjaanlain');
        $alamat = $this->input->post('xalamat');
        $domisili = $this->input->post('xdomisili');
        $kelahiran = $this->input->post('xkelahiran');
        if($pekerjaanlain!=null){
            $pekerjaan = $pekerjaanlain;
        }
        if($pekerjaan && $pekerjaanlain !=null){
            $pekerjaan = $this->input->post('xpekerjaanlain');
        }
        $this->m_warga_pendatang->simpan_warga($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $status, $alamat, $domisili, $kelahiran);
        redirect('adminrt/wargapendatang');
    }
    public function hapus_warga($id){
        $query = $this->m_warga_pendatang->hapus_warga($id);
        if($query){
            $this->session->set_flashdata('pesan','Data berhasil di hapus');
        }
        redirect('adminrt/wargapendatang');
    }
    public function update_warga(){
        $id = $this->input->post('xid');
        $nama = $this->input->post('xnama');
        $nokk = $this->input->post('xnokk');
        $nik = $this->input->post('xnik');
        $rt = $this->input->post('xrt');
        $rw = $this->input->post('xrw');
        $agama = $this->input->post('xagama');
        $kelamin = $this->input->post('xjk');
        $pekerjaan = $this->input->post('xpekerjaan');
        $pekerjaanlain = $this->input->post('xpekerjaanlain');
        $alamat = $this->input->post('xalamat');
        $domisili = $this->input->post('xdomisili');
        $kelahiran = $this->input->post('xkelahiran');

        if($pekerjaanlain!=null){
            $pekerjaan = $pekerjaanlain;
        }
        if($pekerjaan && $pekerjaanlain !=null){
            $pekerjaan = $this->input->post('xpekerjaanlain');
        }
        $status = $this->input->post('xstatus');
        $query = $this->m_warga_pendatang->update_warga($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $id, $status, $alamat, $domisili, $kelahiran);
        if($query){
            $this->session->set_flashdata('pesan','Data berhasil di edit');
        }
        redirect('adminrt/wargapendatang');
    }

        function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->m_warga_pendatang->cari_warga($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = array(
                    'label'         => $row->nik,
                    'description'   => $row->nama_warga,
                );
                echo json_encode($arr_result);
            }
        }
    }

}

?>