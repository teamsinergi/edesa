<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelolaadmin extends CI_Controller {
  public function __construct(){

    parent::__construct();
        $this->load->model('m_kelolaadmin');
        $this->load->library('session');
}

  public function index()
  {
     if ($this->session->userdata('email_rw')==null){
            redirect('adminrw/loginrw');
        }else{
            $nowr = $this->session->userdata('no_rw');
            $data['adminrt'] = $this->m_kelolaadmin->adminrt($nowr);
               $this->template->load('adminrw/static', 'adminrw/kelolaadmin/v_kelola_admin', $data);
     }
  }

  public function simpan_admin_rt(){
    $nama_rt = $this->input->post("xnama_rt");
    $email_rt = $this->input->post("xemail_rt");
    $password = $this->input->post("xpass");
    $rw = $this->input->post("xrw");
    $rt = $this->input->post("xrt");
    $kode_rt = $rw.$rt;
    $this->m_kelolaadmin->simpan_admin($nama_rt, $email_rt, $password, $rw, $rt, $kode_rt);
    redirect('adminrw/kelolaadmin');
  }

}