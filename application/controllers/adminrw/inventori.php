<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventori extends CI_Controller {

	public function __construct(){
            parent::__construct();
                $this->load->model('m_inventori_barang_rw');
                $this->load->library('session');
                $this->load->library('upload');
    }
    public function index()
    {
       if ($this->session->userdata('email_rw')==null){
              $this->load->view("adminrw/login/login");
          }else{
                 
                $kode_rw = $this->session->userdata('no_rw');
                $data['inventori_barang'] = $this->m_inventori_barang_rw->data_barang($kode_rw);
                $this->template->load('adminrw/static', 'adminrw/inventori/v_inventori_barang', $data);
       }
    }
    public function simpan_inventori_barang_rw(){
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $spek = $this->input->post("xspek");
                $banyak = $this->input->post("xbanyak");
                $kondisi = $this->input->post("xkondisi");
                $keterangan = $this->input->post("xketerangan");
                $kode_rw = $this->session->userdata('no_rw');
                $query = $this->m_inventori_barang_rw->simpan_inventori_barang_rw($nama, $spek, $banyak, $kondisi, $keterangan,$photo, $kode_rw);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di simpan');
                }
                redirect('adminrw/inventori/');
            }else{
                redirect('adminrw/inventori/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $spek = $this->input->post("xspek");
            $banyak = $this->input->post("xbanyak");
            $kondisi = $this->input->post("xkondisi");
            $keterangan = $this->input->post("xketerangan");
            $kode_rw = $this->session->userdata('no_rw');
            $query = $this->m_inventori_barang_rw->simpan_inventori_barang_tanpa_foto($nama, $spek, $banyak, $kondisi, $keterangan, $kode_rw);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di simpan');
            }
            redirect('adminrw/inventori/');
        }

    }
    public function hapus_inventori_barang_rw($id){
        $this->m_inventori_barang_rw->hapus_inventori_barang_rw($id);
        redirect('adminrw/inventori');
    }
    public function update_inventori_barang_rw(){
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $spek = $this->input->post("xspek");
                $banyak = $this->input->post("xbanyak");
                $kondisi = $this->input->post("xkondisi");
                $keterangan = $this->input->post("xketerangan");
                $idbarang = $this->input->post('xidbarang'); 
                $kode_rt = $this->session->userdata('kode_rt');
                $query = $this->m_inventori_barang_rw->update_inventori_barang_rw($nama, $spek, $banyak, $kondisi, $keterangan,$photo, $idbarang);
                if($query){
                    $this->session->set_flashdata('pesan','Barang berhasil di update');
                }
                redirect('adminrw/inventori/');
            }else{
                redirect('adminrw/inventori/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $spek = $this->input->post("xspek");
            $banyak = $this->input->post("xbanyak");
            $kondisi = $this->input->post("xkondisi");
            $keterangan = $this->input->post("xketerangan");
            $idbarang = $this->input->post('xidbarang'); 
            $kode_rt = $this->session->userdata('kode_rt');
            $query = $this->m_inventori_barang_rw->update_inventori_barang_tanpa_foto($nama, $spek, $banyak, $kondisi, $keterangan, $idbarang);
            if($query){
                $this->session->set_flashdata('pesan','Barang berhasil di edit');
            }
            redirect('adminrw/inventori/');
        }

    }

}