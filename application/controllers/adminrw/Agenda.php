<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller{
    public function __construct(){
        parent::__construct();
            $this->load->helper('url');
            $this->load->library('Session');
            $this->load->model('m_warga');
            $this->load->model('m_agenda_rw');
    }
    public function index()
    {
        $kode_rw = $this->session->userdata('no_rw');
        $data['agenda'] = $this->m_agenda_rw->agenda($kode_rw);
        $this->template->load('adminrw/static', 'adminrw/agenda/v_agenda', $data);
    }
    public function simpan_agenda(){
        $nama = $this->input->post("xnama");
        $keterangan = $this->input->post("xketerangan");
        $tanggal = $this->input->post("xtanggal");
        $tempat = $this->input->post("xtempat");
        $jam = $this->input->post("xjam");
        $kode_rw = $this->session->userdata('no_rw');
        $this->m_agenda_rw->simpan_agenda($nama, $keterangan, $tanggal, $tempat, $jam, $kode_rw);
        redirect('adminrw/agenda');

    }
    public function update_agenda(){
        $nama = $this->input->post("xnama");
        $keterangan = $this->input->post("xketerangan");
        $tanggal = $this->input->post("xtanggal");
        $tempat = $this->input->post("xtempat");
        $jam = $this->input->post("xjam");
        $id = $this->input->post("xid");
        $this->m_agenda_rw->update_agenda($nama, $keterangan, $tanggal, $tempat, $jam, $id);
        redirect('adminrw/agenda');
    }
    public function hapus_agenda($id){
        $this->m_agenda_rw->hapus_agenda($id);
        redirect("adminrw/agenda");
    }


}

?>