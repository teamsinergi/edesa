<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainrw extends CI_Controller {
public function __construct(){
    parent::__construct();
        $this->load->model('m_keuangan');
        $this->load->library('session');
}
  public function index()
  {
     if ($this->session->userdata('email_rw')==null){
            
            $this->load->view("adminrw/loginrw/loginrw");
        }else{
        	$kode_rt = $this->session->userdata('kode_rt');
          $data['kasmasuk'] = $this->m_keuangan->total_kas_masuk($kode_rt);
          $data['kaskeluar'] = $this->m_keuangan->total_kas_keluar($kode_rt);
               $this->template->load('adminrw/static', 'adminrw/mainrw',$data);
     
     }
  }

}
