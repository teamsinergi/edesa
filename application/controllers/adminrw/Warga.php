<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Warga extends CI_Controller{
    public function __construct(){
        parent::__construct();
            $this->load->helper('url');
            // $this->load->model('m_komentar');
            $this->load->library('Session');
            $this->load->model('m_warga');
            error_reporting(0);
    }
    public function index()
    {
        $kode_rw = $this->session->userdata('no_rw');
        $data['pekerjaan'] = $this->m_warga->pekerjaan();
        $data['agama'] = $this->m_warga->agama();
        $data['warga'] = $this->m_warga->warga_rw($kode_rw);
        $this->template->load('adminrw/static', 'adminrw/warga/v_warga', $data);
    }

    function tambah()
    {
        $data['agama'] = $this->m_warga->agama();
        $data['pekerjaan'] = $this->m_warga->pekerjaan();
        $this->template->load('adminrw/static', 'adminrt/warga/v_tambah_warga', $data);
    }

    function simpan_warga(){
        $nama = $this->input->post('xnama');
        $nokk = $this->input->post('xnokk');
        $nik = $this->input->post('xnik');
        $rt = $this->input->post('xrt');
        $rw = $this->input->post('xrw');
        $status = $this->input->post('xstatus');
        $agama = $this->input->post('xagama');
        $kelamin = $this->input->post('xjk');
        $pekerjaan = $this->input->post('xpekerjaan');
        $pekerjaanlain = $this->input->post('xpekerjaanlain');
        $alamat = $this->input->post('xalamat');
        $domisili = $this->input->post('xdomisili');
        $kelahiran = $this->input->post('xkelahiran');
        if($pekerjaanlain!=null){
            $pekerjaan = $pekerjaanlain;
        }
        if($pekerjaan && $pekerjaanlain !=null){
            $pekerjaan = $this->input->post('xpekerjaanlain');
        }
        $this->m_warga->simpan_warga($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $status, $alamat, $domisili, $kelahiran);
        redirect('adminrt/warga');
    }
    public function hapus_warga($id){
        $query = $this->m_warga->hapus_warga($id);
        if($query){
            $this->session->set_flashdata('pesan','Data berhasil di hapus');
        }
        redirect('adminrt/warga');
    }
    public function update_warga(){
        $id = $this->input->post('xid');
        $nama = $this->input->post('xnama');
        $nokk = $this->input->post('xnokk');
        $nik = $this->input->post('xnik');
        $rt = $this->input->post('xrt');
        $rw = $this->input->post('xrw');
        $agama = $this->input->post('xagama');
        $kelamin = $this->input->post('xjk');
        $pekerjaan = $this->input->post('xpekerjaan');
        $pekerjaanlain = $this->input->post('xpekerjaanlain');
        $alamat = $this->input->post('xalamat');
        $domisili = $this->input->post('xdomisili');
        $kelahiran = $this->input->post('xkelahiran');

        if($pekerjaanlain!=null){
            $pekerjaan = $pekerjaanlain;
        }
        if($pekerjaan && $pekerjaanlain !=null){
            $pekerjaan = $this->input->post('xpekerjaanlain');
        }
        $status = $this->input->post('xstatus');
        $query = $this->m_warga->update_warga($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $id, $status, $alamat, $domisili, $kelahiran);
        if($query){
            $this->session->set_flashdata('pesan','Data berhasil di edit');
        }
        redirect('adminrt/warga');
    }
    public function kepala_keluarga(){
        $kode_rw = $this->session->userdata('no_rw');
        $data['pekerjaan'] = $this->m_warga->pekerjaan();
        $data['agama'] = $this->m_warga->agama();
        $data['warga'] = $this->m_warga->kepala_keluarga_rw($kode_rw);
        $this->template->load('adminrw/static', 'adminrw/warga/v_kepala_warga', $data);
    }
    public function tambah_keluarga($no_kk){
        $data['no_kk'] = $no_kk;
        $data['bapak'] = 1;
        $data['agama'] = $this->m_warga->agama();
        $data['pekerjaan'] = $this->m_warga->pekerjaan();
        $this->template->load('adminrw/static', 'adminrt/warga/v_tambah_warga', $data);
    }
    public function tambah_bapak(){
        $data['bapak'] = 1;
        $data['agama'] = $this->m_warga->agama();
        $data['pekerjaan'] = $this->m_warga->pekerjaan();
        $this->template->load('adminrw/static', 'adminrt/warga/v_tambah_warga', $data);
    }
    public function detail_keluarga($nokk){
        $data['nokk'] = $nokk;
        $data['warga'] = $this->m_warga->detail_keluarga($nokk);
        $data['jumlah_keluarga'] = $this->m_warga->jumlah_keluarga($nokk);
        $data['nama_bapak'] = $this->m_warga->nama_bapak($nokk);
        $this->template->load('adminrw/static', 'adminrw/warga/v_detail_keluarga', $data);
    }
    public function kematian(){
        $kode_rt = $this->session->userdata('no_rt');
        $kode_rw = $this->session->userdata('no_rw');
        $data['pekerjaan'] = $this->m_warga->pekerjaan();
        $data['agama'] = $this->m_warga->agama();
        $data['datawarga'] = $this->m_warga->warga($kode_rt, $kode_rw);
        $data['warga'] = $this->m_warga->kematian_warga($kode_rt, $kode_rw);
        // $this->template->load('static', 'adminrt/warga/v_kematian_warga', $data);
        $this->template->load('adminrw/static','adminrt/warga/v_kematian_warga', $data);
    }
    public function simpan_kematian(){
        $nik = $this->input->post('xnik');
        $cek_nik = $this->m_warga->cek_nik($nik);
        if($cek_nik){
            $tanggal = $this->input->post('xtanggal');
            $tempat = $this->input->post('xtempat');
            $this->m_warga->simpan_kematian($nik, $tempat, $tanggal);
            redirect('adminrt/warga/kematian');
        }else{
            echo "enggak ada";
        }
    }
    public function hapus_kematian($id){
        $this->m_warga->hapus_kematian($id);
        redirect('adminrt/warga/kematian');
    }
    public function update_kematian(){
        $id = $this->input->post("xid");
        $tanggal = $this->input->post("xtanggal");
        $tempat = $this->input->post("xtempat");
        $this->m_warga->update_kematian($id, $tanggal, $tempat);
        redirect('adminrt/warga/kematian');
    }

    function get_autocomplete(){
		if (isset($_GET['term'])) {
		  	$result = $this->m_warga->cari_warga($_GET['term']);
		   	if (count($result) > 0) {
		    foreach ($result as $row)
		     	$arr_result[] = array(
					'label'			=> $row->nik,
					'description'	=> $row->nama_warga,
				);
		     	echo json_encode($arr_result);
		   	}
		}
	}

}

?>