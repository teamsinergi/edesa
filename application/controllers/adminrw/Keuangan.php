<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller {
       public function __construct(){
              parent::__construct();
                  $this->load->model('m_keuangan');
                  $this->load->library('session');
      }
  public function index()
  {
     if ($this->session->userdata('email_rt')==null){
            $this->load->view("adminrt/login/login");
        }else{
               
              $kode_rt = $this->session->userdata('kode_rt');
              $bulan = date('m');
              $data['bulan'] = $this->m_keuangan->nama_bulan($bulan);
              $data['kasmasuk'] = $this->m_keuangan->total_kas_masuk($kode_rt);
              $data['kaskeluar'] = $this->m_keuangan->total_kas_keluar($kode_rt);
              $this->template->load('static', 'adminrt/keuangan/v_keuangan', $data);
     }
  }

  public function kas_keluar(){
    if ($this->session->userdata('email_rt')==null){
           $this->load->view("adminrt/login/login");
    }else{
           $kode_rt = $this->session->userdata('kode_rt');
           $data['kas_keluar'] = $this->m_keuangan->kas_keluar($kode_rt);
           $this->template->load('static', 'adminrt/keuangan/v_keuangan_keluar',$data);
    }
  }

  public function simpan_kas_keluar(){
       $jenis = $this->input->post('xjenis');
       $nominal = $this->input->post('xnominal');
       $tanggal = $this->input->post('xtanggal');
       $keterangan = $this->input->post('xketerangan');
       $kode_rt = $this->session->userdata('kode_rt');
       $query = $this->m_keuangan->simpan_kas_keluar($jenis, $nominal, $tanggal, $keterangan, $kode_rt);
       redirect('adminrt/keuangan/kas_keluar');
  }

  public function kas_masuk(){
       if ($this->session->userdata('email_rt')==null){
              $this->load->view("adminrt/login/login");
       }else{
              $kode_rt = $this->session->userdata('kode_rt');
              $data['kas_masuk'] = $this->m_keuangan->kas_masuk($kode_rt);
              $this->template->load('static', 'adminrt/keuangan/v_keuangan_masuk',$data);
       }
  }
  public function simpan_kas_masuk(){
       $jenis = $this->input->post('xjenis');
       $nominal = $this->input->post('xnominal');
       $tanggal = $this->input->post('xtanggal');
       $keterangan = $this->input->post('xketerangan');
       $kode_rt = $this->session->userdata('kode_rt');
       $query = $this->m_keuangan->simpan_kas_masuk($jenis, $nominal, $tanggal, $keterangan, $kode_rt);
       redirect('adminrt/keuangan/kas_masuk');
  }
  public function filter_keuangan(){
       $bulan = $this->input->post('xbulan');
       $kode_rt = $this->session->userdata('kode_rt');
       $data['bulan'] = $this->m_keuangan->nama_bulan($bulan);
       $data['kasmasuk'] = $this->m_keuangan->total_kas_masuk_filter($kode_rt, $bulan);
       $data['kaskeluar'] = $this->m_keuangan->total_kas_keluar_filter($kode_rt,$bulan);
       $this->template->load('static', 'adminrt/keuangan/v_keuangan', $data);
  }

 
  

}