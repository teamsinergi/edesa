<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginuser extends CI_Controller {
	 public function __construct(){
            parent::__construct();
                $this->load->model('M_login');
                $this->load->library('session');
    }
    public function index(){

  	 if ($this->session->userdata('email_user')==null){  
            $this->load->view("superuser/v_login"); 
        }else{
            redirect('superuser/main');
     }

    //$this->load->view('code/login');
  }

     function login_admin(){

        $user_login=array(
        'email_super'=>$this->input->post('xemail'),
        'password'=>$this->input->post('xpassword'));

            $data=$this->M_login->login_admin_super($user_login['email_super'],$user_login['password']);

            if($data)
            {
                $this->session->set_userdata('id',$data['id']);
                $this->session->set_userdata('email_superuser',$data['email_superuser']);
                $this->session->set_userdata('password',$data['password']);
                	//<?php echo $this->session->userdata('email_rt');
                redirect('superuser/home');
            }
            else{
                $this->session->set_flashdata('error_msg', 'Gagal.! Email Atau Password Salah');
                $this->load->view("superuser/v_login");
            }

       }

      public function user_logout(){
          $this->session->sess_destroy();
          redirect('superuser/loginuser', 'refresh');
      }
      
      public function add(){
        $this->load->view("adminrw/loginrw/v_kelolah_admin");
    }

      public function simpan_admin(){

        $nama_rt = $this->input->post("xnama_rt");
        $email_rt = $this->input->post("xemail_rt");
        $password = $this->input->post("xpass");
        $this->M_login->simpan_admin($nama_rt, $email_rt, $password);
        $this->session->set_flashdata('success', 'Berhasil disimpan');

        redirect('adminrt/login/');
    }

    public function admin()
    {
        $id = $this->session->userdata('id');
        $data['admin'] = $this->M_login->admin($id);
        $this->template->load('static', 'adminrw/loginrw/v_kelolah_admin', $data);
    }

}