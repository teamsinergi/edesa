<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
  public function __construct(){
    parent::__construct();
        $this->load->model('m_keuangan');
        $this->load->model('m_warga');
        $this->load->library('session');
}
  public function index()
  {
     if ($this->session->userdata('email_superuser')==null){
            $this->load->view("superuser/v_login");
        }else{
          $this->template->load('superuser/v_static', 'superuser/v_home');
     
     }
  }

}
