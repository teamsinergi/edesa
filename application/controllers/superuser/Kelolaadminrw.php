<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelolaadminrw extends CI_Controller {
  public function __construct(){
    parent::__construct();
        $this->load->model('m_kelolaadmin');
        $this->load->library('session');
}
  public function index(){
      if ($this->session->userdata('email_superuser')==null){
        $this->load->view("superuser/v_login");
        }else{
            $data['adminrw'] = $this->m_kelolaadmin->adminrw_superuser();
            $this->template->load('superuser/v_static', 'superuser/v_kelola_admin_rw', $data);
      }
  }

  function simpan_admin_rw(){
    $nama = $this->input->post('xnama');
    $email_rw = $this->input->post('xemail');
    $no_rw = $this->input->post('xrw');
    $password = $this->input->post('xpass');
    $kode_rw = $no_rw;
    $nohp = $this->input->post('xnohp');
    $this->m_kelolaadmin->simpan_admin_rw($nama, $email_rw, $no_rw, $kode_rw, $password, $nohp);
    redirect('superuser/kelolaadmin/kelolaadminrw');
  }

  function hapus_admin_rw($id){
        $this->m_kelolaadmin->hapus_admin_rw($id);
        redirect('superuser/kelolaadmin/kelolaadminrw');
  }

  function update_admin_rw(){
      $nama = $this->input->post('xnama');
      $email = $this->input->post('xemail');
      $norw = $this->input->post('xnorw');
      $password = $this->input->post('xpass');
      $no_hp = $this->input->post('xnohp');
      $id = $this->input->post('xid');
      $this->m_kelolaadmin->update_admin_rw($nama, $email, $norw, $password, $no_hp, $id);
      redirect('superuser/kelolaadmin/kelolaadminrw');
  }
  }