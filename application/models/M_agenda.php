<?php
class M_agenda extends CI_Model
{
    public function agenda($kode_rt){
        $query = $this->db->query("SELECT *FROM tbl_agenda WHERE kode_rt='$kode_rt'");
        return $query->result();
    }
    public function simpan_agenda($nama, $keterangan, $tanggal, $tempat, $jam, $kode_rt, $photo){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_agenda(nama_agenda, keterangan, tanggal_agenda, tempat, jam_agenda, kode_rt, create_at, gambar_agenda)
        VALUES('$nama','$keterangan','$tanggal','$tempat','$jam','$kode_rt','$date','$photo')");
        return $query;
    }
    public function simpan_agenda_tanpa_foto($nama, $keterangan, $tanggal, $tempat, $jam, $kode_rt){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_agenda(nama_agenda, keterangan, tanggal_agenda, tempat, jam_agenda, kode_rt, create_at)
        VALUES('$nama','$keterangan','$tanggal','$tempat','$jam','$kode_rt','$date')");
        return $query;
    }
    public function update_agenda($nama, $keterangan, $tanggal, $tempat, $jam, $id, $photo){
        $query = $this->db->query("UPDATE tbl_agenda set nama_agenda='$nama', keterangan='$keterangan', tempat='$tempat', tanggal_agenda='$tanggal', jam_agenda='$jam', gambar_agenda='$photo'
        WHERE id='$id'");
        return $query;
    }

    public function update_agenda_tanpa_foto($nama, $keterangan, $tanggal, $tempat, $jam, $id){
        $query = $this->db->query("UPDATE tbl_agenda set nama_agenda='$nama', keterangan='$keterangan', tempat='$tempat', tanggal_agenda='$tanggal', jam_agenda='$jam'
        WHERE id='$id'");
        return $query;
    }
    public function hapus_agenda($id){
        $query = $this->db->query("DELETE FROM tbl_agenda WHERE id='$id'");
        return $query;
    }
    public function list_agenda(){
        $query = $this->db->query("SELECT *FROM tbl_agenda_rw");
        return $query->result();
    }

}

?>