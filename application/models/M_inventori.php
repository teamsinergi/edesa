<?php 
class M_inventori extends CI_model{
    public function simpan_inventori_barang($nama, $spek, $banyak, $kondisi, $keterangan,$photo, $kode_rt){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_inventori_barang(nama_barang, spesifikasi, banyak_barang, kondisi_barang, keterangan, create_at, foto_barang, kode_rt)
        VALUES('$nama','$spek','$banyak','$kondisi','$keterangan','$date','$photo','$kode_rt')");
        return $query;
    }
    public function simpan_inventori_barang_tanpa_foto($nama, $spek, $banyak, $kondisi, $keterangan, $kode_rt){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_inventori_barang(nama_barang, spesifikasi, banyak_barang, kondisi_barang, keterangan, create_at, kode_rt)
        VALUES('$nama','$spek','$banyak','$kondisi','$keterangan','$date','$kode_rt')");
        return $query;
    }
    public function data_barang($kode_rt){
        $this->db->select('*');
        $this->db->from('tbl_inventori_barang');
        $this->db->where('kode_rt', $kode_rt);
        $query = $this->db->get();
        //$query = $this->db->query("SELECT *FROM tbl_inventori_barang WHERE kode_rt='$kode_rt'");
        return $query->result();
    }
    public function hapus_inventori_barang($id){
        $query = $this->db->query("DELETE FROM tbl_inventori_barang WHERE id='$id'");
        return $query;
    }
    public function update_inventori_barang($nama, $spek, $banyak, $kondisi, $keterangan,$photo, $id){
        $query = $this->db->query("UPDATE tbl_inventori_barang set nama_barang='$nama', spesifikasi='$spek', banyak_barang='$banyak', kondisi_barang='$kondisi', keterangan='$keterangan', foto_barang='$photo'
        WHERE id='$id'");
        return $query;
    }
    public function update_inventori_barang_tanpa_foto($nama, $spek, $banyak, $kondisi, $keterangan, $id){
        $query = $this->db->query("UPDATE tbl_inventori_barang set nama_barang='$nama', spesifikasi='$spek', banyak_barang='$banyak', kondisi_barang='$kondisi', keterangan='$keterangan'
        WHERE id='$id'");
        return $query;
    }



}

?>