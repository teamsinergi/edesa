<?php 
class M_warga extends CI_model{
    public function agama(){
        $query = $this->db->query("SELECT *FROM tbl_agama");
        return $query->result();
    }

   /* public function simpan_warga_pendatang($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $status, $alamat, $domisili, $kelahiran)
    {
        $query = $this->db->query("INSERT INTO tbl_warga_pendatang(nama_warga,no_kk,nik,no_rt, no_rw,pekerjaan,id_agama,jenis_kelamin, status, alamat, domisili, tanggal_lahir)
        VALUES('$nama','$nokk','$nik','$rt','$rw','$pekerjaan','$agama','$kelamin','$status','$alamat','$domisili','$kelahiran')");
        return $query;
    }

    public function warga_pendatang($rt, $rw){

        $query = $this->db->query("SELECT tbl_warga_pendatang.status_hidup, tbl_warga_pendatang.tanggal_lahir, tbl_warga_pendatang.alamat, tbl_warga_pendatang.domisili, tbl_warga_pendatang.status, tbl_warga_pendatang.no_kk, tbl_warga_pendatang.nik, tbl_warga_pendatang.id , tbl_warga_pendatang.nama_warga, tbl_warga_pendatang.no_rt, tbl_warga_pendatang.no_rw, tbl_warga_pendatang.pekerjaan, tbl_warga_pendatang.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga_pendatang.id_agama
        FROM tbl_warga_pendatang, tbl_agama WHERE tbl_warga_pendatang.id_agama = tbl_agama.id_agama AND tbl_warga_pendatang.no_rt='$rt' AND tbl_warga_pendatang.no_rw='$rw'");

        return $query->result();
    }

    public function update_warga_pendatang($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $id, $status, $alamat, $domisili, $kelahiran)
    {
        $query = $this->db->query("UPDATE tbl_warga_pendatang set nama_warga='$nama', no_kk='$nokk',nik='$nik',no_rt='$rt',no_rw='$rw', pekerjaan='$pekerjaan', id_agama='$agama', jenis_kelamin='$kelamin', status='$status', alamat='$alamat', domisili='$domisili', tanggal_lahir='$kelahiran'
        WHERE id='$id'");
        return $query;
    }
    public function hapus_warga_pendatang($id)
    {
        $query = $this->db->query("DELETE FROM tbl_warga_pendatang WHERE id = '$id'");
        return $query;
    }
*/
    public function simpan_warga($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $status, $alamat, $domisili, $kelahiran){
        $date = date('Y-m-d H:i:s');
        $query = $this->db->query("INSERT INTO tbl_warga(nama_warga,no_kk,nik,no_rt, no_rw,pekerjaan,id_agama,jenis_kelamin, status, alamat, domisili, tanggal_lahir, create_at)
        VALUES('$nama','$nokk','$nik','$rt','$rw','$pekerjaan','$agama','$kelamin','$status','$alamat','$domisili','$kelahiran','$date')");
        return $query;
    }
    public function warga($rt, $rw){
        $query = $this->db->query("SELECT tbl_warga.status_hidup, tbl_warga.tanggal_lahir, tbl_warga.alamat, tbl_warga.domisili, tbl_warga.status, tbl_warga.no_kk, tbl_warga.nik, tbl_warga.id , tbl_warga.nama_warga, tbl_warga.no_rt, tbl_warga.no_rw, tbl_warga.pekerjaan, tbl_warga.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga.id_agama
        FROM tbl_warga, tbl_agama WHERE tbl_warga.id_agama = tbl_agama.id_agama AND tbl_warga.no_rt='$rt' AND tbl_warga.no_rw='$rw' ORDER BY tbl_warga.create_at DESC");
        return $query->result();
    }
    public function warga_rw($rw){
        $query = $this->db->query("SELECT tbl_warga.status_hidup, tbl_warga.tanggal_lahir, tbl_warga.alamat, tbl_warga.domisili, tbl_warga.status, tbl_warga.no_kk, tbl_warga.nik, tbl_warga.id , tbl_warga.nama_warga, tbl_warga.no_rt, tbl_warga.no_rw, tbl_warga.pekerjaan, tbl_warga.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga.id_agama
        FROM tbl_warga, tbl_agama WHERE tbl_warga.id_agama = tbl_agama.id_agama AND tbl_warga.no_rw='$rw' ORDER BY tbl_warga.create_at DESC");
        return $query->result();
    }
    public function pekerjaan(){
        $query = $this->db->query("SELECT *FROM tbl_pekerjaan");
        return $query->result();
    }
    public function hapus_warga($id){
        $query = $this->db->query("DELETE FROM tbl_warga WHERE id = '$id'");
        return $query;
    }
    public function update_warga($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $id, $status, $alamat, $domisili, $kelahiran){
        $query = $this->db->query("UPDATE tbl_warga set nama_warga='$nama', no_kk='$nokk',nik='$nik',no_rt='$rt',no_rw='$rw', pekerjaan='$pekerjaan', id_agama='$agama', jenis_kelamin='$kelamin', status='$status', alamat='$alamat', domisili='$domisili', tanggal_lahir='$kelahiran'
        WHERE id='$id'");
        return $query;
    }
    public function kepala_keluarga($rt, $rw){
        $query = $this->db->query("SELECT tbl_warga.tanggal_lahir, tbl_warga.alamat, tbl_warga.domisili, tbl_warga.status, tbl_warga.no_kk, tbl_warga.nik, tbl_warga.no_kk, tbl_warga.id , tbl_warga.nama_warga, tbl_warga.no_rt, tbl_warga.no_rw, tbl_warga.pekerjaan, tbl_warga.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga.id_agama
        FROM tbl_warga, tbl_agama WHERE tbl_warga.id_agama = tbl_agama.id_agama AND tbl_warga.no_rt='$rt' AND tbl_warga.no_rw='$rw' AND tbl_warga.status='bapak'");
        return $query->result();
    }
    public function kepala_keluarga_rw($rw){
        $query = $this->db->query("SELECT tbl_warga.tanggal_lahir, tbl_warga.alamat, tbl_warga.domisili, tbl_warga.status, tbl_warga.no_kk, tbl_warga.nik, tbl_warga.no_kk, tbl_warga.id , tbl_warga.nama_warga, tbl_warga.no_rt, tbl_warga.no_rw, tbl_warga.pekerjaan, tbl_warga.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga.id_agama
        FROM tbl_warga, tbl_agama WHERE tbl_warga.id_agama = tbl_agama.id_agama AND tbl_warga.no_rw='$rw' AND tbl_warga.status='bapak'");
        return $query->result();
    }
    public function detail_keluarga($nokk){
        $query = $this->db->query("SELECT tbl_warga.status, tbl_warga.no_kk, tbl_warga.nik, tbl_warga.id , tbl_warga.nama_warga, tbl_warga.no_rt, tbl_warga.no_rw, tbl_warga.pekerjaan, tbl_warga.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga.id_agama
        FROM tbl_warga, tbl_agama WHERE tbl_warga.id_agama = tbl_agama.id_agama AND tbl_warga.no_kk='$nokk'");
        return $query->result();
    }
    public function jumlah_keluarga($nokk){
        $query = $this->db->query("SELECT count(id) as jumlah_keluarga FROM tbl_warga WHERE no_kk='$nokk'");
        return $query->row()->jumlah_keluarga;
    }
    public function nama_bapak($nokk){
        $query = $this->db->query("SELECT nama_warga FROM tbl_warga WHERE no_kk='$nokk' AND status='bapak'");
        return $query->row()->nama_warga;
    }
    public function cek_nik($nik){
        $query = $this->db->query("SELECT nik as nik FROM tbl_warga WHERE nik='$nik'");
        return $query->result();
    }
    public function kematian_warga($kode_rt, $kode_rw){
        $query = $this->db->query("SELECT tbl_kematian.id, tbl_kematian.tanggal_meninggal, tbl_kematian.tempat_pemakaman, tbl_warga.nama_warga, tbl_warga.jenis_kelamin, tbl_warga.alamat, tbl_warga.no_kk, tbl_warga.nik, tbl_warga.jenis_kelamin, tbl_agama.nama_agama as agama
        FROM tbl_warga, tbl_kematian, tbl_agama WHERE tbl_warga.nik = tbl_kematian.nik AND tbl_warga.no_rw='$kode_rw' AND tbl_warga.no_rt='$kode_rt' AND tbl_warga.id_agama=tbl_agama.id_agama");
        return $query->result();
    }
    public function simpan_kematian($nik, $tempat, $tanggal, $nama){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_kematian(nik,tanggal_meninggal, tempat_pemakaman, create_at , nama_warga)
        VALUES('$nik','$tanggal','$tempat','$date', '$nama')");
        return $query;
    }
    public function hapus_kematian($id){
        $query = $this->db->query("DELETE FROM tbl_kematian WHERE id='$id'");
        return $query;
    }
    public function update_kematian($id, $tanggal, $tempat){
        $query = $this->db->query("UPDATE tbl_kematian SET tanggal_meninggal='$tanggal', tempat_pemakaman='$tempat' WHERE id='$id'");
        return $query;
    }
    public function jumlah_warga($no_rt, $no_rw){
        $query = $this->db->query("SELECT count(id) as jumlah_warga FROM tbl_warga WHERE no_rw='$no_rw' AND no_rt='$no_rt'");
        return $query->row()->jumlah_warga;
    }
    public function cari_warga($title){
        $query = $this->db->query("SELECT * FROM tbl_warga WHERE nik like '%$title%'");
        // $this->db->like('nik', $title , 'both');
		// $this->db->order_by('nik', 'ASC');
		// $this->db->limit(10);
        // return $this->db->get('tbl_warga')->result();
        return $query->result();
    }

    public function cari_warga_kk($no_kk){
        $query = $this->db->query("SELECT * FROM tbl_warga WHERE no_kk like '%$no_kk%' AND status='bapak'");
        // $this->db->like('nik', $title , 'both');
		// $this->db->order_by('nik', 'ASC');
		// $this->db->limit(10);
        // return $this->db->get('tbl_warga')->result();
        return $query->result();
    }
    
    public function akun_warga($kode_rw, $kode_rt){
        $query = $this->db->query("SELECT tbl_akun_warga.id, tbl_warga.nik, tbl_akun_warga.no_kk, tbl_warga.nama_warga
        FROM tbl_warga, tbl_akun_warga
        WHERE tbl_akun_warga.nik = tbl_warga.nik AND tbl_akun_warga.norw='$kode_rw' AND tbl_akun_warga.nort='$kode_rt'");
        return $query->result();
    }
    public function cek_akun($nokk){
        $query = $this->db->query("SELECT *FROM tbl_akun_warga WHERE no_kk='$nokk'");
        return $query->result();
    }
    public function simpan_akun_warga($nokk, $nik, $password, $kode_rw, $kode_rt){
        $tanggal = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_akun_warga(no_kk,nik,password, create_at, norw, nort) VALUES('$nokk','$nik','$password','$tanggal','$kode_rw','$kode_rt')");
        return $query;
    }
    public function update_akun_warga($id, $password){
        $query = $this->db->query("UPDATE tbl_akun_warga SET password='$password' WHERE id='$id'");
        return $query;
    }
    public function hapus_akun_warga($id){
        $query = $this->db->query("DELETE FROM tbl_akun_warga WHERE id='$id'");
        return $query;
    }
    public function simpan_laporan($nama_kejadian, $tempat_kejadian, $photo){

        $tanggal = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_lapor_kejadian(nama_kejadian, tempat_kejadian, foto_kejadian, create_at) VALUES ('$nama_kejadian','$tempat_kejadian','$photo','$tanggal')");
        return $query;

    }
    public function simpan_laporan_tanpa_foto($nama_kejadian, $tempat_kejadian){

        $tanggal = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_lapor_kejadian(nama_kejadian, tempat_kejadian, create_at) VALUES ('$nama_kejadian','$tempat_kejadian','$tanggal')");
        return $query;
    }

    public function agenda_rt($norw, $nort){
        $kode_rt = $norw.$nort;
        $query = $this->db->query("SELECT *FROM tbl_agenda WHERE kode_rt ='$kode_rt' AND DAY(tanggal_agenda)%2=1");
        return $query->result();
    }

    public function agenda_rt1($norw, $nort){
        $kode_rt = $norw.$nort;
        $query = $this->db->query("SELECT *FROM tbl_agenda WHERE kode_rt ='$kode_rt' AND DAY(tanggal_agenda)%2=0");
        return $query->result();
    }
    public function update_status($nik){
        $query = $this->db->query("UPDATE tbl_warga SET status_hidup='meninggal' WHERE nik='$nik'");
        return $query;
    }

    
}

?>