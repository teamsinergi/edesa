<?php 
class M_inventori_barang_rw extends CI_model{
    
    public function simpan_inventori_barang_rw($nama, $spek, $banyak, $kondisi, $keterangan,$photo, $kode_rw){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_inventori_barang_rw(nama_barang, spesifikasi, banyak_barang, kondisi_barang, keterangan, create_at, foto_barang, kode_rw)
        VALUES('$nama','$spek','$banyak','$kondisi','$keterangan','$date','$photo','$kode_rw')");
        return $query;
    }
    public function simpan_inventori_barang_tanpa_foto($nama, $spek, $banyak, $kondisi, $keterangan, $kode_rw){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_inventori_barang_rw(nama_barang, spesifikasi, banyak_barang, kondisi_barang, keterangan, create_at, kode_rw)
        VALUES('$nama','$spek','$banyak','$kondisi','$keterangan','$date','$kode_rw')");
        return $query;
    }
    public function data_barang($kode_rw){
        $query = $this->db->query("SELECT *FROM tbl_inventori_barang_rw WHERE kode_rw='$kode_rw'");
        return $query->result();
    }
    public function hapus_inventori_barang_rw($id){
        $query = $this->db->query("DELETE FROM tbl_inventori_barang_rw WHERE id='$id'");
        return $query;
    }
    public function update_inventori_barang_rw($nama, $spek, $banyak, $kondisi, $keterangan,$photo, $id){
        $query = $this->db->query("UPDATE tbl_inventori_barang_rw set nama_barang='$nama', spesifikasi='$spek', banyak_barang='$banyak', kondisi_barang='$kondisi', keterangan='$keterangan', foto_barang='$photo'
        WHERE id='$id'");
        return $query;
    }
    public function update_inventori_barang_tanpa_foto($nama, $spek, $banyak, $kondisi, $keterangan, $id){
        $query = $this->db->query("UPDATE tbl_inventori_barang_rw set nama_barang='$nama', spesifikasi='$spek', banyak_barang='$banyak', kondisi_barang='$kondisi', keterangan='$keterangan'
        WHERE id='$id'");
        return $query;
    }



}

?>