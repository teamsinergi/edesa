<?php 
class M_keuangan extends CI_model{
    public function simpan_kas_keluar($jenis, $nominal, $tanggal, $keterangan, $kode_rt){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_kas_keluar(jenis_pengeluaran, nominal, tanggal_pengeluaran, keterangan, create_at, kode_rt)
        VALUES('$jenis','$nominal','$tanggal','$keterangan','$date','$kode_rt')");
        return $query;
    }
    public function simpan_kas_masuk($jenis, $nominal, $tanggal, $keterangan, $kode_rt){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_kas_masuk(jenis_pemasukan, nominal, tanggal_pemasukan, keterangan, create_at, kode_rt)
        VALUES('$jenis','$nominal','$tanggal','$keterangan','$date','$kode_rt')");
        return $query;
    }
    public function kas_keluar($kode_rt){
        $query = $this->db->query("SELECT *FROM tbl_kas_keluar WHERE kode_rt='$kode_rt'");
        return $query->result();
    }
    public function kas_masuk($kode_rt){
        $query = $this->db->query("SELECT *FROM tbl_kas_masuk WHERE kode_rt='$kode_rt'");
        return $query->result();
    }

    public function total_kas_masuk($kode_rt){
        $query = $this->db->query("SELECT SUM(nominal) as kasmasuk FROM tbl_kas_masuk WHERE kode_rt='$kode_rt'");
        return $query->row()->kasmasuk;
    }
    public function total_kas_masuk_filter($kode_rt, $bulan){
        $query = $this->db->query("SELECT SUM(nominal) as kasmasuk FROM tbl_kas_masuk WHERE kode_rt='$kode_rt' AND month(tanggal_pemasukan)='$bulan'");
        return $query->row()->kasmasuk;
    }
    public function total_kas_keluar($kode_rt){
        $query = $this->db->query("SELECT SUM(nominal) as kaskeluar FROM tbl_kas_keluar WHERE kode_rt='$kode_rt'");
        return $query->row()->kaskeluar;
    }
    public function total_kas_keluar_filter($kode_rt,$bulan){
        $query = $this->db->query("SELECT SUM(nominal) as kaskeluar FROM tbl_kas_keluar WHERE kode_rt='$kode_rt' AND month(tanggal_pengeluaran)='$bulan'");
        return $query->row()->kaskeluar;
    }
    public function nama_bulan($bulan){
        $query = $this->db->query("SELECT *FROM tbl_bulan ORDER BY kode_bulan='$bulan' DESC");
        return $query->result();
    }


}

?>