<?php 
class M_wisata extends CI_model{
    public function simpan_wisata($nama, $jenis, $tempat, $keterangan,$photo, $kode_rt){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_wisata(nama_wisata, keterangan, tempat, jenis_wisata, foto_wisata, create_at, kode_rt)
        VALUES('$nama','$keterangan','$tempat','$jenis','$photo','$date','$kode_rt')");
        return $query;
    }

    public function simpan_wisata_rw($nama, $jenis, $tempat, $keterangan,$photo, $kode_rw){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_wisata_rw(nama_wisata, keterangan, tempat, jenis_wisata, foto_wisata, create_at, kode_rw)
        VALUES('$nama','$keterangan','$tempat','$jenis','$photo','$date','$kode_rw')");
        return $query;
    }

    public function simpan_wisata_tanpa_foto($nama, $jenis, $tempat, $keterangan, $kode_rt){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_wisata(nama_wisata, keterangan, tempat, jenis_wisata, create_at, kode_rt)
        VALUES('$nama','$keterangan','$tempat','$jenis','$date','$kode_rt')");
        return $query;
    }

    public function simpan_wisata_tanpa_foto_rw($nama, $jenis, $tempat, $keterangan, $kode_rw){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_wisata_rw(nama_wisata, keterangan, tempat, jenis_wisata, create_at, kode_rw)
        VALUES('$nama','$keterangan','$tempat','$jenis','$date','$kode_rw')");
        return $query;
    }

    public function data_wisata($kode_rt){
        $query = $this->db->query("SELECT *FROM tbl_wisata WHERE kode_rt='$kode_rt'");
        return $query->result();
    }
    public function data_wisata_rw($kode_rt){
        $query = $this->db->query("SELECT *FROM tbl_wisata_rw WHERE kode_rw='$kode_rt'");
        return $query->result();
    }
    public function hapus_wisata($id){
        $query = $this->db->query("DELETE FROM tbl_wisata WHERE id='$id'");
        return $query;
    }
    public function hapus_wisata_rw($id){
        $query = $this->db->query("DELETE FROM tbl_wisata_rw WHERE id='$id'");
        return $query;
    }
    public function update_wisata($nama, $jenis, $tempat, $keterangan,$photo, $id){
        $query = $this->db->query("UPDATE tbl_wisata set nama_wisata='$nama', keterangan='$keterangan', tempat='$tempat', jenis_wisata='$jenis', foto_wisata='$photo'
        WHERE id='$id'");
        return $query;
    }

    public function update_wisata_rw($nama, $jenis, $tempat, $keterangan,$photo, $id){
        $query = $this->db->query("UPDATE tbl_wisata_rw set nama_wisata='$nama', keterangan='$keterangan', tempat='$tempat', jenis_wisata='$jenis', foto_wisata='$photo'
        WHERE id='$id'");
        return $query;
    }

    public function update_wisata_tanpa_foto($nama, $jenis, $tempat, $keterangan, $id){
        $query = $this->db->query("UPDATE tbl_wisata set nama_wisata='$nama', keterangan='$keterangan', tempat='$tempat', jenis_wisata='$jenis'
        WHERE id='$id'");
        return $query;
    }
    public function update_wisata_tanpa_foto_rw($nama, $jenis, $tempat, $keterangan, $id){
        $query = $this->db->query("UPDATE tbl_wisata_rw set nama_wisata='$nama', keterangan='$keterangan', tempat='$tempat', jenis_wisata='$jenis'
        WHERE id='$id'");
        return $query;
    }

}

?>