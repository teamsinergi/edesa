<?php 
class M_pengaduan extends CI_model{
    public function simpan_pengaduan($data){
        $this->db->insert('tbl_lapor_kejadian', $data);
    }
    public function data_pengaduan($kode_rw){
        $this->db->select('*');
        $this->db->from('tbl_lapor_kejadian');
        $this->db->join('tbl_warga', 'tbl_lapor_kejadian.nik = tbl_warga.nik');
        $this->db->where('tbl_lapor_kejadian.norw', $kode_rw);
        $query = $this->db->get();
        return $query->result();
    }

    
}

?>