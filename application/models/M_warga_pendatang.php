<?php 
class M_warga_pendatang extends CI_model{
    
    public function agama(){
        $query = $this->db->query("SELECT *FROM tbl_agama");
        return $query->result();
    }

    public function simpan_warga($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $status, $alamat, $domisili, $kelahiran){
        $query = $this->db->query("INSERT INTO tbl_warga_pendatang(nama_warga,no_kk,nik,no_rt, no_rw,pekerjaan,id_agama,jenis_kelamin, status, alamat, domisili, tanggal_lahir)
        VALUES('$nama','$nokk','$nik','$rt','$rw','$pekerjaan','$agama','$kelamin','$status','$alamat','$domisili','$kelahiran')");

        return $query;
    }
    public function warga($rt, $rw){
        $query = $this->db->query("SELECT tbl_warga_pendatang.status_hidup, tbl_warga_pendatang.tanggal_lahir, tbl_warga_pendatang.alamat, tbl_warga_pendatang.domisili, tbl_warga_pendatang.status, tbl_warga_pendatang.no_kk, tbl_warga_pendatang.nik, tbl_warga_pendatang.id , tbl_warga_pendatang.nama_warga, tbl_warga_pendatang.no_rt, tbl_warga_pendatang.no_rw, tbl_warga_pendatang.pekerjaan, tbl_warga_pendatang.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga_pendatang.id_agama
        FROM tbl_warga_pendatang, tbl_agama WHERE tbl_warga_pendatang.id_agama = tbl_agama.id_agama AND tbl_warga_pendatang.no_rt='$rt' AND tbl_warga_pendatang.no_rw='$rw'");
        return $query->result();
    }
    public function warga_rw($rw){
        $query = $this->db->query("SELECT tbl_warga_pendatang.status_hidup, tbl_warga_pendatang.tanggal_lahir, tbl_warga_pendatang.alamat, tbl_warga_pendatang.domisili, tbl_warga_pendatang.status, tbl_warga_pendatang.no_kk, tbl_warga_pendatang.nik, tbl_warga_pendatang.id , tbl_warga_pendatang.nama_warga, tbl_warga_pendatang.no_rt, tbl_warga_pendatang.no_rw, tbl_warga_pendatang.pekerjaan, tbl_warga_pendatang.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga_pendatang.id_agama
        FROM tbl_warga_pendatang, tbl_agama WHERE tbl_warga_pendatang.id_agama = tbl_agama.id_agama AND tbl_warga_pendatang.no_rw='$rw'");
        return $query->result();
    }
    public function pekerjaan(){
        $query = $this->db->query("SELECT *FROM tbl_pekerjaan");
        return $query->result();
    }
    public function hapus_warga($id){
        $query = $this->db->query("DELETE FROM tbl_warga_pendatang WHERE id = '$id'");
        return $query;
    }
    public function update_warga($nama, $nokk, $nik, $rt, $rw, $pekerjaan, $agama, $kelamin, $id, $status, $alamat, $domisili, $kelahiran){
        $query = $this->db->query("UPDATE tbl_warga_pendatang set nama_warga='$nama', no_kk='$nokk',nik='$nik',no_rt='$rt',no_rw='$rw', pekerjaan='$pekerjaan', id_agama='$agama', jenis_kelamin='$kelamin', status='$status', alamat='$alamat', domisili='$domisili', tanggal_lahir='$kelahiran'
        WHERE id='$id'");
        return $query;
    }

    public function jumlah_warga($no_rt, $no_rw){
        $query = $this->db->query("SELECT count(id) as jumlah_warga FROM tbl_warga_pendatang WHERE no_rw='$no_rw' AND no_rt='$no_rt'");
        return $query->row()->jumlah_warga;
    }
    public function cari_warga($title){
        $this->db->like('nik', $title , 'both');
        $this->db->order_by('nik', 'ASC');
        $this->db->limit(10);
        return $this->db->get('tbl_warga_pendatang')->result();
    }
/*    public function kepala_keluarga($rt, $rw){
        $query = $this->db->query("SELECT tbl_warga.tanggal_lahir, tbl_warga.alamat, tbl_warga.domisili, tbl_warga.status, tbl_warga.no_kk, tbl_warga.nik, tbl_warga.no_kk, tbl_warga.id , tbl_warga.nama_warga, tbl_warga.no_rt, tbl_warga.no_rw, tbl_warga.pekerjaan, tbl_warga.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga.id_agama
        FROM tbl_warga, tbl_agama WHERE tbl_warga.id_agama = tbl_agama.id_agama AND tbl_warga.no_rt='$rt' AND tbl_warga.no_rw='$rw' AND tbl_warga.status='bapak'");
        return $query->result();
    }
    public function detail_keluarga($nokk){
        $query = $this->db->query("SELECT tbl_warga.status, tbl_warga.no_kk, tbl_warga.nik, tbl_warga.id , tbl_warga.nama_warga, tbl_warga.no_rt, tbl_warga.no_rw, tbl_warga.pekerjaan, tbl_warga.jenis_kelamin, tbl_agama.nama_agama as agama, tbl_warga.id_agama
        FROM tbl_warga, tbl_agama WHERE tbl_warga.id_agama = tbl_agama.id_agama AND tbl_warga.no_kk='$nokk'");
        return $query->result();
    }
    public function jumlah_keluarga($nokk){
        $query = $this->db->query("SELECT count(id) as jumlah_keluarga FROM tbl_warga WHERE no_kk='$nokk'");
        return $query->row()->jumlah_keluarga;
    }
    public function nama_bapak($nokk){
        $query = $this->db->query("SELECT nama_warga FROM tbl_warga WHERE no_kk='$nokk' AND status='bapak'");
        return $query->row()->nama_warga;
    }
    public function cek_nik($nik){
        $query = $this->db->query("SELECT nik as nik FROM tbl_warga WHERE nik='$nik'");
        return $query->result();
    }
    public function kematian_warga($kode_rt, $kode_rw){
        $query = $this->db->query("SELECT tbl_kematian.id, tbl_kematian.tanggal_meninggal, tbl_kematian.tempat_pemakaman, tbl_warga.nama_warga, tbl_warga.jenis_kelamin, tbl_warga.alamat, tbl_warga.no_kk, tbl_warga.nik, tbl_warga.jenis_kelamin, tbl_agama.nama_agama as agama
        FROM tbl_warga, tbl_kematian, tbl_agama WHERE tbl_warga.nik = tbl_kematian.nik AND tbl_warga.no_rw='$kode_rw' AND tbl_warga.no_rt='$kode_rt' AND tbl_warga.id_agama=tbl_agama.id_agama");
        return $query->result();
    }
    public function simpan_kematian($nik, $tempat, $tanggal){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_kematian(nik,tanggal_meninggal, tempat_pemakaman, create_at)
        VALUES('$nik','$tanggal','$tempat','$date')");
        return $query;
    }
    public function hapus_kematian($id){
        $query = $this->db->query("DELETE FROM tbl_kematian WHERE id='$id'");
        return $query;
    }
    public function update_kematian($id, $tanggal, $tempat){
        $query = $this->db->query("UPDATE tbl_kematian SET tanggal_meninggal='$tanggal', tempat_pemakaman='$tempat' WHERE id='$id'");
        return $query;
    }
    public function jumlah_warga($no_rt, $no_rw){
        $query = $this->db->query("SELECT count(id) as jumlah_warga FROM tbl_warga WHERE no_rw='$no_rw' AND no_rt='$no_rt'");
        return $query->row()->jumlah_warga;
    }
    public function cari_warga($title){
        $this->db->like('nik', $title , 'both');
		$this->db->order_by('nik', 'ASC');
		$this->db->limit(10);
		return $this->db->get('tbl_warga')->result();
	}*/
}

?>