<?php
class M_kelolaadmin extends CI_Model
{
    public function simpan_admin($nama_rt, $email_rt, $password, $rw, $rt, $kode_rt){
        $query = $this->db->query("INSERT INTO tbl_admin_rt(nama_rt,email_rt,password, no_rw, no_rt, kode_rt,status)
        VALUES('$nama_rt','$email_rt','$password','$rw','$rt','$kode_rt','2')");
        return $query;
    }
    public function adminrt($nowr){
        $query = $this->db->query("SELECT *FROM tbl_admin_rt WHERE no_rw='$nowr'");
        return $query->result();
    }
    public function adminrt_superuser(){
        $query = $this->db->query("SELECT *FROM tbl_admin_rt ORDER BY create_at DESC");
        return $query->result();
    }
    public function adminrw_superuser(){
        $query = $this->db->query("SELECT *FROM tbl_admin_rw");
        return $query->result();
    }
    public function banyakrw(){
        $query = $this->db->query("SELECT COUNT(id) as banyakrw FROM tbl_admin_rw");
        return $query->row()->banyakrw;
    }
    public function banyakrt(){
        $query = $this->db->query("SELECT COUNT(id) as banyakrt FROM tbl_admin_rt GROUP BY kode_rt");
        return $query->row()->banyakrt;
    }
    public function kepalakeluarga(){
        $query = $this->db->query("SELECT COUNT(no_kk) as kepalakeluarga FROM tbl_warga GROUP BY no_kk");
        return $query->row()->kepalakeluarga;
    }
    public function banyakwarga(){
        $query = $this->db->query("SELECT COUNT(id) as kepalakeluarga FROM tbl_warga");
        return $query->row()->kepalakeluarga;
    }
    public function simpan_admin_rt($nama_rt, $email_rt, $password, $rw, $rt, $kode_rt, $nohp){
        $date = date('Y-m-d H:i:s');
        $query = $this->db->query("INSERT INTO tbl_admin_rt(nama_rt,email_rt,password, no_rw, no_rt, kode_rt,status, no_hp,create_at)
        VALUES('$nama_rt','$email_rt','$password','$rw','$rt','$kode_rt','2','$nohp','$date')");
        return $query;
    }
    public function hapus_admin_rt($id){
        $query = $this->db->query("DELETE FROM tbl_admin_rt WHERE id='$id'");
        return $query;
    }
    public function simpan_admin_rw($nama, $email_rw, $no_rw, $kode_rw, $password, $nohp){
        $query = $this->db->query("INSERT INTO tbl_admin_rw(no_hp,nama_rw, no_rw, email_rw, username, password, kode_rw, status)
        VALUES('$nohp','$nama','$no_rw','$email_rw','$email_rw','$password','$kode_rw','1')");
        return $query;
    }
    public function hapus_admin_rw($id){
        $query = $this->db->query("DELETE FROM tbl_admin_rw WHERE id='$id'");
        return $query;
    }
    public function update_admin_rw($nama, $email, $norw, $password, $no_hp, $id){
	    $query = $this->db->query("UPDATE tbl_admin_rw SET nama_rw='$nama', no_rw='$norw', no_hp='$no_hp',email_rw='$email', username='$email', password='$password', kode_rw='$norw' WHERE id='$id'");
	    return $query;
    }
    public function update_admin_rt($email, $no_rw, $no_rt, $password, $id){
        $kode_rt=$no_rw.$no_rt;
        $query = $this->db->query("UPDATE tbl_admin_rt SET email_rt='$email', no_rw='$no_rw', no_rt='$no_rt', password='$password', kode_rt='$kode_rt' WHERE id='$id'");
        return $query;
    }
    

}

?>