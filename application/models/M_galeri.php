<?php 
class M_galeri extends CI_model{
    public function simpan_galeri($judul, $tanggal, $tempat, $keterangan,$photo, $kode_rt){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_galeri(nama_galeri, keterangan, tempat, tanggal_galeri, foto_galeri, create_at, kode_rt)
        VALUES('$judul','$keterangan','$tempat','$tanggal','$photo','$date','$kode_rt')");
        return $query;
    }
    public function simpan_galeri_rw($judul, $tanggal, $tempat, $keterangan,$photo, $kode_rw){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_galeri_rw(nama_galeri, keterangan, tempat, tanggal_galeri, foto_galeri, create_at, kode_rw)
        VALUES('$judul','$keterangan','$tempat','$tanggal','$photo','$date','$kode_rw')");
        return $query;
    }
    public function simpan_galeri_tanpa_foto($judul, $tanggal, $tempat, $keterangan, $kode_rt){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_galeri(nama_galeri, keterangan, tempat, tanggal_galeri, create_at, kode_rt)
        VALUES('$judul','$keterangan','$tempat','$tanggal','$date','$kode_rt')");
        return $query;
    }
    public function simpan_galeri_tanpa_foto_rw($judul, $tanggal, $tempat, $keterangan, $kode_rw){
        $date = date('Y-m-d');
        $query = $this->db->query("INSERT INTO tbl_galeri_rw(nama_galeri, keterangan, tempat, tanggal_galeri, create_at, kode_rw)
        VALUES('$judul','$keterangan','$tempat','$tanggal','$date','$kode_rw')");
        return $query;
    }
    public function data_galeri($kode_rt){
        $query = $this->db->query("SELECT *FROM tbl_galeri WHERE kode_rt='$kode_rt'");
        return $query->result();
    }
    public function data_galeri_rw($kode_rw){
        $query = $this->db->query("SELECT *FROM tbl_galeri_rw WHERE kode_rw='$kode_rw'");
        return $query->result();
    }
    public function hapus_galeri($id){
        $query = $this->db->query("DELETE FROM tbl_galeri WHERE id='$id'");
        return $query;
    }
    public function hapus_galeri_rw($id){
        $query = $this->db->query("DELETE FROM tbl_galeri_rw WHERE id='$id'");
        return $query;
    }
    public function update_galeri($judul, $tanggal, $tempat, $keterangan,$photo, $id){
        $query = $this->db->query("UPDATE tbl_galeri set nama_galeri='$judul', keterangan='$keterangan', tempat='$tempat', tanggal_galeri='$tanggal', foto_galeri='$photo'
        WHERE id='$id'");
        return $query;
    }
    public function update_galeri_rw($judul, $tanggal, $tempat, $keterangan,$photo, $id){
        $query = $this->db->query("UPDATE tbl_galeri_rw set nama_galeri='$judul', keterangan='$keterangan', tempat='$tempat', tanggal_galeri='$tanggal', foto_galeri='$photo'
        WHERE id='$id'");
        return $query;
    }

    public function update_galeri_tanpa_foto($judul, $tanggal, $tempat, $keterangan, $id){
        $query = $this->db->query("UPDATE tbl_galeri set nama_galeri='$judul', keterangan='$keterangan', tempat='$tempat', tanggal_galeri='$tanggal'
        WHERE id='$id'");
        return $query;
    }
    public function update_galeri_tanpa_foto_rw($judul, $tanggal, $tempat, $keterangan, $id){
        $query = $this->db->query("UPDATE tbl_galeri_rw set nama_galeri='$judul', keterangan='$keterangan', tempat='$tempat', tanggal_galeri='$tanggal'
        WHERE id='$id'");
        return $query;
    }
    public function list_galeri(){
        $query = $this->db->query("SELECT *FROM tbl_galeri");
        return $query->result();
    }

}

?>