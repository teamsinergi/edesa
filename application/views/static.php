<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>E - Desa</title>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/jquery-ui.css'?>">
    <link rel="shortcut icon" href="<?php  echo site_url('assets/images/villagex.png')?>" type="image/x-icon" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/jquery-ui.css'?>">
    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
  <meta name="author" content="">
  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url(); ?>assets1/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url(); ?>assets1/css/sb-admin.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="<?php echo base_url(); ?>assets/styles/shards-dashboards.1.1.0.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/extras.1.1.0.min.css">
     <link href="<?php echo base_url(); ?>assets1vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <script async defer src="https://buttons.github.io/buttons.js"></script>
  </head>
  <body class="h-100">
  <!-- <div class="color-switcher animated">
      <h5>Warna Tampilan</h5>
      <ul class="accent-colors">
        <li class="accent-primary active" data-color="primary">
          <i class="material-icons">check</i>
        </li>
        <li class="accent-secondary" data-color="secondary">
          <i class="material-icons">check</i>
        </li>
        <li class="accent-success" data-color="success">
          <i class="material-icons">check</i>
        </li>
        <li class="accent-info" data-color="info">
          <i class="material-icons">check</i>
        </li>
        <li class="accent-warning" data-color="warning">
          <i class="material-icons">check</i>
        </li>
        <li class="accent-danger" data-color="danger">
          <i class="material-icons">check</i>
        </li>
      </ul>
     
      <div class="social-wrapper">
      </div>
      <div class="close">
        <i class="material-icons">close</i>
      </div>
      </div> -->
   <div class="color-switcher-toggle animated pulse infinite">
      <i class="material-icons">settings</i>
    </div>
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
          <div class="main-navbar">
            <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
              <a class="navbar-brand w-100 mr-0" href="<?php echo site_url('main'); ?>" style="line-height: 25px;">
                <div class="d-table m-auto">
                  <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;" src="<?php echo base_url(); ?>assets/images/shards-dashboards-logo.svg" alt="Shards Dashboard">
                  <span class="d-none d-md-inline ml-1">Admin E - desa</span>
                </div>
              </a>
              <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
              </a>
            </nav>
          </div>
          <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
            <div class="input-group input-group-seamless ml-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-search"></i>
                </div>
              </div>
              <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
          </form>

          <div class="nav-wrapper">
            <ul class="nav flex-column">
              <li class="nav-item <?php echo $this->uri->segment(2) == 'main' ? 'active': ''; ?>">
                <a class="nav-link" href="<?php echo site_url('adminrt/main')?>">
                  <i class="material-icons">dashboard</i>
                  <span>Dashboard</span>
                </a>
              </li>
              <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'warga' ? 'active': ''; ?>">
                <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                  <i class="material-icons">people</i>
                  <span>Warga</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                  <a class="dropdown-item" href="<?php echo site_url('adminrt/warga/kepala_keluarga')?>">                  
                    <span>Kepala Keluarga</span>
                  </a>
                  <a class="dropdown-item" href="<?php echo site_url('adminrt/warga')?>">                  
                    <span>Data Warga</span>
                  </a>
                  <a class="dropdown-item" href="<?php echo site_url('adminrt/warga/kematian')?>">       
                    <span>Kematian</span>
                  </a>
                  
                  <a class="dropdown-item" href="<?php echo site_url('adminrt/wargapendatang')?>">       
                    <span>Warga Pendatang</span>
                  </a>

                </div>
              </li>
              <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'keuangan' ? 'active': ''; ?>">
                <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                  <i class="material-icons">attach_money</i>
                  <span>Keuangan</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                  <a class="dropdown-item" href="<?php echo site_url('adminrt/keuangan')?>">
                  
                  <span>Dashboard Data Keuangan</span>
                </a>
                 <a class="dropdown-item" href="<?php echo site_url('adminrt/keuangan/kas_keluar')?>">
                  
                  <span>Keuangan keluar</span>
                </a>

                <a class="dropdown-item" href="<?php echo site_url('adminrt/keuangan/kas_masuk')?>">
                  
                  <span>Keuangan masuk</span>
                </a>

                </div>
              </li>
              <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'inventori' ? 'active': ''; ?>">
                <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                  <i class="fa fa-cubes"></i>
                  <span>Inventori RT</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                  <a class="dropdown-item" href="<?php echo site_url('adminrt/inventori')?>">
                  <span>Barang RT</span>
                </a>
                 <!-- <a class="dropdown-item" href="<?php echo site_url('adminrt/keuangan/kas_keluar')?>">
                  <span>Budaya</span>
                </a> -->
                <!-- <a class="dropdown-item" href="<?php echo site_url('adminrt/keuangan/kas_masuk')?>">
                  <span>Wisata</span>
                </a> -->
                </div>
              </li>
              <li class="nav-item <?php echo $this->uri->segment(2) == 'main' ? 'galeri': ''; ?>">
                <a class="nav-link " href="<?php echo site_url('adminrt/galeri')?>">
                  <i class="material-icons">collections</i>
                  <span>Galeri</span>
                </a>
              </li>
              <li class="nav-item <?php echo $this->uri->segment(2) == 'agenda' ? 'active': ''; ?>">
                <a class="nav-link " href="<?php echo site_url('adminrt/agenda')?>">
                  <i class="material-icons">date_range</i>
                  <span>Agenda RT</span>
                </a>
              </li>
              <li class="nav-item <?php echo $this->uri->segment(2) == 'main' ? 'wisata': ''; ?>">
                <a class="nav-link " href="<?php echo site_url('adminrt/wisata')?>">
                  <i class="material-icons">landscape</i>
                  <span>Wisata</span>
                </a>
              </li>
              <li class="nav-item <?php echo $this->uri->segment(2) == 'kelolaadmin' ? 'active': ''; ?>">
                <a class="nav-link " href="<?php echo site_url('adminrt/warga/akun_warga')?>">
                  <i class="material-icons">person_add</i>
                  <span>Akun Warga</span>
                </a>
              </li>

              <!-- <li class="nav-item">
                <a class="nav-link " href="<?php echo site_url('adminrt/keuangan')?>">
                  <i class="material-icons">view_module</i>
                  <span>Keuangan</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo site_url('adminrt/keuangan/kas_keluar')?>">
                  <i class="material-icons">view_module</i>
                  <span>Keuangan keluar</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="<?php echo site_url('adminrt/keuangan')?>">
                  <i class="material-icons">view_module</i>
                  <span>Keuangan masuk</span>
                </a>
              </li>
              
              <li class="nav-item <?php echo $this->uri->segment(1) == 'component' ? 'active': '' ?>">
                <a class="nav-link " href="<?php echo site_url('component')?>">
                  <i class="material-icons">vertical_split</i>
                  <span>Blog Posts</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="add-new-post.html">
                  <i class="material-icons">note_add</i>
                  <span>Add New Post</span>
                </a>
              </li> -->
              <!-- <li class="nav-item">
                <a class="nav-link " href="form-components.html">
                  <i class="material-icons">view_module</i>
                  <span>Forms &amp; Components</span>
                </a>
              </li> -->
              <!-- <li class="nav-item">
                <a class="nav-link " href="tables.html">
                  <i class="material-icons">table_chart</i>
                  <span>Tables</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="user-profile-lite.html">
                  <i class="material-icons">person</i>
                  <span>User Profile</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="errors.html">
                  <i class="material-icons">error</i>
                  <span>Errors</span>
                </a>
              </li> -->
            <!-- main sidebar buat pak RW -->
            <!-- <li class="nav-item <?php echo $this->uri->segment(2) == 'main' ? 'active': ''; ?>">
                <a class="nav-link" href="<?php echo site_url('adminrt/main')?>">
                  <i class="material-icons">edit</i>
                  <span>DIbawah milih pak RW</span>
                </a>
              </li> -->

            <!-- main sidebar buat pak RW -->
              <!-- <li class="nav-item <?php echo $this->uri->segment(2) == 'kelolaadmin' ? 'active': ''; ?>">
                <a class="nav-link " href="<?php echo site_url('adminrw/kelolaadmin')?>">
                  <i class="material-icons">person_add</i>
                  <span>Tambah Admin RT</span>
                </a>
              </li> -->
            </ul>
          </div>
        </aside>
         <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <div class="main-navbar sticky-top bg-white">
            <!-- Main Navbar -->
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
              <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
                <div class="input-group input-group-seamless ml-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <i class="fas fa-search"></i>
                    </div>
                  </div>
                  <input class="navbar-search form-control" type="text" placeholder="Pencarian ..." aria-label="Search"> </div>
              </form>
              <ul class="navbar-nav border-left flex-row ">
                <li class="nav-item border-right dropdown notifications">
                  <a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="nav-link-icon__wrapper">
                      <i class="material-icons">&#xE7F4;</i>
                      <span class="badge badge-pill badge-danger">2</span>
                    </div>
                  </a>

                  <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">
                      <div class="notification__icon-wrapper">
                        <div class="notification__icon">
                          <i class="material-icons">&#xE6E1;</i>
                        </div>
                      </div>
                      <div class="notification__content">
                        <span class="notification__category">Analytics</span>
                        <p>Your website’s active users count increased by
                          <span class="text-success text-semibold">28%</span> in the last week. Great job!</p>
                      </div>
                    </a>

                    <a class="dropdown-item" href="#">
                      <div class="notification__icon-wrapper">
                        <div class="notification__icon">
                          <i class="material-icons">&#xE8D1;</i>
                        </div>
                      </div>
                      <div class="notification__content">
                        <span class="notification__category">Sales</span>
                        <p>Last week your store’s sales count decreased by
                          <span class="text-danger text-semibold">5.52%</span>. It could have been worse!</p>
                      </div>
                    </a>

                    <a class="dropdown-item notification__all text-center" href="#"> View all Notifications </a>
                  </div>
                </li>

                <li class="nav-item border-right dropdown notifications">
                  <a class="nav-link nav-link-icon text-center" href="<?php echo site_url('adminrt/login/user_logout')?>">
                    <div class="nav-link-icon__wrapper">

                      <i class="fa fa-power-off"></i>
                      
                    </div>
                  </a>
                </li>
              </ul>



        <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
        <!-- End Main Sidebar -->
       <!-- Isi Tampilan-->
       <?php echo $contents;?>
       <!-- End Of Isi Tampilan-->





       <footer class="main-footer d-flex p-2 px-3 bg-white border-top">
            <ul class="nav">
              <li class="nav-item">
                <a class="nav-link" href="#">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Services</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Products</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Blog</a>
              </li>
            </ul>
            <span class="copyright ml-auto my-auto mr-2">Copyright © 2018
              <a href="https://designrevision.com" rel="nofollow">SINERGI Creative</a>
            </span>
          </footer>
        </main>
      </div>
    </div>

    <!-- Load partial : ALERT -->

                <!-- ======================================================== ini buat Akun Warga ======================================================== -->
                <!-- <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
                <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
                <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#nokk').autocomplete({
                                source: "<?php echo site_url('adminrt/warga/get_autocomplete_akunwarga');?>",
                                select: function (event, ui) {
                                    $('[name="title"]').val(ui.item.label); 
                                    $('[name="xnama"]').val(ui.item.xnama);
                                    $('[name="xnik"]').val(ui.item.xnik); 

                                }
                            });
                    });
                  </script> -->
              <!-- ======================================================== ini buat Akun Warga ======================================================== -->
    
    <!--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script> 
    <script src="<?php echo base_url(); ?>assets/scripts/extras.1.1.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/scripts/shards-dashboards.1.1.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/scripts/app/app-blog-overview.1.1.0.js"></script>

    <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url(); ?>../assets1/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>../assets1/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url(); ?>assets1/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="<?php echo base_url(); ?>assets1/vendor/datatables/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(); ?>assets1/vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url(); ?>assets1/js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="<?php echo base_url(); ?>assets1/js/demo/datatables-demo.js"></script>
              

  </body>
</html>
