<!DOCTYPE html>
<html lang="en">
<head>
	<title>Daftar</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url()?>asuser/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asuser/css/main.css">
<!--===============================================================================================-->
</head>
<body>

        <form class="login100-form validate-form" role="form" method="post" action="<?php echo base_url('adminrt/login/login_admin'); ?>">

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-33">
						Daftar
					</span>
					<label>Nama*</label>
					<div class="wrap-input100 validate-input" data-validate = "Nama Diperlukan">
						<input class="input100" type="text" name="namart" placeholder="Nama">

						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>
					<label>Nomor RT*</label>
					<div class="wrap-input100 validate-input" data-validate = "No RT Diperlukan">
						<input class="input100" type="number" name="nort" placeholder="Nomor RT">

						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>
					<label>Nomor RW*</label>
					<div class="wrap-input100 validate-input" data-validate = "No RW Diperlukan">
						<input class="input100" type="number" name="norw" placeholder="Nomor RW">

						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>
					<label>Pekerjaan*</label>
					<div class="wrap-input100 validate-input" data-validate = "Pekerjaan Diperlukan">
						<input class="input100" type="text" name="pekerjaan" placeholder="Pekerjaan">

						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>
					<br>
					<label>Jenis Kelamin</label>
					<div style="margin-left: 10%" data-validate = "Jenis Kelamin Diperlukan">
						<input type="radio" name="jenkel" value="Laki-laki"> Laki - Laki
						<input type="radio" name="jenkel" value="Perempuan"> Perempuan
					</div><br>
					<label>Agama*</label>
					<div class="wrap-input100 validate-input" data-validate = "Pekerjaan Diperlukan">
						<select name="agama" style="width: 100%; height: 45px">
						<option value="Islam"> Islam </option>
						<option value="Kristen Katholik">Kristen Katholik</option>
						<option value="Kristen Protestan">Kristen Protestan</option>
						<option value="Hindu">Hindu</option>
						<option value="Budha">Budha</option>
						<option value="Lainnya">Lainnya</option>
						</select>

						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>
					<label style="margin-top: 4%">Email*</label>
					<div class="wrap-input100 validate-input" data-validate = "Email Diperlukan, contoh: ex@abc.xyz">
						<input class="input100" type="text" name="Email_rt" placeholder="Email">

						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>
					<label>Password*</label>
					<div class="wrap-input100 rs1 validate-input" data-validate="Password Diperlukan">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="container-login100-form-btn m-t-20">
						<input type="submit" value = "Daftar" class="login100-form-btn">			
					</div>

					<div class="text-center">
						<span class="txt1">
							Sudah Punya Akun?
						</span>

						<a href="<?php echo site_url('adminrw/loginrw')?>" class="txt2 hov1">
							Masuk
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>asuser/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>asuser/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>asuser/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url()?>asuser/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>asuser/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>asuser/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url()?>asuser/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>asuser/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>asuser/js/main.js"></script>

</body>
</html>