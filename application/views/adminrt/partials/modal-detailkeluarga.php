 
<!-- 1. adminrt/warga/w_warga-->
 <!-- ============================= EDIT ========================= -->
 <?php $no =0; foreach($warga as $data): $no++?>
 <div class="modal fade" id="edit-DetailK<?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Keluarga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/warga/update_warga')?>" method="POST">
      <div class="modal-body">
      <div class="container">
            <div class="form-group" hidden>
                <label for="usr">id :</label>
                <input type="text" class="form-control" id="usr" name="xid" value="<?php echo $data->id?>">
            </div>
            <div class="form-group float-label-control">
                        <label for="">Status Keluarga</label>
                        <select class="form-control" id="agama" name="xstatus">
                            
                              <?php if($data->status=="bapak"){?>
                                <option value="bapak">Bapak</option>
                              <?php }elseif($data->status=="ibu"){?>
                                <option value="ibu">Ibu</option>
                              <?php }else{?>
                                <option value="anak">Anak</option>
                              <?php }?>
                            <option value="bapak">Bapak</option>
                            <option value="ibu">Ibu</option>
                            <option value="anak">Anak</option>
                              <option value="ibu">Kelurga Lain</option>
                        </select>
                    </div>
            <div class="form-group">
                <label for="usr">NO KK :</label>
                <input type="text" class="form-control" id="usr" name="xnokk" value="<?php echo $data->no_kk?>">
            </div>
            <div class="form-group">
                <label for="usr">NIK :</label>
                <input type="text" class="form-control" id="usr" name="xnik" value="<?php echo $data->nik?>">
            </div>
            <div class="form-group">
                <label for="usr">Nama :</label>
                <input type="text" class="form-control" id="usr" name="xnama" value="<?php echo $data->nama_warga?>">
            </div>
            <div class="form-group">
                <label for="usr">Tanggal Lahir :</label>
                <input type="date" class="form-control" id="usr" name="xkelahiran" value="<?php echo $data->tanggal_lahir?>">
            </div>
            <div class="form-group">
                <label for="usr">Alamat :</label>
                <input type="text" class="form-control" id="usr" name="xalamat" value="<?php echo $data->alamat?>">
            </div>
            <div class="form-group">
                <label for="usr">Alamat Domisili :</label>
                <input type="text" class="form-control" id="usr" name="xdomisili" value="<?php echo $data->domisili?>">
            </div>
            <div class="form-group">
                <label for="usr">No. RW :</label>
                <input type="text" class="form-control" id="usr" name="xrw" value="<?php echo $data->no_rw?>">
            </div>
            <div class="form-group">
                <label for="usr">No. RT :</label>
                <input type="text" class="form-control" id="usr" name="xrt" value="<?php echo $data->no_rt?>">
            </div>
            <div class="form-group float-label-control">
                        <label for="">Pekerjaan</label>
                        <div class="row">
                          <div class="col-sm-6">
                            <label class="radio-inline"><input type="radio" name="xpekerjaan" value="<?php echo $data->pekerjaan?>" checked> <?php echo $data->pekerjaan?> </label>
                          </div>
                        <?php foreach($pekerjaan as $kunci):?>
                            <div class="col-sm-6">
                              <label class="radio-inline"><input type="radio" name="xpekerjaan" value="<?php echo $kunci->nama_pekerjaan?>"> <?php echo $kunci->nama_pekerjaan?> </label>
                            </div>
                        <?php endforeach?>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                              <input type="radio" name="xpekerjaan" value="">Other <input type="text" name="xpekerjaanlain" />​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
                            </div>
                        </div>
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Jenis Kelsamin</label>
                        <?php if($data->jenis_kelamin == "pria"){?>
                        <div class="row">
                            <div class="col-sm-3">
                              <label class="radio-inline"><input type="radio" name="xjk" value="pria" checked> Pria </label>
                            </div>
                            <div class="col-sm-3">
                              <label class="radio-inline"><input type="radio" name="xjk" value="wanita"> Waria</label>
                            </div>
                        </div>
                        <?php }else{ ?>
                          <div class="row">
                            <div class="col-sm-3">
                              <label class="radio-inline"><input type="radio" name="xjk" value="pria"> Pria </label>
                            </div>
                            <div class="col-sm-3">
                              <label class="radio-inline"><input type="radio" name="xjk" value="wanita" checked> Waria</label>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
            <div class="form-group float-label-control">
                        <label for="">Agama</label>
                        <select class="form-control" id="agama" name="xagama">
                          <option value="<?php echo $data->id_agama?>"><?php echo $data->agama?></option>
                          <?php foreach($agama as $key):?>
                            <option value="<?php echo $key->id_agama?>"><?php echo $key->nama_agama?></option>
                          <?php endforeach?>
                        </select>
                    </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>

<?php endforeach?>
 <!-- =================================================================================== -->

<!-- ============================= HAPUS ========================= -->
<?php $no =0; foreach($warga as $data): $no++?>
  <div class="modal fade" id="hapus-DetailK<?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Keluarga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/warga/hapus_warga/'.$data->id)?>" metdod="POST">
      <div class="modal-body">
      <div class="container">
       <h5>Anda yakin ingin menghapus data <?php echo $data->nama_warga?> ?</h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <!-- <button id="delAlr" type="button" class="btn btn-primary" data-dismiss="modal" onclick="$('.alert').show()"> Hapus </button> -->
        <!-- <input id="delAlr" type="submit" class="btn btn-primary" data-dismiss="modal" value="Hapus"> -->
        <button type="submit" class="btn btn-danger">Hapus</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->


 <!-- end of 1. adminrt/warga/w_warga-->
