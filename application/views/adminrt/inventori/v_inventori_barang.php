<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets1/css/custom.css');?>">
<nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
 <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Data Inventori Barang</h3><br>
                <div >
            <button class="btn btn-primary" data-toggle="modal" data-target="#mKeuanganK"><i class="fas fa-plus"></i> Tambah Inventori Barang</button>
          </div>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col">
                <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Data Inventori Barang</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Spesifikasi</th>
                    <th>Banyak Barang</th>
                    <th>Kondisi Barang</th>
                    <th>Keterangan</th>
                    <th>Foto Barang</th>
                    <th>Tanggal</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <!-- <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Spesifikasi</th>
                    <th>Banyak Barang</th>
                    <th>Kondisi Barang</th>
                    <th>Keterangan</th>
                    <th>Tanggal</th>
                    <th>Option</th>
                  </tr>
                </tfoot> -->
                <tbody>
                <?php $no =0; foreach($inventori_barang as $data): $no++?>
                    <tr>
                      <td><?php echo $no?></td>
                      <td><?php echo $data->nama_barang?></td>
                      <td><?php echo $data->spesifikasi?></td>
                      <td><?php echo $data->banyak_barang?></td>
                      <td><?php echo $data->kondisi_barang?></td>
                      <td><?php echo $data->keterangan?></td>
                      <td><?php echo $data->create_at?></td>
                      <td><img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>" width="64" /></td>
                      <td>
                        <div class="d-flex justified-content-center">
                         <a data-toggle="modal" data-target="#edit-pjModal<?php echo $data->id?>" class="btn btn-small"><i class="fa fa-edit" style="color: #3C8DBC; font-size: 20px; font-weight: bold;"></i> </a>
                          <a data-toggle="modal" data-target="#hapus-pjModal<?php echo $data->id?>" class="btn btn-small"><i class="fa fa-trash" style="color: #D73925; font-size: 20px; font-weight: bold;"></i> </a>
                        </div>
                        </td>
                        
                    </tr>
                  <?php endforeach?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              
            <!-- Load partial : MODAL -->
 
<!-- 1. adminrt/warga/w_warga-->
 <!-- ============================= tambah ========================= -->
 <!-- Modal -->
<div class="modal fade" id="mKeuanganK" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Inventori Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/inventori/simpan_inventori_barang')?>" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
            <div class="form-group">
                <label for="usr">Nama Barang :</label>
                <input type="text" class="form-control" id="usr" name="xnama" required>
            </div>
            <div class="form-group">
                <label for="usr">Spesifikasi :</label>
                <input type="text" class="form-control" id="usr" name="xspek" required>
            </div>
            <div class="form-group">
                <label for="usr">Banyak Barang :</label>
                <input type="text" class="form-control" id="usr" name="xbanyak" required>
            </div>
            <div class="form-group float-label-control">
                <label for="">Kondisi</label>
                <select class="form-control" id="agama" name="xkondisi" required>
                  <option value="" disabled selected>Pilih Kondisi</option>
                  <option value="Baik">Baik</option>
                  <option value="Rusak">Rusak</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan :</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" required>
            </div>
            <div class="form-group">
								<label for="inputUserName" class="col-sm-4 control-label">Photo</label>
								<div class="form-control">
                  <input type="file" name="filefoto"/>
                </div>
						</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

 
<!-- 1. adminrt/warga/w_warga-->
 <!-- ============================= EDIT ========================= -->
 <?php $no =0; foreach($inventori_barang as $data): $no++?>
 <div class="modal fade" id="edit-pjModal<?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/inventori/update_inventori_barang')?>" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
            <div class="form-group">
                <label for="usr">id Barang :</label>
                <input type="text" class="form-control" id="usr" name="xidbarang" value="<?php echo $data->id?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Nama Barang :</label>
                <input type="text" class="form-control" id="usr" name="xnama" value="<?php echo $data->nama_barang?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Spesifikasi :</label>
                <input type="text" class="form-control" id="usr" name="xspek" value="<?php echo $data->spesifikasi?>" required>
            </div>
            <div class="form-group">
                <label for="usr">Banyak Barang :</label>
                <input type="text" class="form-control" id="usr" name="xbanyak" value="<?php echo $data->banyak_barang?>" required>
            </div>
            <div class="form-group float-label-control">
                <label for="">Kondisi</label>
                <select class="form-control" id="agama" name="xkondisi" required>
                  <option value="<?php echo $data->kondisi_barang?>" selected><?php echo $data->kondisi_barang?></option>
                  <option value="Baik">Baik</option>
                  <option value="Rusak">Rusak</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan :</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" value="<?php echo $data->keterangan?>" required>
            </div>
            <div class="form-group">
								<label for="inputUserName" class="col-sm-4 control-label">Photo</label>
								<div class="form-control">
                  <input type="file" name="filefoto"/>
                </div>
						</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->

<!-- ============================= HAPUS ========================= -->
<?php $no =0; foreach($inventori_barang as $data): $no++?>
  <div class="modal fade" id="hapus-pjModal<?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/inventori/hapus_inventori_barang/'.$data->id)?>" metdod="POST">
      <div class="modal-body">
      <div class="container">
       <h5>Anda yakin ingin menghapus data <?php echo $data->nama_barang?> ?</h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <!-- <button id="delAlr" type="button" class="btn btn-primary" data-dismiss="modal" onclick="$('.alert').show()"> Hapus </button> -->
        <!-- <input id="delAlr" type="submit" class="btn btn-primary" data-dismiss="modal" value="Hapus"> -->
        <button type="submit" class="btn btn-danger">Hapus</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->
 <!-- end of 1. adminrt/warga/w_warga-->

<script src="../assets1/js/custom.js"></script>
