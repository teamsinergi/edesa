<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets1/css/custom.css');?>">
<nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
 <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Data Agenda</h3><br>
                <div >
            <button class="btn btn-primary" data-toggle="modal" data-target="#mKeuanganK"><i class="fas fa-plus"></i> Tambah Agenda</button>
          </div>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col">
                <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Data Agenda</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Keterangan</th>
                    <th>Tempat</th>
                    <th>Tanggal</th>
                    <th>jam</th>
                    <th>Gambar</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <!-- <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Keterangan</th>
                    <th>Tempat</th>
                    <th>Tanggal</th>
                    <th>jam</th>
                    <th>Option</th>
                  </tr>
                </tfoot> -->
                <tbody>
                <?php $no =0; foreach($agenda as $data): $no++?>
                    <tr>
                      <td><?php echo $no?></td>
                      <td><?php echo $data->nama_agenda?></td>
                      <td><?php echo $data->keterangan?></td>
                      <td><?php echo $data->tempat?></td>
                      <td><?php echo $data->tanggal_agenda?></td>
                      <td><?php echo $data->jam_agenda?></td>
                      <td><img src="<?php echo base_url('assets/images/'.$data->gambar_agenda) ?>" width="64" /></td>
                      <td>
                        <div class="d-flex justified-content-center">
                         <a data-toggle="modal" data-target="#edit-pjModal<?php echo $data->id?>" class="btn btn-small"><i class="fa fa-edit" style="color: #3C8DBC; font-size: 20px; font-weight: bold;"></i> </a>
                          <a data-toggle="modal" data-target="#hapus-pjModal<?php echo $data->id?>" class="btn btn-small"><i class="fa fa-trash" style="color: #D73925; font-size: 20px; font-weight: bold;"></i> </a>
                        </div>
                        </td>
                        
                    </tr>
                  <?php endforeach?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              
            <!-- Load partial : MODAL -->
 
<!-- 1. adminrt/warga/w_warga-->
 <!-- ============================= tambah ========================= -->
 <!-- Modal -->
<div class="modal fade" id="mKeuanganK" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Agenda</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/agenda/simpan_agenda')?>" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
            <div class="form-group">
                <label for="usr">Nama Agenda :</label>
                <input type="text" class="form-control" id="usr" name="xnama" required>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan :</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" required>
            </div>
            <div class="form-group">
                <label for="usr">Tempat :</label>
                <input type="text" class="form-control" id="usr" name="xtempat" required>
            </div>
            <div class="form-group">
                <label for="usr">Tanggal :</label>
                <input type="date" class="form-control" id="usr" name="xtanggal" required>
            </div>
            <div class="form-group">
                <label for="usr">Jam :</label>
                <input type="time" class="form-control" id="usr" name="xjam" required>
            </div>
            <div class="form-group">
								<label for="inputUserName" class="col-sm-4 control-label">Photo</label>
								<div class="form-control">
                  <input type="file" name="filefoto"/>
                </div>
						</div>
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

 
<!-- 1. adminrt/warga/w_warga-->
 <!-- ============================= EDIT ========================= -->
 <?php $no =0; foreach($agenda as $data): $no++?>
 <div class="modal fade" id="edit-pjModal<?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Agenda</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?php echo site_url('adminrt/agenda/update_agenda')?>" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
                <div class="form-group" hidden>
                    <label for="usr">Id  :</label>
                    <input type="text" class="form-control" id="usr" name="xid" value="<?php echo $data->id?>" required>
                </div>
                <div class="form-group">
                    <label for="usr">Judul :</label>
                    <input type="text" class="form-control" id="usr" name="xnama" value="<?php echo $data->nama_agenda?>" required>
                </div>
                <div class="form-group">
                    <label for="usr">Keterangan :</label>
                    <input type="text" class="form-control" id="usr" name="xketerangan" value="<?php echo $data->keterangan?>" required>
                </div>
                <div class="form-group">
                    <label for="usr">Tempat :</label>
                    <input type="text" class="form-control" id="usr" name="xtempat" value="<?php echo $data->tempat?>" required>
                </div>
                <div class="form-group">
                    <label for="usr">Tanggal :</label>
                    <input type="date" class="form-control" id="usr" name="xtanggal" value="<?php echo $data->tanggal_agenda?>" required>
                </div>
                <div class="form-group">
                    <label for="usr">Jam :</label>
                    <input type="time" class="form-control" id="usr" name="xjam" value="<?php echo $data->jam_agenda?>" required>
                </div>
                <div class="form-group">
                    <label for="inputUserName" class="col-sm-4 control-label">Photo</label>
                    <div class="form-control">
                      <input type="file" name="filefoto"/>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->

<!-- ============================= HAPUS ========================= -->
<?php $no =0; foreach($agenda as $data): $no++?>
  <div class="modal fade" id="hapus-pjModal<?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Agenda</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/agenda/hapus_agenda/'.$data->id)?>" metdod="POST">
      <div class="modal-body">
      <div class="container">
       <h5>Anda yakin ingin menghapus agenda <?php echo $data->nama_agenda?> ?</h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <!-- <button id="delAlr" type="button" class="btn btn-primary" data-dismiss="modal" onclick="$('.alert').show()"> Hapus </button> -->
        <!-- <input id="delAlr" type="submit" class="btn btn-primary" data-dismiss="modal" value="Hapus"> -->
        <button type="submit" class="btn btn-danger">Hapus</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->
 <!-- end of 1. adminrt/warga/w_warga-->

<script src="../assets1/js/custom.js"></script>
