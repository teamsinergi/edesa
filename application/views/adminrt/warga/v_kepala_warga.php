<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets1/css/custom.css');?>">
<nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
 <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Data Kepala Keluarga</h3><br>
                <div >
            <a class="btn btn-primary" href="<?php echo site_url('adminrt/warga/tambah_bapak'); ?>"><i class="fas fa-plus"></i>Tambah Kepala Keluarga</a>
          </div>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col">
                <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Data Kepala Keluarga</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th style="width: 10%;">No RW</th>
                    <th style="width: 10%;">No RT</th>
                    <th>Pekerjaan</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <!-- <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>No RT</th>
                    <th>No RW</th>
                    <th>Pekerjaan</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Option</th>
                  </tr>
                </tfoot> -->
                <tbody>
                <?php $no =0; foreach($warga as $data): $no++?>
                    <tr>
                      <td><?php echo $no?></td>
                      <td><?php echo $data->nama_warga?></td>
                      <td><?php echo $data->no_rw?></td>
                      <td><?php echo $data->no_rt?></td>
                      <td><?php echo $data->pekerjaan?></td>
                      <td><?php echo $data->jenis_kelamin?></td>
                      <td><?php echo $data->agama?></td>
                      <td>
                        <div class="d-flex justified-content-center" id="KepalaK" >
                         
                        <a data-toggle="modal" data-target="#edit-KepalaK<?php echo $data->id?>" id="KepalaKeluarga" class="btn btn-small"><i class="fa fa-edit" style="color: #3C8DBC; font-size: 18.5px; font-weight: bold;"></i> </a>

                        <a href="<?php echo site_url('adminrt/warga/tambah_keluarga/'.$data->no_kk)?>"  class="btn btn-small"><i class="fa fa-user-plus" style="color: #E6C10A; font-size: 18.5px; font-weight: bold;"></i> </a>

                        <a href="<?php echo site_url('adminrt/warga/detail_keluarga/'.$data->no_kk)?>"  class="btn btn-small"><i class="fa fa-info" style="color: #2d2d2d; font-size: 18.5px; font-weight: bold;"></i> </a>

                        <a data-toggle="modal" data-target="#hapus-KepalaK<?php echo $data->id?>" class="btn btn-small"><i class="fa fa-trash" style="color: #D73925; font-size: 18.5px; font-weight: bold;"></i> </a>
                        </div>
                        </td>
                        
                    </tr>
                  <?php endforeach?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              
            <!-- Load partial : MODAL -->
            <?php $this->load->view('adminrt/partials/modals.php');?>

            </div>
            <!-- End Default Light Table -->
            <!-- Default Dark Table -->
          </div>
          
            <!-- Load partial : ALERT -->
            <?php $this->load->view('adminrt/partials/alerts.php');?>

          <script src="../assets1/js/custom.js"></script>
