<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets1/css/custom.css');?>">
<nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
 <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Akun Warga</h3><br>
                <!-- <div >
                <button class="btn btn-primary" data-toggle="modal" data-target="#mKeuanganK"><i class="fas fa-plus"></i> Tambah Data Kematian</button>
          </div> -->
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col">
                <!-- DataTables Example -->
                <div id="accordion">


  <div class="card">
  
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
      <button class="btn btn-primary" data-toggle="modal"><i class="fas fa-plus"></i> 
      Tambah Akun Warga</button>
      </a>
    </div>
    <div id="collapseTwo" class="collapse" data-parent="#accordion">
      <div class="card-body">
         <div class="container">
              <div class="row">
              <form action="<?php echo site_url('adminrt/warga/simpan_akun_warga')?>" method="POST" enctype="multipart/form-data">
                  <div class="form-group">
                      <label>No KK</label>
                      <input type="text" class="form-control" id="nokk" placeholder="No KK" name="xnokk" style="width:500px;">
                  </div>
                  <div class="form-group">
                      <label>NIK</label>
                      <input type="text" class="form-control" name="xnik" style="width:500px;" readonly>
                  </div>
                  <div class="form-group">
                      <label>Nama</label>
                      <textarea class="form-control" style="width:500px;" name="xnama" readonly></textarea>
                  </div>
            <div class="form-group">
                <label for="usr">Password</label>
                <input type="password" class="form-control" id="usr" name="xpassword" required>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
                </form>
              </div>
	          </div>
            
      </div>
    </div>
  </div>
</div>
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            <!-- testing -->
            <!-- <div class="container">
              <div class="row">
                <h2>Autocomplete Codeigniter</h2>
              </div>
              <div class="row">
                <form>
                  <div class="form-group">
                      <label>Title</label>
                      <input type="text" name="title" class="form-control" id="title" placeholder="Title" style="width:500px;">
                  </div>
                  <div class="form-group">
                      <label>Description</label>
                      <textarea name="description" class="form-control" placeholder="Description" style="width:500px;"></textarea>
                  </div>
                </form>
              </div>
	          </div> -->
            <!-- testing -->
            Akun Warga</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>KK</th>
                    <th>NIK</th>
                    <th>Option</th>
                  </tr>
                </thead>

                <tbody>
                <?php $no =0; foreach($akun_warga as $data): $no++?>
                    <tr>
                      <td><?php echo $no?></td>
                      <td><?php echo $data->nama_warga?></td>
                      <td><?php echo $data->no_kk?></td>
                      <td><?php echo $data->nik?></td>
                      <td>
                        <div class="d-flex justified-content-center">
                         <a data-toggle="modal" data-target="#edit-pjModal<?php echo $data->id?>" class="btn btn-small"><i class="fa fa-edit" style="color: #3C8DBC; font-size: 20px; font-weight: bold;"></i> </a>
                          <a data-toggle="modal" data-target="#hapus-pjModal<?php echo $data->id?>" class="btn btn-small"><i class="fa fa-trash" style="color: #D73925; font-size: 20px; font-weight: bold;"></i> </a>
                        </div>
                        </td>
                        
                    </tr>
                  <?php endforeach?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              
            <!-- Load partial : MODAL -->

<!-- 1. adminrt/warga/w_warga-->
 <!-- ============================= tambah ========================= -->
 <!-- Modal -->
 <div class="modal fade" id="mKeuanganK" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Kematian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/warga/simpan_kematian')?>" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
            <div class="form-group">
                <label for="usr">NIK :</label>
                <input type="text" class="form-control" id="titlse" name="xnik" required>
            </div>
            <div class="form-group">
				      <label>Description</label>
				      <textarea name="description" class="form-control" placeholder="Description"></textarea>
				    </div>
            <div class="form-group">
                <label for="usr">Tanggal Meninggal :</label>
                <input type="date" class="form-control" id="usr" name="xtanggal" required>
            </div>
            <div class="form-group">
                <label for="usr">Tempat Pemakaman : </label>
                <input type="text" class="form-control" id="usr" name="xtempat" required>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

 
<!-- 1. adminrt/warga/w_warga-->
  <?php $no =0; foreach($akun_warga as $data): $no++?>
 <div class="modal fade" id="edit-pjModal<?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Akun Warga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/warga/update_akun_warga')?>" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
            <div class="form-group" hidden>
                <label for="usr">id :</label>
                <input type="text" class="form-control" id="usr" name="xid" value="<?php echo $data->id?>" required readonly>
            </div>
            <div class="form-group">
                <label for="usr">NO KK :</label>
                <input type="text" class="form-control" id="usr" name="xnokk" value="<?php echo $data->no_kk?>" required readonly>
            </div>
            <div class="form-group">
                <label for="usr">Nama Kepala Keluarga :</label>
                <input type="text" class="form-control" id="usr" name="xtanggal" value="<?php echo $data->nama_warga?>" readonly>
            </div>
            <div class="form-group">
                <label for="usr">Password : </label>
                <input type="text" class="form-control" id="usr" name="xpassword" required>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->

<!-- ============================= HAPUS ========================= -->
<?php $no =0; foreach($akun_warga as $data): $no++?>
  <div class="modal fade" id="hapus-pjModal<?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Kematian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/warga/hapus_akun_warga/'.$data->id)?>" metdod="POST">
      <div class="modal-body">
      <div class="container">
       <h5>Anda yakin ingin menghapus data <?php echo $data->nama_warga?> ?</h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <!-- <button id="delAlr" type="button" class="btn btn-primary" data-dismiss="modal" onclick="$('.alert').show()"> Hapus </button> -->
        <!-- <input id="delAlr" type="submit" class="btn btn-primary" data-dismiss="modal" value="Hapus"> -->
        <button type="submit" class="btn btn-danger">Hapus</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->
 <!-- end of 1. adminrt/warga/w_warga-->

            </div>
            <!-- End Default Light Table -->
            <!-- Default Dark Table -->
          </div>
          
            <!-- Load partial : ALERT
            <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
            <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
            <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#title').autocomplete({
                            source: "<?php echo site_url('adminrt/warga/get_autocomplete');?>",
                            select: function (event, ui) {
                                $('[name="title"]').val(ui.item.label); 
                                $('[name="description"]').val(ui.item.description); 
                            }
                        });
                });
              </script> -->
              <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
                <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
                <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#nokk').autocomplete({
                                source: "<?php echo site_url('adminrt/warga/get_autocomplete_akunwarga');?>",
                                select: function (event, ui) {
                                    $('[name="title"]').val(ui.item.label); 
                                    $('[name="xnama"]').val(ui.item.xnama);
                                    $('[name="xnik"]').val(ui.item.xnik); 

                                }
                            });
                    });
                  </script>
            <?php $this->load->view('adminrt/partials/alerts.php');?>
          <script src="../assets1/js/custom.js"></script>
