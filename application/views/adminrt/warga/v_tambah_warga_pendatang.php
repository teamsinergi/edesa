 <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
 <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Tambah Data Warga Pendatang</h3><br>

              </div>
            </div>
            <!-- End Page Header -->

            <!-- Default Light Table -->
            <div class="row ">
              <div class="col">
                <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tambah Data</div>

          <div class="card-body">
          <form action="<?php echo site_url('adminrt/wargapendatang/simpan_warga')?>" method="POST">
                <h4 class="page-header">Data Warga Pendatang<?php 
                ini_set('display_errors', 0);
                error_reporting(E_ALL);?>
                <!-- error hidiing -->
                </h4>
          <div class="row">
            <div class="col-sm-8">
                <form role="form">
                    <div class="form-group float-label-control">
                        <label for="">Status Keluarga</label>
                        <select class="form-control" id="agama" name="xstatus">
                          <?php $cinta=$no_kk; if($no_kk){?>
                            <option value="ibu">Ibu</option>
                            <option value="anak">Anak</option>
                            <option value="ibu">Keluraga Lain</option>
                          <?php }elseif($bapak==1){?>
                            <option value="bapak">Bapak</option>
                          <?php }else{?>
                            <option value="" disabled selected>Pilih Status</option>
                            <option value="bapak">Bapak</option>
                            <option value="ibu">Ibu</option>
                            <option value="anak">Anak</option>
                            <option value="ibu">Keluraga Lain</option>
                            
                          <?php }?>
                        </select>
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">No. Kartu Keluarga</label>
                          <?php if($no_kk){?>
                            <input type="text" class="form-control" value="<?php echo $no_kk?>" name="xnokk" readonly>
                          <?php }else{?>                        
                            <input type="text" class="form-control" placeholder="No. Kartu Keluarga" name="xnokk">
                          <?php }?>
                    </div>

                    <div class="form-group float-label-control">
                        <label for="">NIK</label>
                        <input type="text" class="form-control" placeholder="NIK" name="xnik">
                    </div>

                    <div class="form-group float-label-control">
                        <label for="">Nama Warga</label>
                        <input type="text" class="form-control" placeholder="Nama Warga" name="xnama">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Tanggal Lahir</label>
                        <input type="date" class="form-control" placeholder="Nama Warga" name="xkelahiran">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Alamat Disini</label>
                        <input type="text" class="form-control" placeholder="Alamat Asal" name="xalamat" value="otomatis nanti" readonly>
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Alamat asal/domisili</label>
                        <input type="text" class="form-control" placeholder="Alamat Domisili" name="xdomisili">
                    </div>

                  <div class="row">
                    <div class="form-group float-label-control col-md-5">
                        <label for="">No. RT</label>
                        <input type="text" class="form-control" placeholder="No. RT" name="xrt" value="<?php echo $this->session->userdata('no_rt')?>" readonly>
                    </div>

                    <div class="form-group float-label-control col-md-5">
                        <label for="">No. RW</label>
                        <input type="text" class="form-control" placeholder="No. RT" name="xrw" value="<?php echo $this->session->userdata('no_rw')?>" readonly>
                    </div>
              </div>

                    <!-- <div class="form-group float-label-control">
                        <label for="">Pekerjaan</label>
                        <input type="text" class="form-control" placeholder="Pekerjaan" name="xpekerjaan">
                    </div> -->
                    <div class="form-group float-label-control">
                        <label for="">Pekerjaan</label>
                        <div class="row">
                        <?php foreach($pekerjaan as $data):?>
                            <div class="col-sm-6">
                              <label class="radio-inline"><input type="radio" name="xpekerjaan" value="<?php echo $data->nama_pekerjaan?>"> <?php echo $data->nama_pekerjaan?> </label>
                            </div>
                        <?php endforeach?>
                        <div class="col-sm-6" style="height: 55px;">
                              <input type="radio" name="xpekerjaan" value=""> Lainnya <input type="text" class="form-control" name="xpekerjaanlain" />​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
                            </div>
                        </div>
                            
                    </div>

                    <div class="form-group float-label-control">
                        <label for="">Jenis Kelamin</label>
                        <div class="row">

                            <div class="col-sm-2">
                              <label class="radio-inline"> <input type="radio" name="xjk" value="pria"> Pria </label>
                            </div>

                            <div class="col-sm-2">
                              <label class="radio-inline"> <input type="radio" name="xjk" value="wanita"> Wanita</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group float-label-control">
                        <label for="">Agama</label>
                        <select class="form-control" id="agama" name="xagama">
                          <?php foreach($agama as $data):?>
                            <option value="<?php echo $data->id_agama?>"><?php echo $data->nama_agama?></option>
                          <?php endforeach?>
                        </select>
                    </div>
                    
            </div>
          </div>
          <input class="btn btn-primary" type="submit" name="btn" value="Tambah Warga" />
              </form>
                </div>
              </div>
            </div>
            <!-- End Default Light Table -->
            <!-- Default Dark Table -->
           
          </div>