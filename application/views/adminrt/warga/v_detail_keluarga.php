<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets1/css/custom.css');?>">
<nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
 <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Data Keluarga</h3><br>
                <div >
            <a class="btn btn-primary" href="<?php echo site_url('adminrt/warga/tambah_keluarga/'.$nokk); ?>"><i class="fas fa-plus"></i> Tambah Anggota Keluarga</a>
          </div>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col">
                <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Data Warga
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        Nama Kepala Keluarga &nbsp; :
                        &ensp;

                        <?php echo $nama_bapak?> 
                    </div>
                    <!-- <div class="col-6">
                        <?php echo $nama_bapak?> 
                    </div> -->
                </div>
                <!-- <div class="row">
                    <div class="col-6">
                        Alamat Sekarang 
                    </div>
                    <div class="col-6">
                        nama 
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        Alamat Domisili  
                    </div>
                    <div class="col-6">
                        nama 
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        Rw/Rt 
                    </div>
                    <div class="col-6">
                        nama 
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-6">
                        Jumlah Keluarga  
                        &emsp; &emsp; &ensp; :
                        &ensp; <?php echo $jumlah_keluarga?> 
                    </div>
                    <!-- <div class="col-6">
                        <?php echo $jumlah_keluarga?> 
                    </div> -->
                </div>
            </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>KK</th>
                    <th>NIK</th>
                    <th style="width: 10%;">No RT</th>
                    <th style="width: 10%;">No RW</th>
                    <th>Pekerjaan</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Status</th>
                    <!-- <th>Option</th> -->
                  </tr>
                </thead>
                <!-- <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>No RT</th>
                    <th>No RW</th>
                    <th>Pekerjaan</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Option</th>
                  </tr>
                </tfoot> -->
                <tbody>
                <?php $no =0; foreach($warga as $data): $no++?>
                    <tr>
                      <td><?php echo $no?></td>
                      <td><?php echo $data->nama_warga?></td>
                      <td><?php echo $data->no_kk?></td>
                      <td><?php echo $data->nik?></td>
                      <td><?php echo $data->no_rw?></td>
                      <td><?php echo $data->no_rt?></td>
                      <td><?php echo $data->pekerjaan?></td>
                      <td><?php echo $data->jenis_kelamin?></td>
                      <td><?php echo $data->agama?></td>
                      <td><?php echo $data->status?></td>
                      <!-- <td>
                        <div class="d-flex justified-content-center">
                         
                         <a data-toggle="modal" data-target="#edit-DetailK<?php echo $data->id?>" class="btn btn-small"><i class="fa fa-edit" style="color: #3C8DBC; font-size: 20px; font-weight: bold;"></i> </a>
                          <a data-toggle="modal" data-target="#hapus-DetailK<?php echo $data->id?>" class="btn btn-small"><i class="fa fa-trash" style="color: #D73925; font-size: 20px; font-weight: bold;"></i> </a>
                        </div>
                        </td> -->
                        
                    </tr>
                  <?php endforeach?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              
            <!-- Load partial : MODAL -->
             
<!-- 1. adminrt/warga/w_warga-->
 <!-- ============================= EDIT =========================
 <?php $no =0; foreach($warga as $data): $no++?>
 <div class="modal fade" id="edit-DetailK<?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Keluarga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/warga/update_warga')?>" method="POST">
      <div class="modal-body">
      <div class="container">
            <div class="form-group" hidden>
                <label for="usr">id :</label>
                <input type="text" class="form-control" id="usr" name="xid" value="<?php echo $data->id?>">
            </div>
            <div class="form-group float-label-control">
                        <label for="">Status Keluarga</label>
                        <select class="form-control" id="agama" name="xstatus">
                            
                              <?php if($data->status=="bapak"){?>
                                <option value="bapak">Bapak</option>
                              <?php }elseif($data->status=="ibu"){?>
                                <option value="ibu">Ibu</option>
                              <?php }else{?>
                                <option value="anak">Anak</option>
                              <?php }?>
                            <option value="bapak">Bapak</option>
                            <option value="ibu">Ibu</option>
                            <option value="anak">Anak</option>
                        </select>
                    </div>
            <div class="form-group">
                <label for="usr">NO KK :</label>
                <input type="text" class="form-control" id="usr" name="xnokk" value="<?php echo $data->no_kk?>">
            </div>
            <div class="form-group">
                <label for="usr">NIK :</label>
                <input type="text" class="form-control" id="usr" name="xnik" value="<?php echo $data->nik?>">
            </div>
            <div class="form-group">
                <label for="usr">Nama :</label>
                <input type="text" class="form-control" id="usr" name="xnama" value="<?php echo $data->nama_warga?>">
            </div>
            <div class="form-group">
                <label for="usr">Tanggal Lahir :</label>
                <input type="date" class="form-control" id="usr" name="xkelahiran" value="<?php echo $data->tanggal_lahir?>">
            </div>
            <div class="form-group">
                <label for="usr">Alamat :</label>
                <input type="text" class="form-control" id="usr" name="xalamat" value="<?php echo $data->alamat?>">
            </div>
            <div class="form-group">
                <label for="usr">Alamat Domisili :</label>
                <input type="text" class="form-control" id="usr" name="xdomisili" value="<?php echo $data->domisili?>">
            </div>
            <div class="form-group">
                <label for="usr">No. RW :</label>
                <input type="text" class="form-control" id="usr" name="xrw" value="<?php echo $data->no_rw?>">
            </div>
            <div class="form-group">
                <label for="usr">No. RT :</label>
                <input type="text" class="form-control" id="usr" name="xrt" value="<?php echo $data->no_rt?>">
            </div>
            <div class="form-group float-label-control">
                        <label for="">Pekerjaan</label>
                        <div class="row">
                          <div class="col-sm-6">
                            <label class="radio-inline"><input type="radio" name="xpekerjaan" value="<?php echo $data->pekerjaan?>" checked> <?php echo $data->pekerjaan?> </label>
                          </div>
                        <?php foreach($pekerjaan as $kunci):?>
                            <div class="col-sm-6">
                              <label class="radio-inline"><input type="radio" name="xpekerjaan" value="<?php echo $kunci->nama_pekerjaan?>"> <?php echo $kunci->nama_pekerjaan?> </label>
                            </div>
                        <?php endforeach?>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                              <input type="radio" name="xpekerjaan" value="">Other <input type="text" name="xpekerjaanlain" />​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
                            </div>
                        </div>
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Jenis Kelsamin</label>
                        <?php if($data->jenis_kelamin == "pria"){?>
                        <div class="row">
                            <div class="col-sm-3">
                              <label class="radio-inline"><input type="radio" name="xjk" value="pria" checked> Pria </label>
                            </div>
                            <div class="col-sm-3">
                              <label class="radio-inline"><input type="radio" name="xjk" value="wanita"> Waria</label>
                            </div>
                        </div>
                        <?php }else{ ?>
                          <div class="row">
                            <div class="col-sm-3">
                              <label class="radio-inline"><input type="radio" name="xjk" value="pria"> Pria </label>
                            </div>
                            <div class="col-sm-3">
                              <label class="radio-inline"><input type="radio" name="xjk" value="wanita" checked> Waria</label>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
            <div class="form-group float-label-control">
                        <label for="">Agama</label>
                        <select class="form-control" id="agama" name="xagama">
                          <option value="<?php echo $data->id_agama?>"><?php echo $data->agama?></option>
                          <?php foreach($agama as $key):?>
                            <option value="<?php echo $key->id_agama?>"><?php echo $key->nama_agama?></option>
                          <?php endforeach?>
                        </select>
                    </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>

<?php endforeach?>
 <!-- =================================================================================== -->

<!-- ============================= HAPUS ========================= -->
<?php $no =0; foreach($warga as $data): $no++?>
  <div class="modal fade" id="hapus-DetailK <?php echo $data->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Keluarga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/warga/hapus_warga/'.$data->id)?>" metdod="POST">
      <div class="modal-body">
      <div class="container">
       <h5>Anda yakin ingin menghapus data <?php echo $data->nama_warga?> ?</h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <!-- <button id="delAlr" type="button" class="btn btn-primary" data-dismiss="modal" onclick="$('.alert').show()"> Hapus </button> -->
        <!-- <input id="delAlr" type="submit" class="btn btn-primary" data-dismiss="modal" value="Hapus"> -->
        <button type="submit" class="btn btn-danger">Hapus</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->


 <!-- end of 1. adminrt/warga/w_warga-->

            </div>
            <!-- End Default Light Table -->
            <!-- Default Dark Table -->
          </div>
          
            <!-- Load partial : ALERT -->
            <?php $this->load->view('adminrt/partials/alerts.php');?>

          <script src="../assets1/js/custom.js"></script>
