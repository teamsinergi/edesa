<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets1/css/custom.css');?>">
<nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
 <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Data Keuangan Keluar</h3><br>
                <div >
            <button class="btn btn-primary" data-toggle="modal" data-target="#mKeuanganK"><i class="fas fa-plus"></i> Tambah Transaksi</button>
          </div>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col">
                <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Data Keuangan Keluar</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Jenis Pengeluaran</th>
                    <th>Nominal</th>
                    <th>Keterangan</th>
                    <th>Tanggal Pengeluaran</th>
                    
                  </tr>
                </thead>
                <!-- <tfoot>
                  <tr>
                  <th>No</th>
                    <th>Jenis Pengeluaran</th>
                    <th>Nominal</th>
                    <th>Keterangan</th>
                    <th>Tanggal Pengeluaran</th>
                    <th>Option</th>
                  </tr>
                </tfoot> -->
                <tbody>
                <?php $no =0; foreach($kas_keluar as $data): $no++?>
                    <tr>
                      <td><?php echo $no?></td>
                      <td><?php echo $data->jenis_pengeluaran?></td>
                      <td><?php echo $data->nominal?></td>
                      <td><?php echo $data->keterangan?></td>
                      <td><?php echo $data->tanggal_pengeluaran?></td>
                      
                        
                    </tr>
                  <?php endforeach?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              
            <!-- Load partial : MODAL -->
 
<!-- 1. adminrt/warga/w_warga-->
 <!-- ============================= EDIT ========================= -->
 <!-- Modal -->
<div class="modal fade" id="mKeuanganK" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Transaksi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('adminrt/keuangan/simpan_kas_keluar')?>" method="POST">
      <div class="modal-body">
            <div class="form-group float-label-control">
                <label for="">Jenis Pengeluaran</label>
                <select class="form-control" id="agama" name="xjenis" required>
                  <option value="" disabled selected>Pilih Jenis</option>
                  <option value="Pembelian Barang">Pembelian Barang</option>
                  <option value="Penyewaan Barang">Penyewaan Barang</option>
                  <option value="Sodakoh">Sodakoh</option>
                  <option value="Pembiayaan Acara">Pembiayaan Acara</option>
                  <option value="Lainnya">Lainnya</option>
                </select>
            </div>
            <div class="form-group">
                <label for="usr">Nominal :</label>
                <input type="number" class="form-control" id="usr" name="xnominal" required>
            </div>
            <div class="form-group">
                <label for="usr">Keterangan :</label>
                <input type="text" class="form-control" id="usr" name="xketerangan" required>
            </div>
            <div class="form-group">
                <label for="usr">Tanggal Transaksi :</label>
                <input type="date" class="form-control" id="usr" name="xtanggal" required>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
 <!-- =================================================================================== -->

 <!-- end of 1. adminrt/warga/w_warga-->

            </div>
            <!-- End Default Light Table -->
            <!-- Default Dark Table -->
          </div>
          
            <!-- Load partial : ALERT -->
            <?php $this->load->view('adminrt/partials/alerts.php');?>

          <script src="../assets1/js/custom.js"></script>
