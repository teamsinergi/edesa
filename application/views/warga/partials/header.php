<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>E-desa</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Infinitude Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <!-- //Meta tag Keywords -->
    <!-- Custom-Files -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets2/css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="<?php echo base_url(); ?>assets2/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->

    
</head>

<body>
    <!-- home -->
    <div id="home">
        <!--/top-nav -->
        <div class="top_w3pvt_main container">
            <!--/header -->
            <header class="nav_w3pvt text-center ">
                <!-- nav -->
                <nav class="wthree-w3ls">
                    <div id="logo">
                         <h1> <a class="navbar-brand px-0 mx-0 <?php echo $this->uri->segment(2) == 'main' ? 'active': ''; ?>" href=""<?php echo site_url('warga/main'); ?>">RW. 09
                            </a>
                        </h1>
                    </div>

                    <label for="drop" class="toggle">Menu</label>
                    <input type="checkbox" id="drop" />
                    <ul class="menu mr-auto">
                        <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'main' ? 'active': ''; ?>"><a href="<?php echo site_url('warga/main'); ?>">Beranda</a></li>
                            <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'tentang' ? 'active': ''; ?>"><a href="<?php echo site_url('warga/tentang'); ?>">Tentang</a></li>
                            <!-- First Tier Drop Down -->
                            <label for="drop-2" class="toggle toggle-2">Pages <span class="fa fa-angle-down" aria-hidden="true"></span> </label>
                            <li>
                            <a href="#">Menu <span class="fa fa-angle-down" aria-hidden="true"></span></a>
                            <input type="checkbox" id="drop-2" />
                            <ul>

                                <li><a href="#blog" class="drop-text">Agenda RW</a></li>
                            </ul>
                        </li>
                        <li><a href="#gallery">Galeri</a></li>
                        <li><a href="<?php echo site_url('warga/wisata')?>" class="drop-text">Wisata</a></li>
                        <li><a href="#footer">Kontak</a></li>

                        
                            
                            <a href="<?php echo site_url('warga/Login_warga')?>">
                            <li>
                                <div id="btn-login" class="btn more black" style="padding: 4px 20px;"> 
                                    LOGIN
                                <div>
                            </li>
                            </a> 
                            
                        

                    </ul>
                </nav>
                <!-- //nav -->
            </header>
            <!--//header -->
        </div>
        <!-- //top-nav -->
        
</div>
    <!-- //home -->