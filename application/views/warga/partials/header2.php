<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>E-desa</title>
    <!-- Meta tag Keywords -->
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="Infinitude Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
     <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url(); ?>../assets1/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>../assets1/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url(); ?>assets1/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="<?php echo base_url(); ?>assets1/vendor/datatables/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(); ?>assets1/vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url(); ?>assets1/js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="<?php echo base_url(); ?>assets1/js/demo/datatables-demo.js"></script>
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="<?php echo base_url(); ?>assets/styles/shards-dashboards.1.1.0.min.css">
    <link href="<?php echo base_url(); ?>assets1/css/sb-admin.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- //Meta tag Keywords -->
    <!-- Custom-Files -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets2/css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets2/css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="<?php echo base_url(); ?>assets2/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->

    
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/extras.1.1.0.min.css">
     <link href="<?php echo base_url(); ?>assets1vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    
</head>

<body>
    <!-- home -->
    <div id="home">
        <!--/top-nav -->
        <div class="top_w3pvt_main container">
            <!--/header -->
            <header class="nav_w3pvt text-center ">
                <!-- nav -->
                <nav class="wthree-w3ls">
                    <div id="logo">
                        <h1> <a class="navbar-brand px-0 mx-0 <?php echo $this->uri->segment(2) == 'main' ? 'active': ''; ?>" href=""<?php echo site_url('warga/main'); ?>">RW. 09
                            </a>
                        </h1>
                    </div>

                    <label for="drop" class="toggle">Menu</label>
                    <input type="checkbox" id="drop" />
                    <ul class="menu mr-auto">
                        <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'main' ? 'active': ''; ?>"><a href="<?php echo site_url('warga/main'); ?>">Beranda</a></li>

                            <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'tentang' ? 'active': ''; ?>"><a href="<?php echo site_url('warga/tentang'); ?>">Tentang</a></li>
                            <!-- First Tier Drop Down -->
                            

                            <li>
                                <label for="drop-2" class="toggle toggle-2">Menu <span class="fa fa-angle-down" aria-hidden="false"></span> </label>
                            <a href="#">Menu  <span class="fa fa-angle-down" aria-hidden="true"></span></a>
                            <input type="checkbox" id="drop-2" />

                            <ul>

                                <li><a href="#blog" class="drop-text">Agenda RW</a></li>
                                <li><a href="<?php echo site_url('warga/agenda')?>" class="drop-text">Agenda RT</a></li>
                                <li><a href="<?php echo site_url('warga/keuangan')?>" class="drop-text">Keuangan</a></li>
                                <li><a href="<?php echo site_url('warga/inventori')?>" class="drop-text">Inventori</a></li>
                                
                            </ul>
                        </li>

                        <li><a href="#gallery">Galeri</a></li>
                        <li><a href="<?php echo site_url('warga/wisata')?>" class="drop-text">Wisata</a></li>
                        <li><a href="#footer">Kontak</a></li>

                       

                            <!-- <a href="<?php echo site_url('warga/Login_warga/warga_keluar')?>">
                            <li>
                                <div id="btn-login" class="btn more black" style="padding: 4px 20px;"> 
                                    LOGOUT

                                </div>


                            
                            </li>

                        </a> -->
                         <li>
                                <label for="drop-user" class="toggle toggle-2">Pengguna <span class="fa fa-angle-down" aria-hidden="false"></span> </label>
                            <a href="#"> <div id="btn-login" class="btn more black" style="padding: 4px 15px;"> 
                                    <i class="fa fa-user"></i>

                                </div></a>
                            <input type="checkbox" id="drop-user" />

                            <ul style="left: 86%; text-align: center; ">

                                <li><a href="<?php echo site_url('warga/profile')?>" class="drop-text">Profil</a></li>
                                <li><a href="<?php echo site_url('warga/Login_warga/warga_keluar')?>" class="drop-text">Logout</a></li>
                            </ul>
                        </li>


                    </ul>




                </nav>
                <!-- //nav -->
            </header>
            <!--//header -->
        </div>
        <!-- //top-nav -->
        
</div>
    <!-- //home -->