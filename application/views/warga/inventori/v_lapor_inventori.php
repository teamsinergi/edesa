<div id="home">

     <?php if($this->session->userdata('no_kk')!=null){?>
    
        <?php $this->load->view('warga/partials/header2.php')?>
    <?php }else{?>
        
    <?php $this->load->view('warga/partials/header.php')?>
    <?php }?>
                  <div class="images">
                <div class="images-inner">
                    <div class="image-slide" style="height: 19%">
                        <div class="banner-w3pvt-3">
                            <div class="overlay-w3ls">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- about -->
    <section class="about py-5">
               <!--/team -->
        <div class="container p-md-5">
            <h3 class="tittle-w3ls text-center">Inventori
            </h3>

            
        <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
 <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-5">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Data Inventori Barang</h3><br>
                
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col">
                <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Data Inventori Barang</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Spesifikasi</th>
                    <th>Banyak Barang</th>
                    <th>Kondisi Barang</th>
                    <th>Keterangan</th>
                    <th>Foto Barang</th>
                   
                  </tr>
                </thead>
                
                <tbody>
                <?php $no=0; foreach($inventori_barang as $data): $no++;?>
                    <tr>
                      <td><?php echo $no?></td>
                      <td><?php echo $data->nama_barang?></td>
                      <td><?php echo $data->spesifikasi?></td>
                      <td><?php echo $data->banyak_barang?></td>
                      <td><?php echo $data->kondisi_barang?></td>
                      <td><?php echo $data->keterangan?></td>
                      <td><img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>" alt="infinitude" class="img-fluid" style="max-height: 50px"></td>
                    </tr>
                <?php endforeach?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          

        </div>
    </section>
    <!--//team-->
        </div>
    </section>
    <!-- //about -->
    


    <?php $this->load->view('warga/partials/footer.php'); ?>