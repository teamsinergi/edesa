<div id="home">

    <?php $this->load->view('warga/partials/header2.php')?>

<!-- about -->
     <!--/services-->
    <section class="services" id="services">
        <div class="over-lay-blue py-5">
            <div class="container py-md-5">
                <div class="row my-4">
                    <div class="col-lg-5 services-innfo pr-5">
                        <h3 class="tittle-w3ls two mb-3 text-left">Bondan Wirawan, S.E.</h3>
                         <p class="sub-tittle mt-2 mb-sm-3 text-left">Nomor Induk Keluarga : <b>123456789</b> </p> 
                        <a href="services.html"><img src="<?php echo base_url(); ?>assets2/images/bonds.jpeg" alt="w3pvt" class="img-fluid"></a>
                    </div>
                    <div class="col-lg-7 services-grid-inf">
                        <div class="row services-w3pvt-main mt-5">
                            <div class="col-lg-6 feature-gird">
                                <div class="row features-hny-inner-gd mt-3">
                                    <div class="col-md-2 featured_grid_left">
                                        <div class="icon_left_grid">
                                            <span class="fa fa-address-card" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-10 featured_grid_right_info">
                                        <h4><a class="link-hny" href="single.html">Alamat</a></h4>
                                        <p>Cokrodiningratan, Jetis, Yogyakarta City, Special Region of Yogyakarta 55233.</p>

                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6 feature-gird">
                                <div class="row features-hny-inner-gd mt-3">
                                    <div class="col-md-2 featured_grid_left">
                                        <div class="icon_left_grid">
                                            <span class="fa fa-home" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-10 featured_grid_right_info">
                                        <h4><a class="link-hny" href="single.html">Domisili</a></h4>
                                        <p>Jogja.</p>

                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="row services-w3pvt-main mt-3">
                            <div class="col-lg-6 feature-gird " style="width: 100%;">
                                <div class="row features-hny-inner-gd mt-4">
                                    <div class="col-md-2 featured_grid_left">
                                        <div class="icon_left_grid">
                                            <span class="fa fa-users" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-10 featured_grid_right_info">
                                        <h4><a class="link-hny" href="single.html">Data Keluarga</a></h4>

                                        <p> NIK Kartu Keluarga : <b> 123456789 </b>
                                            <br>
                                            Jumlah Anggota Keluarga : 2 
                                        </p>

                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//services-->
    <!-- //about -->
   

    <?php $this->load->view('warga/partials/footer.php'); ?>














  