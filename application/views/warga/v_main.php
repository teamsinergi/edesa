 <div id="home">

    <?php if($this->session->userdata('no_kk')!=null){?>
    
        <?php $this->load->view('warga/partials/header2.php')?>
    <?php }else{?>
        
    <?php $this->load->view('warga/partials/header.php')?>
    <?php }?>

<!-- banner slider -->
        <div id="homepage-slider" class="st-slider">
            <input type="radio" class="cs_anchor radio" name="slider" id="play1" checked="" />
            <input type="radio" class="cs_anchor radio" name="slider" id="slide1" />
            <input type="radio" class="cs_anchor radio" name="slider" id="slide2" />
            <input type="radio" class="cs_anchor radio" name="slider" id="slide3" />
            <div class="images">
                <div class="images-inner">
                    <div class="image-slide">
                        <div class="banner-w3pvt-1">
                            <div class="overlay-w3ls">

                            </div>

                        </div>
                    </div>
                    <div class="image-slide">
                        <div class="banner-w3pvt-2">
                            <div class="overlay-w3ls">

                            </div>
                        </div>
                    </div>
                    <div class="image-slide">
                        <div class="banner-w3pvt-3">
                            <div class="overlay-w3ls">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="labels">
                <div class="fake-radio">
                    <label for="slide1" class="radio-btn"></label>
                    <label for="slide2" class="radio-btn"></label>
                    <label for="slide3" class="radio-btn"></label>
                </div>
            </div>
            <!-- banner-hny-info -->
            <div class="banner-hny-info">
                <h3>RW. 09 
                    <br>Kel. Cokrodiningratan Kec. Jetis 
                    <br>YOGYAKARTA</h3>
                <div class="top-buttons mx-auto text-center mt-md-5 mt-3">
                    <a href="single.html" class="btn more mr-2">Read More</a>
                    <a href="contact.html" class="btn">Contact Us</a>
                </div>
                <div class="d-flex hny-stats-inf" style="margin-top: 4%">
                    <div class="col-md-2 stats_w3pvt_counter_grid mt-3">
                        <div class="d-md-flex justify-content-center">
                            <h5 class="counter"><?php echo $banyakrw?></h5>
                            <p class="para-w3pvt">RW</p>
                        </div>
                    </div>
                    <div class="col-md-4 stats_w3pvt_counter_grid mt-3">
                        <div class="d-md-flex justify-content-center">
                            <h5 class="counter"><?php echo $banyakrt?></h5>
                            <p class="para-w3pvt">RT</p>
                        </div>
                    </div>
                    <div class="col-md-5 stats_w3pvt_counter_grid mt-3">
                        <div class="d-md-flex justify-content-center">
                            <h5 class="counter"><?php echo $kepalakeluarga?></h5>
                            <p class="para-w3pvt">Kepala Keluarga</p>
                        </div>
                    </div>
                    <div class="col-md-4 stats_w3pvt_counter_grid mt-3">
                        <div class="d-md-flex justify-content-center">
                            <h5 class="counter"><?php echo $banyakwarga?></h5>
                            <p class="para-w3pvt">Warga</p>
                        </div>
                    </div>
                </div>

            </div>
            <!-- //banner-hny-info -->
        </div>
        <!-- //banner slider -->
    </div>
    <!-- //banner -->
<!-- about -->
    <section class="about py-5">
        <div class="container p-md-5">
            <div class="about-hny-info text-left px-md-5">
                <h3 class="tittle-w3ls mb-3"><span class="pink">RW</span> 09</h3>
                <p class="sub-tittle mt-3 mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a class="btn more black" href="single.html" role="button">Read More</a>
            </div>
        </div>
    </section>
    <!-- //about -->
    <!--/ab-->
    <!-- <section class="banner_bottom py-5">
        <div class="container py-md-5">
            <div class="row inner_sec_info">

                <div class="col-md-6 banner_bottom_grid help">
                    <img src="<?php echo base_url(); ?>assets2/images/ab.jpg" alt=" " class="img-fluid">
                </div>
                <div class="col-md-6 banner_bottom_left mt-lg-0 mt-4">
                    <h4><a class="link-hny" href="services.html">
                           Lorem Ipsum</a></h4>
                    <p>Lorem.</p>
                    <a class="btn more black mt-3" href="services.html" role="button">Selengkapnya</a>

                </div>
            </div>
            <div class="row features-w3pvt-main" id="features">
                <div class="col-md-4 feature-gird">
                    <div class="row features-hny-inner-gd">
                        <div class="col-md-3 featured_grid_left">
                            <div class="icon_left_grid">
                                <span class="fa fa-globe" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-md-9 featured_grid_right_info pl-lg-0">
                            <h4><a class="link-hny" href="single.html">Kalo Perlu Bisa dipake</a></h4>
                            <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 feature-gird">
                    <div class="row features-hny-inner-gd">
                        <div class="col-md-3 featured_grid_left">
                            <div class="icon_left_grid">
                                <span class="fa fa-laptop" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-md-9 featured_grid_right_info pl-lg-0">
                            <h4><a class="link-hny" href="single.html">Kalo Perlu Bisa dipake</a></h4>
                            <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 feature-gird">
                    <div class="row features-hny-inner-gd">
                        <div class="col-md-3 featured_grid_left">
                            <div class="icon_left_grid">
                                <span class="fa fa-handshake-o" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-md-9 featured_grid_right_info pl-lg-0">
                            <h4><a class="link-hny" href="single.html">Kalo Perlu Bisa dipake</a></h4>
                            <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section> -->
    <!--//ab-->

    <!--/services-->
    <!-- <section class="services" id="services">
        <div class="over-lay-blue py-5">
            <div class="container py-md-5">
                <div class="row my-4">
                    <div class="col-lg-5 services-innfo pr-5">
                        <h3 class="tittle-w3ls two mb-3 text-left"><span class="pink">What</span> We Provide</h3>
                        <p class="sub-tittle mt-2 mb-sm-3 text-left">Integer pulvinar leo id viverra feugiat. Pellentesque libero ut justo, semper at tempus vel, ultrices in ligula..</p>
                        <a href="services.html"><img src="<?php echo base_url(); ?>assets2/images/ab2.jpg" alt="w3pvt" class="img-fluid"></a>
                    </div>
                    <div class="col-lg-7 services-grid-inf">
                        <div class="row services-w3pvt-main mt-5">
                            <div class="col-lg-6 feature-gird">
                                <div class="row features-hny-inner-gd mt-3">
                                    <div class="col-md-2 featured_grid_left">
                                        <div class="icon_left_grid">
                                            <span class="fa fa-paint-brush" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-10 featured_grid_right_info">
                                        <h4><a class="link-hny" href="single.html">UI/UX Designs</a></h4>
                                        <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>

                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6 feature-gird">
                                <div class="row features-hny-inner-gd mt-3">
                                    <div class="col-md-2 featured_grid_left">
                                        <div class="icon_left_grid">
                                            <span class="fa fa-bullhorn" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-10 featured_grid_right_info">
                                        <h4><a class="link-hny" href="single.html">SEO Marketing</a></h4>
                                        <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>

                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="row services-w3pvt-main mt-5">
                            <div class="col-lg-6 feature-gird ">
                                <div class="row features-hny-inner-gd mt-3">
                                    <div class="col-md-2 featured_grid_left">
                                        <div class="icon_left_grid">
                                            <span class="fa fa-shield" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-10 featured_grid_right_info">
                                        <h4><a class="link-hny" href="single.html">User Experience</a></h4>
                                        <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>

                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6 feature-gird">
                                <div class="row features-hny-inner-gd mt-3">
                                    <div class="col-md-2 featured_grid_left">
                                        <div class="icon_left_grid">
                                            <span class="fa fa-lightbulb-o" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-10 featured_grid_right_info">
                                        <h4><a class="link-hny" href="single.html">Creative Strategy</a></h4>
                                        <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>

                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--//services-->
    <!-- /projects -->
    <section class="projects py-5" id="gallery">
        <div class="container py-md-5">
            <h3 class="tittle-w3ls text-left mb-5">Galeri</h3>
            <div class="row news-grids mt-md-5 mt-4 text-center">
                <?php foreach($galeri as $data):?>
                    <div class="col-md-4 gal-img">
                        <a href="#gal1"><img src="<?php echo base_url('assets/images/'.$data->foto_galeri) ?>" alt="w3pvt" class="img-fluid"></a>
                        <div class="gal-info">
                            <h5><?php echo $data->nama_galeri?><span class="decription"><?php echo $data->tempat?></span></h5>
                        </div>
                    </div>
                <?php endforeach?>

            </div>
            <!-- popup-->
            <div id="gal1" class="pop-overlay">
                <div class="popup">
                    <img src="<?php echo base_url(); ?>assets2/images/g1.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#gallery">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal2" class="pop-overlay">
                <div class="popup">
                    <img src="<?php echo base_url(); ?>assets2/images/g2.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#gallery">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal3" class="pop-overlay">
                <div class="popup">
                    <img src="<?php echo base_url(); ?>assets2/images/g3.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#gallery">&times;</a>
                </div>
            </div>
            <!-- //popup3 -->
            <!-- popup-->
            <div id="gal4" class="pop-overlay">
                <div class="popup">
                    <img src="<?php echo base_url(); ?>assets2/images/g4.jpg" alt="Popup Image" class="img-fluid" />
                    <h5>View Project</h5>
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#gallery">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal5" class="pop-overlay">
                <div class="popup">
                    <img src="<?php echo base_url(); ?>assets2/images/g5.jpg" alt="Popup Image" class="img-fluid" />
                    <h5 class="mt-3">View Project</h5>
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#gallery">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal6" class="pop-overlay">
                <div class="popup">
                    <img src="<?php echo base_url(); ?>assets2/images/g6.jpg" alt="Popup Image" class="img-fluid" />
                    <h5 class="mt-3">View Project</h5>
                    <p>Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#gallery">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal7" class="pop-overlay">
                <div class="popup">
                    <img src="<?php echo base_url(); ?>assets2/images/g7.jpg" alt="Popup Image" class="img-fluid" />
                    <h5 class="mt-3">View Project</h5>
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#gallery">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal8" class="pop-overlay">
                <div class="popup">
                    <img src="<?php echo base_url(); ?>assets2/images/g8.jpg" alt="Popup Image" class="img-fluid" />
                    <h5 class="mt-3">View Project</h5>
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#gallery">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal9" class="pop-overlay">
                <div class="popup">
                    <img src="<?php echo base_url(); ?>assets2/images/g9.jpg" alt="Popup Image" class="img-fluid" />
                    <h5 class="mt-3">View Project</h5>s
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#gallery">&times;</a>
                </div>
            </div>
            <!-- //popup -->
        </div>
    </section>
    <!-- //projects -->
    <!-- /blogs -->
    <section class="blog-posts" id="blog">
        <div class="blog-w3pvt-info-content container-fluid">
            <h3 class="tittle-w3ls text-center mb-5">Agenda RW</h3>
            
            <?php foreach($agenda as $data):?>

               
            <div class="blog-grids-main row text-left">
                <div class="col-lg-3 col-md-6 blog-grid-img px-0">
                    <img src="<?php echo base_url(); ?>assets2/images/g2.jpg" alt="Popup Image" class="img-fluid" />
                </div>
                <div class="col-lg-3 col-md-6 blog-grid-info px-0">
                    <div class="date-post">
                        <h6 class="date"><?php echo $data->tanggal_agenda?></h6>
                        <h4><a class="link-hny" href="single.html"><?php echo $data->nama_agenda?></a></h4>
                        <p><?Php echo $data->keterangan?></p>
                    </div>
                </div>
                <!-- <div class="col-lg-3 col-md-6 blog-grid-img px-0">
                    <img src="<?php echo base_url(); ?>assets2/images/g4.jpg" alt="Popup Image" class="img-fluid" />
                </div>
                <div class="col-lg-3 col-md-6 blog-grid-info px-0 ">
                    <div class="date-post">
                        <h6 class="date">May, 04th 2019</h6>
                        <h4><a class="link-hny" href="single.html">Perayaan Idul Adha</a></h4>
                        <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                    </div>
                </div> -->
            </div>
            <?php endforeach?>

            <!-- <div class="blog-grids-main row text-left">

                <div class="col-lg-3 col-md-6 blog-grid-info px-0">
                    <div class="date-post">
                        <h6 class="date">May, 04th 2019</h6>
                        <h4><a class="link-hny" href="single.html">Kerja Bakti</a></h4>
                        <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 blog-grid-img px-0">
                    <img src="<?php echo base_url(); ?>assets2/images/g6.jpg" alt="Popup Image" class="img-fluid" />
                </div>

                <div class="col-lg-3 col-md-6 blog-grid-info px-0">
                    <div class="date-post">
                        <h6 class="date">May, 04th 2019</h6>
                        <h4><a class="link-hny" href="single.html">Arisan di ruma bu Sri</a></h4>
                        <p>Lorem Ipsum is simply text the printing and typesetting standard industry.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 blog-grid-img px-0">
                    <img src="<?php echo base_url(); ?>assets2/images/g8.jpg" alt="Popup Image" class="img-fluid" />
                </div> -->

            </div>
        </div>

    </section>
    <!-- //blogs -->
    <!--/mid-->
    <section class="banner_bottom py-5" id="appointment">
        <div class="container py-md-5">
            <div class="row inner_sec_info">


                <div class="col-lg-5 banner_bottom_left">

                    <div class="login p-md-5 p-4 mx-auto bg-white mw-100">
                        <h4>
                            Ajukan Aduan Anda</h4>
                        <form action="<?php echo site_url('warga/pengaduan/simpan_pengaduan')?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="usr">Keterangan :</label>
                                <input type="text" class="form-control" id="usr" name="xketerangan" >             </div>
                            <div class="form-group">
                                <label for="usr">Tempat :</label>
                                <input type="text" class="form-control" id="usr" name="xtempat" >         </div>
                            <div class="form-group">
                                <label for="usr">Tanggal :</label>
                                <input type="date" class="form-control" id="usr" name="xtanggal" >
                            </div>
                            <div class="form-group">
                                <label for="usr">Jam :</label>
                                <input type="time" class="form-control" id="usr" name="xjam" >             </div>
                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Photo</label>
                                <div class="form-control">
                                <input type="file" name="filefoto"/>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                        </form>

                    </div>

                </div>
                <div class="col-lg-7 banner_bottom_grid help pl-lg-5">
                    <img width="110%" src="<?php echo base_url(); ?>assets/images/pengaduan1.png" alt=" " class="img-fluid mb-4">
                    <h4>Apa Itu Fitur Pengaduan?</h4>
                    <p class="mt-3">Fitur pengaduan adalah fitur untuk melaporkan masalah atau keluhan yang di alami oleh warga kepada ketua RT </p>

                </div>
            </div>

        </div>
    </section>
    <!--//mid-->

    <!--/services-->
    <section class="testmonials" id="test">
        <div class="over-lay-blue py-5">
            <div class="container py-md-5">
                <h3 class="tittle-w3ls two text-center mb-5">Sambutan</h3>
                <div class="row my-4">
                    <div class="col-lg-4 testimonials_grid mt-3">
                        <div class="p-lg-5 p-4 testimonials-gd-vj">
                            <p class="sub-test"><span class="fa fa-quote-left s4" aria-hidden="true"></span> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                            <div class="row mt-4">
                                <div class="col-3 testi-img-res">
                                    <img src="<?php echo base_url(); ?>assets2/images/t1.jpg" alt=" " class="img-fluid">
                                </div>
                                <div class="col-9 testi_grid">
                                    <h5 class="mb-2">Lorem Ipsum</h5>
                                    <p>Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 testimonials_grid mt-3">
                        <div class="p-lg-5 p-4 testimonials-gd-vj">
                            <p class="sub-test"><span class="fa fa-quote-left s4" aria-hidden="true"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                            <div class="row mt-4">
                                <div class="col-3 testi-img-res">
                                    <img src="<?php echo base_url(); ?>assets2/images/t2.jpg" alt=" " class="img-fluid">
                                </div>
                                <div class="col-9 testi_grid">
                                    <h5 class="mb-2">Lorem Ipsum</h5>
                                    <p>Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 testimonials_grid mt-3">
                        <div class="p-lg-5 p-4 testimonials-gd-vj">
                            <p class="sub-test"><span class="fa fa-quote-left s4" aria-hidden="true"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>
                            <div class="row mt-4">
                                <div class="col-3 testi-img-res">
                                    <img src="<?php echo base_url(); ?>assets2/images/t1.jpg" alt=" " class="img-fluid">
                                </div>
                                <div class="col-9 testi_grid">
                                    <h5 class="mb-2">Lorem Ipsum</h5>
                                    <p>Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//testimonials-->

    <!-- /news-letter -->
     <div class="map-w3pvt mt-5">
                       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.092438567726!2d110.36705141477796!3d-7.78002309439312!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a59afe9ac454b%3A0x75b8b5f4e03cf202!2sAngkringan+dan+Penyetan+Bu+Nina!5e0!3m2!1sen!2sid!4v1565954648280!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
    <!-- <section class="news-letter-w3pvt py-5">
        <div class="container contact-form mx-auto text-left">
            <h3 class="title-w3ls two text-left mb-3">Newsletter </h3>
            <form method="post" action="#" class="w3ls-frm">
                <div class="row subscribe-sec">
                    <p class="news-para col-lg-3">Start working together?</p>
                    <div class="col-lg-6 con-gd">
                        <input type="email" class="form-control" id="email" placeholder="Your Email here..." name="email" required>

                    </div>
                    <div class="col-lg-3 con-gd">
                        <button type="submit" class="btn submit">Subscribe</button>
                    </div>

                </div>

            </form>
        </div>
    </section> -->
    <!-- //news-letter -->
    </div>

    <?php $this->load->view('warga/partials/footer.php'); ?>