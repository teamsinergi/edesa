<div id="home">

     <?php if($this->session->userdata('no_kk')!=null){?>
    
        <?php $this->load->view('warga/partials/header2.php')?>
    <?php }else{?>
        
    <?php $this->load->view('warga/partials/header.php')?>
    <?php }?>
                  <div class="images">
                <div class="images-inner">
                    <div class="image-slide" style="height: 19%">
                        <div class="banner-w3pvt-3">
                            <div class="overlay-w3ls">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- about -->
    <section class="about py-5">
               <!--/team -->
        <div class="container p-md-5">
            <h3 class="tittle-w3ls text-center">Agenda RT
            </h3>

            
            <div class="row mt-lg-5 mt-4">
                <?php foreach($agenda_rt as $data):?>
                    <div class="col-md-4 team-gd text-center">
                        <div class="team-img mb-4">
                            <a href="single.html"><img src="<?php echo base_url('assets/images/'.$data->gambar_agenda) ?>" class="img-fluid" alt="user-image"></a>
                        </div>
                        <div class="team-info">
                            <span class="sub-tittle-team"><?php echo $data->tanggal_agenda?></span>
                            <h3><a href="single.html"> <?php echo $data->nama_agenda?></a></h3>
                            <p><?php echo $data->keterangan?></p>
                        </div>
                    </div>
                <?php endforeach?>
            </div>

        </div>
    </section>
    <!--//team-->
        </div>
    </section>
    <!-- //about -->
   

    <?php $this->load->view('warga/partials/footer.php'); ?>