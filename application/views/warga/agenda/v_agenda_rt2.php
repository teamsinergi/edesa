<div id="home">

     <?php if($this->session->userdata('no_kk')!=null){?>
    
        <?php $this->load->view('warga/partials/header2.php')?>
    <?php }else{?>
        
    <?php $this->load->view('warga/partials/header.php')?>
    <?php }?>
                  <div class="images">
                <div class="images-inner">
                    <div class="image-slide" style="height: 19%">
                        <div class="banner-w3pvt-3">
                            <div class="overlay-w3ls">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- about -->
<section class="about py-5">
        <div class="py-md-5">
            <div class="about-hny-info" id="timeline">
                <h3 class="tittle-w3ls mb-5 text-center">Agenda RT </h3>
                <div class="demo-card-wrapper">
                    <?php $no=0; foreach($agenda_rt as $data): $no++;?>
                        <div class="demo-card demo-card--step<?php echo $no;?>">
                            <div class="head">
                                <div class="number-box">
                                    <span><?php echo $no?></span>
                                </div>
                                <h3><span class="small"><?php echo $data->tanggal_agenda?></span><?php echo $data->nama_agenda?></h3>
                            </div>
                            <div class="body">
                                <p><?php echo $data->keterangan?></p>
                                <img src="<?php echo base_url('assets/images/'.$data->gambar_agenda) ?>" alt="infinitude" class="img-fluid" style="max-height: 250px">
                            </div>
                        </div>
                    <?php endforeach?>
                    <?php foreach($agenda_rt1 as $data): $no++;?>
                        <div class="demo-card demo-card--step<?php echo $no;?>">
                            <div class="head">
                                <div class="number-box">
                                    <span><?php echo $no?></span>
                                </div>
                                <h3><span class="small"><?php echo $data->tanggal_agenda?></span><?php echo $data->nama_agenda?></h3>
                            </div>
                            <div class="body">
                                <p><?php echo $data->keterangan?></p>
                                <img src="<?php echo base_url('assets/images/'.$data->gambar_agenda) ?>" alt="infinitude" class="img-fluid" style="max-height: 250px">
                            </div>
                        </div>
                    <?php endforeach?>
                </div>

            </div>
        </div>
    </section>
    <!-- //about -->
   

    <?php $this->load->view('warga/partials/footer.php'); ?>