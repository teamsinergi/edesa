<div id="home">

     <?php if($this->session->userdata('no_kk')!=null){?>
    
        <?php $this->load->view('warga/partials/header2.php')?>
    <?php }else{?>
        
    <?php $this->load->view('warga/partials/header.php')?>
    <?php }?>
                  <div class="images">
                <div class="images-inner">
                    <div class="image-slide" style="height: 19%">
                        <div class="banner-w3pvt-3">
                            <div class="overlay-w3ls">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- about -->
    <section class="about py-5">
               <!--/team -->
        <div class="container p-md-5">
            <h3 class="tittle-w3ls text-center">Laporan Keuangan
            </h3>

            
        <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <form action="<?php echo site_url('warga/keuangan/filter_keuangan')?>" method="post">
                            <div class="form-group" style="font-size:10px; padding-top:5px;">
                            <h4>Laporan Kas Desa Bulan :</h4>
                                <select class="form-control" id="sel1" name="xbulan" onchange="this.form.submit();">
                                    <?php foreach($bulan as $data):?>
                                        <option value="<?php echo $data->kode_bulan?>"><?php echo $data->nama_bulan?></option>
                                    <?php endforeach?>
                                </select>
                            </div> 
                        </form>
            <!-- End Page Header -->
            <!-- Small Stats Blocks -->
            <div class="row">
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Kas Desa</span>
                        <h6 class="stats-small__value count my-3">Rp. <?php $totalkas = ($kasmasuk-$kaskeluar);  echo number_format($totalkas,0,',','.') ?></h6>
                      </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Kas Keluar</span>
                        <h6 class="stats-small__value count my-3">Rp. <?php echo number_format($kaskeluar,0,',','.') ?></h6>
                      </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-2"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Kas Masuk</span>
                        <h6 class="stats-small__value count my-3">Rp. <?php echo number_format($kasmasuk,0,',','.') ?></h6>
                      </div>
                     </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-3"></canvas>
                  </div>
                </div>
              </div>
              <!-- <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Users</span>
                        <h6 class="stats-small__value count my-3">2,413</h6>
                      </div>
                      <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--increase">12.4%</span>
                      </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-4"></canvas>
                  </div>
                </div>
              </div> -->
              <!-- <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Subscribers</span>
                        <h6 class="stats-small__value count my-3">17,281</h6>
                      </div>
                      <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--decrease">2.4%</span>
                      </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-5"></canvas>
                  </div>
                </div>
              </div> -->
            </div>
            <!-- End Small Stats Blocks -->
          </div>
          

        </div>
    </section>
    <!--//team-->
        </div>
    </section>
    <!-- //about -->
    


    <?php $this->load->view('warga/partials/footer.php'); ?>